/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module d2dparts is aliced;
private:

version = ogl_dots;

version(ogl_dots) {
  import iv.glbinds;
  public enum dotsAtTexture = false;
} else {
  public enum dotsAtTexture = true;
}

import iv.cmdcon;
import arsd.color;

import glutils;
import wadarc;

import d2dmap;
import d2dimage;
import dengapi : map, unsyncrandu31;


// ////////////////////////////////////////////////////////////////////////// //
// "dead" blood dots will stay on image
//TODO: "dormant" blood should fall down if map was modified


public void dotInit () {
  imgParts = new TrueColorImage(map.width*TileSize, map.height*TileSize);
  imgPartsOld = new TrueColorImage(map.width*TileSize, map.height*TileSize);
  foreach (int y; 0..map.height*TileSize) {
    foreach (int x; 0..map.width*TileSize) {
      putPixel(x, y, Color(0, 0, 0, 0));
      putPixelOld(x, y, Color(0, 0, 0, 0));
    }
  }
  texParts = new Texture(imgParts, Texture.Option.Nearest, Texture.Option.Clamp);
  /*
  foreach (int y; 0..map.height*TileSize) {
    foreach (int x; 0..map.width*TileSize) {
      putPixel(x, y, Color(0, 200, 0, 255));
    }
  }
  */
  //texParts.setFromImage(imgParts, 0, 0);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared public Texture texParts; // texture for particles; can be blended on bg
__gshared TrueColorImage imgParts, imgPartsOld;


void putPixelOld (int x, int y, Color clr) {
  pragma(inline, true);
  if (/*clr.a != 0 &&*/ x >= 0 && y >= 0 && x < imgPartsOld.width && y < imgPartsOld.height) {
    imgPartsOld.imageData.colors.ptr[y*imgPartsOld.width+x] = clr;
  }
}


void putPixel (int x, int y, Color clr) {
  pragma(inline, true);
  if (/*clr.a != 0 &&*/ x >= 0 && y >= 0 && x < imgParts.width && y < imgParts.height) {
    imgParts.imageData.colors.ptr[y*imgParts.width+x] = clr;
  }
}


Color getPixel (int x, int y) {
  pragma(inline, true);
  return (x >= 0 && y >= 0 && x < imgParts.width && y < imgParts.height ? imgParts.imageData.colors.ptr[y*imgParts.width+x] : Color(0, 0, 0, 0));
}


// ////////////////////////////////////////////////////////////////////////// //
enum BL_XV = 4;
enum BL_YV = 4;
//enum BL_MINT = 10;
//enum BL_MAXT = 14;

enum SP_V = 2;
enum SP_MINT = 5;
enum SP_MAXT = 7;


// ////////////////////////////////////////////////////////////////////////// //
struct DotParticle {
  int prevX, prevY;
  int lastX, lastY;
  int x, y, xv, yv, vx, vy;
  ubyte time;
  ubyte color; // from D2D palette
  Color drawColor;
  //Color prevColor; // on image; a=0: was empty

  @disable this (this); // no copy
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared DotParticle[1024] dots;
__gshared uint dotsUsed;


void dotRemove (usize idx, bool leftOnPic) {
  if (idx >= dotsUsed) return;
  import core.stdc.string : memmove;
  auto dt = dots.ptr+idx;
  if (leftOnPic) putPixelOld(dt.x, dt.y, dt.drawColor);
  /*
  auto pclr = (leftOnPic ? dt.drawColor : dt.prevColor);
  bool found = false;
  int px = (leftOnPic ? dt.x : dt.lastX);
  int py = (leftOnPic ? dt.y : dt.lastY);
  // if it has color, reassign this color to next dot at this coords
  foreach (ref dp; dots[0..dotsUsed]) {
    if (dp.lastX == px && dp.lastY == py) {
      dp.prevColor = pclr;
      found = true;
      break;
    }
  }
  // if not found, just draw it
  if (!found) putPixel(px, py, pclr);
  */
  // move dots
  --dotsUsed;
  if (idx < dotsUsed) memmove(dots.ptr+idx, dots.ptr+idx+1, (dotsUsed-idx)*dots[0].sizeof);
}


DotParticle* dotAlloc () {
  if (dotsUsed == dots.length) dotRemove(0, false);
  auto res = dots.ptr+dotsUsed;
  ++dotsUsed;
  *res = DotParticle.init;
  return res;
}


void dotAdd (int x, int y, int xv, int yv, ubyte color, ubyte time) {
  //conwriteln("dotAdd: x=", x, "; y=", y, "; xv=", xv, "; yv=", yv, "; color=", color, "; time=", time);
  if (x < 0 || y < 0 || x >= map.width*TileSize || y >= map.height*TileSize) return;
  if (!Z_canfit(x, y)) return;
  auto dot = dotAlloc();
  if (dot is null) return;
  dot.x = dot.prevX = dot.lastX = x;
  dot.y = dot.prevY = dot.lastY = y;
  dot.xv = xv;
  dot.yv = yv;
  dot.color = color;
  dot.time = time;
  dot.vx = dot.vy = 0;
  //dot.prevColor = getPixel(x, y);
  dot.drawColor = d2dpal[color];
  dot.drawColor.a = 180;
}


// ////////////////////////////////////////////////////////////////////////// //
public void dotDraw (float itp) {
  version(ogl_dots) {
    Color lastc;
    {
      if (dotsUsed == 0) return;
      bindTexture(0);
      auto dt = dots.ptr;
      lastc = dt.drawColor;
      glColor4f(dt.drawColor.r/255.0f, dt.drawColor.g/255.0f, dt.drawColor.b/255.0f, dt.drawColor.a/255.0f);
      glBegin(GL_POINTS);
    }
  }
  import core.stdc.math : roundf;
  // remove dots from image (in reverse order)
  //foreach_reverse (ref dt; dots[0..dotsUsed]) putPixel(dt.lastX, dt.lastY, dt.prevColor);
  imgParts.imageData.colors[] = imgPartsOld.imageData.colors[];
  //imgParts.imageData.colors[] = Color(0, 0, 0, 0);
  // draw dots
  foreach (ref dt; dots[0..dotsUsed]) {
    int nx = cast(int)(dt.x+roundf((dt.x-dt.prevX)*itp));
    int ny = cast(int)(dt.y+roundf((dt.y-dt.prevY)*itp));
    dt.lastX = nx;
    dt.lastY = ny;
    //dt.prevColor = getPixel(nx, ny);
    version(ogl_dots) {
      if (dt.drawColor != lastc) {
        glEnd();
        glColor4f(dt.drawColor.r/255.0f, dt.drawColor.g/255.0f, dt.drawColor.b/255.0f, dt.drawColor.a/255.0f);
        glBegin(GL_POINTS);
        lastc = dt.drawColor;
      }
      glVertex2i(nx, ny);
    } else {
      putPixel(nx, ny, dt.drawColor);
    }
  }
  version(ogl_dots) {
    glEnd();
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  } else {
    // update texture
    texParts.setFromImage(imgParts, 0, 0);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void dotThink () {
  // now move dots
  int idx = 0;
  auto dt = dots.ptr;
  while (idx < dotsUsed) {
    if (dt.time) {
      dt.prevX = dt.x;
      dt.prevY = dt.y;
      int xv = dt.xv+dt.vx;
      int yv = dt.yv+dt.vy;
      if (dt.time < 254) {
        if ((--dt.time) == 0) {
          dotRemove(idx, false);
          continue;
        }
      }
      int st = Z_moveobj(ref *dt);
      if (st&(Z_HITWATER|Z_FALLOUT)) {
        // blood?
        //if (dt.time == 255) putPixel(dt.x, dt.y, d2dpal.ptr[dt.color]);
        dotRemove(idx, (dt.time == 255));
        dt.time = 0;
        continue;
      }
      if (st&Z_HITLAND) {
        if (!dt.xv) {
          if (yv > 2) {
            dt.vx = (xv ? Z_sign(dt.vx) : (unsyncrandu31&0x01 ? -1 : 1));
            if (unsyncrandu31%yv == 0) dt.vx *= 2;
            dt.yv = yv-2;
          }
        }
        dt.xv = 0;
        if (dt.time > 4 && dt.time != 255) dt.time = 4;
      }
      if (st&Z_HITWALL) {
        dt.vx = Z_sign(xv)*2;
        dt.yv = Z_sign(dt.yv);
        if (dt.yv >= 0 && (unsyncrandu31&0x03)) --dt.yv;
        if (dt.yv >= 0 && (unsyncrandu31&0x01)) --dt.yv;
      }
      if (st&Z_HITCEIL) {
        dt.xv = 0;
        dt.yv = (unsyncrandu31%100 ? -2 : 0);
      }
    } else {
      dotRemove(idx, false);
      continue;
    }
    ++idx;
    ++dt;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void dotAddBlood (int x, int y, int xv, int yv, int n) {
  while (n-- > 0) {
    int dx = x+cast(int)unsyncrandu31%(2*2+1)-2;
    int dy = y+cast(int)unsyncrandu31%(2*2+1)-2;
    int dxv = cast(int)unsyncrandu31%(BL_XV*2+1)-BL_XV+Z_dec(xv, 3);
    int dyv = -cast(int)unsyncrandu31%(BL_YV)+Z_dec(yv, 3)-3;
    ubyte clr = cast(ubyte)(0xB0+unsyncrandu31%16);
    dotAdd(dx, dy, dxv, dyv, clr, 255);
  }
}


public void dotAddSpark (int x, int y, int xv, int yv, int n) {
  while (n-- > 0) {
    int dx = x+cast(int)unsyncrandu31%(2*2+1)-2;
    int dy = y+cast(int)unsyncrandu31%(2*2+1)-2;
    int dxv = cast(int)unsyncrandu31%(SP_V*2+1)-SP_V-xv/4;
    int dyv = cast(int)unsyncrandu31%(SP_V*2+1)-SP_V-yv/4;
    ubyte clr = cast(ubyte)(0xA0+unsyncrandu31%6);
    ubyte time = cast(ubyte)(unsyncrandu31%(SP_MAXT-SP_MINT+1)+SP_MINT);
    dotAdd(dx, dy, dxv, dyv, clr, time);
  }
}


public void dotAddWater (int x, int y, int xv, int yv, int n, int color) {
  static immutable ubyte[3] ct = [0xC0, 0x70, 0xB0];
  if (color >= 0 && color < 3) {
    while (n-- > 0) {
      import std.math : abs;
      int dx = x+cast(int)unsyncrandu31%(2*2+1)-2;
      int dy = y+cast(int)unsyncrandu31%(2*2+1)-2;
      int dxv = cast(int)unsyncrandu31%(BL_XV*2+1)-BL_XV+Z_dec(xv, 3);
      int dyv = -cast(int)unsyncrandu31%(BL_YV)-abs(yv);
      ubyte clr = cast(ubyte)(unsyncrandu31%16+ct.ptr[color]);
      dotAdd(dx, dy, dxv, dyv, clr, 254);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
int clamp7 (int v) { /*pragma(inline, true);*/ import std.math : abs; return (abs(v) <= 7 ? v : (v > 0 ? 7 : -7)); }


int Z_sign (int n) { pragma(inline, true); return (n < 0 ? -1 : (n > 0 ? 1 : 0)); }


int Z_dec (int n, int delta) {
  import std.math : abs;
  if (abs(n) > delta) {
    if (n > 0) return n-delta;
    if (n < 0) return n+delta;
  }
  return 0;
}


int Z_checkArea (int x, int y, scope int delegate (ubyte tt) dg) {
  int tx = x/TileSize;
  int ty = y/TileSize;
  if (tx < 0 || ty < 0 || tx >= map.width || ty >= map.height) return 0;
  if (auto res = dg(map.tiles.ptr[LevelMap.Type].ptr[ty*map.width+tx])) return res;
  return 0;
}


bool Z_checkAreaFit (int x, int y, scope int delegate (ubyte tt) dg) {
  if (Z_checkArea(x, y, dg)) {
    int tx = x/TileSize;
    int ty = y/TileSize;
    auto fgt = map.tiles.ptr[LevelMap.Front].ptr[ty*map.width+tx];
    if (fgt >= map.walltypes.length) return false;
    if (map.walltypes[fgt]) return true;
    auto bgt = map.tiles.ptr[LevelMap.Back].ptr[ty*map.width+tx];
    if (bgt >= map.walltypes.length) return false;
    if (map.walltypes[bgt]&0x02) return true;
  }
  return false;
}


bool Z_canfit (int x, int y) {
/*
  int tx = x/TileSize;
  int ty = y/TileSize;
  if (tx < 0 || ty < 0 || tx >= map.width || ty >= map.height) return true;
  auto tt = map.tiles.ptr[LevelMap.Type].ptr[ty*map.width+tx];
  if (tt == LevelMap.TILE_WALL || tt == LevelMap.TILE_DOORC) {
    fgt = map.tiles.ptr[LevelMap.Front].ptr[ty*map.width+tx];
    if (map.walltype[fgt]) return false;
    bgt = map.tiles.ptr[LevelMap.Back].ptr[ty*map.width+tx];
    if (map.walltype[bgt]&0x02) return false;
  }
  return true;
*/
  return !Z_checkAreaFit(x, y, (tt) => (tt == LevelMap.TILE_WALL || tt == LevelMap.TILE_DOORC ? 1 : 0));
}


bool Z_hitceil (int x, int y) {
  return Z_checkAreaFit(x, y, (tt) => (tt == LevelMap.TILE_WALL || tt == LevelMap.TILE_DOORC ? 1 : 0));
}


bool Z_canstand (int x, int y) {
  return Z_checkAreaFit(x, y, (tt) => (tt == LevelMap.TILE_WALL || tt == LevelMap.TILE_DOORC  || tt == LevelMap.TILE_STEP ? 1 : 0));
}


// 0: not; 1: up; 2: down
int Z_inlift (int x, int y) {
  return Z_checkArea(x, y, (tt) {
    if (tt == LevelMap.TILE_LIFTU) return 1;
    if (tt == LevelMap.TILE_LIFTD) return 2;
    return 0;
  });
}


// 0: not; >0: water
int Z_inwater (int x, int y) {
  return Z_checkArea(x, y, (tt) {
    if (tt == LevelMap.TILE_WATER) return 1;
    if (tt == LevelMap.TILE_ACID1) return 2;
    if (tt == LevelMap.TILE_ACID2) return 3;
    return 0;
  });
}


int Z_moveobj (ref DotParticle dot) {
  enum r = 0;
  enum h = 1;
  enum CELW = TileSize;
  enum CELH = TileSize;
  immutable int FLDW = map.width*CELW;
  immutable int FLDH = map.height*CELH;
  int xv, yv, lx, ly;
  int inw;

  int st = 0;
  int x = dot.x;
  int y = dot.y;

  switch (Z_inlift(x, y)) {
    case 0:
      if (++dot.yv > MAX_YV) --dot.yv;
      break;
    case 1:
      if (--dot.yv < -5) ++dot.yv;
      break;
    case 2:
      if (dot.yv > 5) --dot.yv; else ++dot.yv;
      break;
    default:
  }

  inw = Z_inwater(x, y);
  if (inw != 0) {
    import std.math : abs;
    st |= Z_INWATER;
    if ((xv = abs(dot.xv)+1) > 5) dot.xv = Z_dec(dot.xv, xv/2-2);
    if ((xv = abs(dot.yv)+1) > 5) dot.yv = Z_dec(dot.yv, xv/2-2);
    if ((xv = abs(dot.vx)+1) > 5) dot.vx = Z_dec(dot.vx, xv/2-2);
    if ((xv = abs(dot.vy)+1) > 5) dot.vy = Z_dec(dot.vy, xv/2-2);
  }

  dot.vx = Z_dec(dot.vx, 1);
  dot.vy = Z_dec(dot.vy, 1);

  xv = dot.xv+dot.vx;
  yv = dot.yv+dot.vy;

  while (xv || yv) {
    if (x < -100 || x >= FLDW*CELW+100 || y < -100 || y >= FLDH*CELH+100) {
      // out of map
      st |= Z_FALLOUT;
    }

    lx = x;
    x += clamp7(xv);

    if (!Z_canfit(x, y)) {
      if (xv == 0) x = lx;
      else if (xv < 0) x = ((lx-r)&0xFFF8)+r;
      else x = ((lx+r)&0xFFF8)-r+7;
      xv = dot.xv = dot.vx = 0;
      st |= Z_HITWALL;
    }
    xv -= clamp7(xv);

    ly = y;
    y += clamp7(yv);
    if (yv >= TileSize) --y;
    // moving up and hit the ceiling
    if (yv < 0 && Z_hitceil(x, y)) {
      y = ((ly-h+1)&0xFFF8)+h-1;
      yv = dot.vy = 1;
      dot.yv = 0;
      st |= Z_HITCEIL;
    }
    if (yv > 0 && Z_canstand(x, y)) {
      y = ((y+1)&0xFFF8)-1;
      yv = dot.yv = dot.vy = 0;
      st |= Z_HITLAND;
    }
    yv -= clamp7(yv);
  }

  dot.x = x;
  dot.y = y;

  if (Z_inwater(x, y)) {
    st |= Z_INWATER;
    if (!inw) st |= Z_HITWATER;
  } else if (inw) {
    st |= Z_HITAIR;
  }

  return st;
}


// ////////////////////////////////////////////////////////////////////////// //
enum MAX_YV = 30;


enum {
  Z_HITWALL  = 1<<0,
  Z_HITCEIL  = 1<<1,
  Z_HITLAND  = 1<<2,
  Z_FALLOUT  = 1<<3,
  Z_INWATER  = 1<<4,
  Z_HITWATER = 1<<5,
  Z_HITAIR   = 1<<6,
  Z_BLOCK    = 1<<7,
}
