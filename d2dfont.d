/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module d2dfont is aliced;
private:

import arsd.color;
import iv.vfs;

import iv.glbinds;
import iv.cmdcon;
import glutils;
import wadarc;

import d2dimage;
import tatlas;

import iv.vfs.koi8;


// ////////////////////////////////////////////////////////////////////////// //
struct D2DFont {
  bool imagesLoaded;
  D2DImage[256] vga;
  TexAtlas.Rect[256] rects;
  GLuint listBase;
  TexAtlas fontAtlas;

  // called when context is destroyed; so we don't need to free resources
  void glRegen () {
    // create display lists
    listBase = glGenLists(256);
    fontAtlas.updateTexture();
    glPushMatrix();
    scope(exit) glPopMatrix();
    foreach (ubyte cc; 0..256) {
      auto vi = vga[cc];
      TexAtlas.Rect arc = rects[cc];
      if (vi is null) {
        // try uppercase
        ubyte cn = cast(ubyte)koi8upper(cast(char)cc);
        vi = vga[cn];
        arc = rects[cn];
        if (vi is null) {
          // try lowercase
          cn = cast(ubyte)koi8lower(cast(char)cc);
          vi = vga[cn];
          arc = rects[cn];
        }
        if (vi is null) {
          // else use space
          vi = vga[32];
          arc = rects[32];
        }
      }
      glNewList(listBase+cc, GL_COMPILE);
      // draw char sprite
      int x0 = -vi.sx;
      int y0 = -vi.sy;
      int x1 = x0+vi.width;
      int y1 = y0+vi.height;
      auto frect = fontAtlas.texCoords(arc);
      glBegin(GL_QUADS);
        glTexCoord2f(frect.x0, frect.y0); glVertex2i(x0, y0); // top-left
        glTexCoord2f(frect.x1, frect.y0); glVertex2i(x1, y0); // top-right
        glTexCoord2f(frect.x1, frect.y1); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(frect.x0, frect.y1); glVertex2i(x0, y1); // bottom-left
      glEnd();
      // move coords
      glTranslatef(vi.width, 0, 0);
      // done
      glEndList();
    }
  }

  // build texture atlas
  private void buildAtlas (bool asBW=false, int spwdt=-1, int sphgt=-1) {
    // create empty thing for space (if necessary)
    if (vga[32] is null) {
      int height = 0, wdt, count;
      foreach (D2DImage v; vga) if (v !is null) { if (height < v.height) height = v.height; wdt += v.width; ++count; }
      wdt /= count;
      if (spwdt <= 0) spwdt = wdt;
      if (sphgt <= 0) sphgt = height;
      vga[32] = new D2DImage(spwdt, sphgt);
    }
    int asz = 128;
    for (;;) {
      bool success = true;
      fontAtlas = new TexAtlas(asz, asz);
      foreach (ubyte cc; 0..256) {
        if (vga[cc] is null) continue;
        auto img = (asBW ? vga[cc].blackAndWhiteChan!"red".img : vga[cc].img);
        auto rc = fontAtlas.insert(img);
        if (!rc.valid) { success = false; break; }
        rects[cc] = rc;
      }
      if (success) break;
      asz *= 2; // increase atlas size
      if (asz > 2048) assert(0, "invalid bitmap font (too huge)");
    }
  }
}


__gshared D2DFont smfont;
__gshared D2DFont bffont;
__gshared D2DFont smbwfont;


//TODO: reinit font function
public void loadSmFont () { loadSmFontAt(smfont, false); loadSmFontAt(smbwfont, true); }
public void loadBfFont () { loadBfFontAt(bffont); }


void loadSmFontAt (ref D2DFont font, bool asBW) {
  if (!font.imagesLoaded) {
    foreach (ubyte cc; 0..256) {
      //if (cc == 'y') continue; // smfont has invalid glyph there (why?!)
      try {
        import std.string : format;
        font.vga[cc] = new D2DImage("fonts/stcf/stcfnx%02x.vga".format(cc));
      } catch (Exception) {
      }
    }
  }
  font.buildAtlas(asBW);
  font.glRegen();
  /*
  {
    import arsd.png;
    writePng("_zfont.png", font.fontAtlas.img);
  }
  */
}


void loadBfFontAt (ref D2DFont font) {
  if (!font.imagesLoaded) {
    foreach (ubyte cc; 0..256) {
      try {
        import std.string : format;
        font.vga[cc] = new D2DImage("fonts/stbf/stbf_x%02x.vga".format(cc));
      } catch (Exception) {
      }
    }
  }
  font.buildAtlas();
  font.glRegen();
  /*
  {
    import arsd.png;
    writePng("_zfont.png", font.fontAtlas.img);
  }
  */
}


void fontDrawText (ref D2DFont font, int x, int y, const(char)[] str) {
  if (str.length == 0) return;
  glPushMatrix();
  glPushAttrib(GL_LIST_BIT/*|GL_TEXTURE_BIT*/);
  scope(exit) {
    glPopAttrib();
    glPopMatrix();
  }
  bindTexture(font.fontAtlas.tex.id);
  //glRasterPos2i(x, y);
  glTranslatef(x, y, 0);
  glListBase(font.listBase);
  glCallLists(cast(uint)str.length, GL_UNSIGNED_BYTE, str.ptr);
}


int fontTextWidth (ref D2DFont font, const(char)[] str) {
  int res = 0;
  foreach (char ch; str) {
    auto v = font.vga.ptr[ch];
    if (v !is null) res += v.width;
  }
  return res;
}


int fontTextHeight (ref D2DFont font, const(char)[] str) {
  int res = 0;
  foreach (char ch; str) {
    auto v = font.vga.ptr[ch];
    if (v !is null && res < v.height) res = v.height;
  }
  return res;
}


public void smDrawText (int x, int y, const(char)[] str) { fontDrawText(smfont, x, y, str); }
public void bfDrawText (int x, int y, const(char)[] str) { fontDrawText(bffont, x, y, str); }
public void smbwDrawText (int x, int y, const(char)[] str) { fontDrawText(smbwfont, x, y, str); }

public int smTextWidth (const(char)[] str) { return fontTextWidth(smfont, str); }
public int bfTextWidth (const(char)[] str) { return fontTextWidth(bffont, str); }

public int smTextHeight (const(char)[] str) { return fontTextHeight(smfont, str); }
public int bfTextHeight (const(char)[] str) { return fontTextHeight(bffont, str); }


// ////////////////////////////////////////////////////////////////////////// //
__gshared Texture conFontTex;
__gshared GLuint conListBase;
//enum ConCharWdt = 10;
//enum ConCharHgt = 16;
enum ConCharWdt = 8;
enum ConCharHgt = 8;

public void loadConFont () {
  //conFontTex = new Texture("console/confont.png", Texture.Option.Nearest);
  conFontTex = new Texture("console/confont8.png", Texture.Option.Nearest);
  float iw = cast(float)conFontTex.width;
  float ih = cast(float)conFontTex.height;
  conListBase = glGenLists(256);
  glPushMatrix();
  scope(exit) glPopMatrix();
  foreach (ubyte cc; 0..256) {
    //int x = 0, y = 0;
    //if (cc > ' ') { x = ((cc-32)%16)*16; y = ((cc-32)/16)*16; }
    int x = (cc%16)*ConCharWdt;
    int y = (cc/16)*ConCharHgt;
    glNewList(conListBase+cc, GL_COMPILE);
    // draw char sprite
    int x0 = 0;
    int y0 = 0;
    int x1 = ConCharWdt;
    int y1 = ConCharHgt;
    float tx0 = x/iw, ty0 = y/ih;
    float tx1 = (x+ConCharWdt)/iw, ty1 = (y+ConCharHgt)/ih;
    glBegin(GL_QUADS);
      glTexCoord2f(tx0, ty0); glVertex2i(x0, y0); // top-left
      glTexCoord2f(tx1, ty0); glVertex2i(x1, y0); // top-right
      glTexCoord2f(tx1, ty1); glVertex2i(x1, y1); // bottom-right
      glTexCoord2f(tx0, ty1); glVertex2i(x0, y1); // bottom-left
    glEnd();
    // move coords
    glTranslatef(ConCharWdt, 0, 0);
    // done
    glEndList();
  }
}

public int conCharHeight () { pragma(inline, true); return ConCharHgt; }
public int conCharWidth (char ch) { pragma(inline, true); return ConCharWdt; }

public void conDrawChar (char ch) {
  bindTexture(conFontTex.id);
  glCallList(cast(uint)ch+conListBase);
}
