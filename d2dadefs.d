/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module d2dadefs is aliced;

private:
import iv.glbinds;
import iv.cmdcon;
import glutils;
import dacs;
import d2dimage;
import d2dsprite;


// ////////////////////////////////////////////////////////////////////////// //
private enum BIT(ubyte n) = (1U<<n);


// ////////////////////////////////////////////////////////////////////////// //
// thing flags
public enum {
  AF_NOCOLLISION = BIT!(0),
  AF_NOGRAVITY   = BIT!(1),
  AF_NOTHINK     = BIT!(2),
  AF_NOONTOUCH   = BIT!(3), // `onTouch` will never be called with `me` for this actor
  AF_NODRAW      = BIT!(4), // don't draw sprite
  AF_NOLIGHT     = BIT!(5), // no attached light
  AF_NOANIMATE   = BIT!(6), // don't do animation
  AF_PIXEL       = BIT!(7), // draw pixel, color is `s`
  AF_TELEPORT    = BIT!(8), // don't interpolate camera; will be autoreset on next frame
}


// ////////////////////////////////////////////////////////////////////////// //
// known D2D actors (and pseudoactors)
struct ActorDefD2D {
  StrId classtype;
  StrId classname;
  ushort mapid; // thing id in map

  this (string ctype, string cname, ushort thid=0) {
    classtype = StrPool.intern(ctype);
    classname = StrPool.intern(cname);
    mapid = thid;
  }

  string toString () const {
    import std.string : format;
    return "ActorDefD2D(%s, %s, %s)".format(classtype.get, classname.get, mapid);
  }
}

immutable ActorDefD2D[] d2dactordefs;
public immutable ActorDefD2D[ushort] d2dactordefsById; // by mapid

shared static this () {
  d2dactordefs = [
    ActorDefD2D("playerstart", "Player1", 1),
    ActorDefD2D("playerstart", "Player2", 2),
    ActorDefD2D("playerstart", "DMStart", 3),
    ActorDefD2D("item", "Clip", 100),
    ActorDefD2D("item", "Shell", 101),
    ActorDefD2D("item", "Rocket", 102),
    ActorDefD2D("item", "Cell", 103),
    ActorDefD2D("item", "Ammo", 104),
    ActorDefD2D("item", "ShellBox", 105),
    ActorDefD2D("item", "RocketBox", 106),
    ActorDefD2D("item", "CellPack", 107),
    ActorDefD2D("item", "StimPack", 108),
    ActorDefD2D("item", "MediKit", 109),
    ActorDefD2D("item", "BackPack", 110),
    ActorDefD2D("item", "Chainsaw", 111),
    ActorDefD2D("item", "Shotgun", 112),
    ActorDefD2D("item", "SuperShotgun", 113),
    ActorDefD2D("item", "MachineGun", 114),
    ActorDefD2D("item", "RocketLauncher", 115),
    ActorDefD2D("item", "Plasmagun", 116),
    ActorDefD2D("item", "BFG9000", 117),
    ActorDefD2D("item", "ArmorGreen", 118),
    ActorDefD2D("item", "ArmorBlue", 119),
    ActorDefD2D("item", "MegaSphere", 120),
    ActorDefD2D("item", "Invulnerability", 121),
    ActorDefD2D("item", "Aqualung", 122),
    ActorDefD2D("item", "KeyRed", 123),
    ActorDefD2D("item", "KeyGreen", 124),
    ActorDefD2D("item", "KeyBlue", 125),
    ActorDefD2D("item", "ProtectionSuit", 126),
    ActorDefD2D("item", "Super", 127),
    ActorDefD2D("item", "TorchRed", 128),
    ActorDefD2D("item", "TorchGreen", 129),
    ActorDefD2D("item", "TorchBlue", 130),
    ActorDefD2D("item", "Gor1", 131),
    ActorDefD2D("item", "FCan", 132),
    ActorDefD2D("item", "Gun2", 133),
    ActorDefD2D("monster", "Demon", 200),
    ActorDefD2D("monster", "Imp", 201),
    ActorDefD2D("monster", "Zombie", 202),
    ActorDefD2D("monster", "Sergeant", 203),
    ActorDefD2D("monster", "Cyberdemon", 204),
    ActorDefD2D("monster", "Chaingunner", 205),
    ActorDefD2D("monster", "BaronOfHell", 206),
    ActorDefD2D("monster", "HellKnight", 207),
    ActorDefD2D("monster", "Cacodemon", 208),
    ActorDefD2D("monster", "LostSoul", 209),
    ActorDefD2D("monster", "PainElemental", 210),
    ActorDefD2D("monster", "SpiderMastermind", 211),
    ActorDefD2D("monster", "Arachnotron", 212),
    ActorDefD2D("monster", "Mancubus", 213),
    ActorDefD2D("monster", "Revenant", 214),
    ActorDefD2D("monster", "Archvile", 215),
    ActorDefD2D("monster", "Fish", 216),
    ActorDefD2D("monster", "Barrel", 217),
    ActorDefD2D("monster", "Robot", 218),
    ActorDefD2D("monster", "Man", 219),
  ];
  foreach (const ref ActorDefD2D d2da; d2dactordefs) {
    if (d2da.mapid) d2dactordefsById[d2da.mapid] = d2da;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public string getD2DSwitchClassName (uint swtype) {
  switch (swtype) {
    case 1: return "Exit"; // SW_EXIT
    case 2: return "ExitSecret"; // SW_EXITS
    case 3: return "DoorOpen"; // SW_OPENDOOR
    case 4: return "DoorClose"; // SW_SHUTDOOR
    case 5: return "TrapClose"; // SW_SHUTTRAP
    case 6: return "Door"; // SW_DOOR
    case 7: return "Door5"; // SW_DOOR5
    case 8: return "Press"; // SW_PRESS
    case 9: return "Teleport"; // SW_TELE
    case 10: return "Secret"; // SW_SECRET
    case 11: return "LiftUp"; // SW_LIFTUP
    case 12: return "LiftDown"; // SW_LIFTDOWN
    case 13: return "Trap"; // SW_TRAP
    case 14: return "Lift"; // SW_LIFT
    case 64: return "WinGame"; // SW_WINGAME
    default:
  }
  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
public bool processCL (ref string[] args) {
  static string h() (const(void)[] src) {
    static immutable uint[5] g = [0x31c84b1, 0x95eed, 0x1c39, 0x55, 0x1];
    uint a = 0; int c = 0; auto d = cast(const(ubyte)[])src; string e; ubyte f = 0x5a;
    foreach (immutable b; d) { a += (b-0x21)*g[c++]; if (c == 5) { foreach (immutable _; 0..4) { e ~= ((a>>24)^(f^=0xff))&0xff; a <<= 8; } a = 0; c = 0; } }
    if (c > 1) { a += g[--c]; foreach (immutable _; 0..c) { e ~= ((a>>24)^(f^=0xff))&0xff; a <<= 8; } }
    return e;
  }

  for (usize idx = 1; idx < args.length; ++idx) {
    string s = null;
    if (args[idx] == h("Lj-`b^coRZ")) {
      s = h("l86;\"KeCe2M0e21^coRZJglC6au@O$_Ks*Wf6Z40cqs6ff0@&oKfmL2f62ib^b4#!f6YOr`\\"~
        "E/latM@m_)9@`Y8kb;^ikD\\bT_mi_C3>^KfauKf.Y-\"__^+jY6huq^bgmo^b`h_d<:3aau%:'K"~
        "dkD2gj7<sKf6k%^iZaobV[+YKeC.tM0eJPdm)H1ei(<.eU#.s`&#SYMfc-");
    } else if (args[idx] == h("Lj.0,a#Lj_dP]$!g]")) {
      s = h("`=C<Xep>@bbVmR!_KLVpb<CV4bs-n2aYV=\"gj7@*dm)D[KmM]6_0WsCf0=sNb<CVCbW!R#^j="~
                 "3]KeCV7Ke:5&^gO>baYni3bW\")tKe:(g^j=B`^j=$Yf/:E&dm)E,YBkmAb<CV4bs-n@au@+^f/"~
                 "qA8ep>Ild66)Ca$5JYY>Buk^b*JQ^j=B`^j>H2a#Lj_dP]$!M0d5fKdk/$KM[AKnM7n+cTBQ(Ke"~
                 "CV3d<`hhd6EJ6d5&L``\\taeep>_!`%Ha(`%K%bKM^hrdQ?&FKeCV7Kb2TjcS*=ba\"u=2M0d)s_"~
                 "0XZfcnqqPKau?lKfms?a>:slep?L@b<E]iehk;t`\\N3lY>FUNKeCM*c?d_kep>e&f.+]sKdkG-f"~
                 "04mEbBhDpb<DOpKmMu@b<:P3bW!=`d5&L`a?.Qhau@+^__o5Ubs-n2atLUed<^UmY6i?tf0@h=^cmH"~
                 "-^j=$GKeCV7KauHgd4`Red3udpM,8nPj>=]!eh5*&_0X$ObqmI+Kepnud5&it_D9M\\Y>F::_01Mo"~
                 "ep>OsKeL>+bULe_d5&it_D9Mbbs-n2d5B'1M,8nPnM7n+mlSQL^aRVg`\\E<rdPK9+Kg4?Wep>J$Kf"~
                 "mI=a?PMXju^XKKe:P*_`Z$sb<:P3bp^^f_D6ftbs-n7co/a*gj7C+b!(YYKmMf:_0X-Tf/Ul:_0X$KK"~
                 "eq,&`]M7+KM\\ieb<Li0d4NLfKb_cqM0dQ$b'Sg]b<CV4bp^^f_D6g,b<CV4bp^^f_D8<@Kau?lKe1@"~
                 "peN:m!a#:k\"^j=Enb:\\Jtb;7-m^aP!fb<*]uKM_&#Kf6_/d<a\"hKg*:6_01Mob<CV3bV%.dbV%Im"~
                 "bs0-1YBkm8^j=Enb:\\K3`&>t$eM>?*_Ks6a`\\EBJYB]+b_01Mf`$pBmbr*joei(H&bBhPk^j>H2a"~
                 "#Lj_dP]$!RS\\]aju(4EKeCMoa<QC#f/M9(Kf@e5a$7L$bULe_a#2Y!Y8)aRf/q#,atM=l_)pG!Kc."~
                 "feaYh3rKR2cBaYq'^KRL%MKbD1I`\\P[bd5Jd(KepnubqmpscSaL\"Y>DD`nL_[.KfI[?aYq@+M0d)s_"~
                 "0XKa^j=QibV%-bKeCM!^aZI5Kc\\H%Kg\"3>^j<pU_0WsSa>h:+Kepnud5&L`dQ>K6Sl\"K+aa2#aa=5n"~
                 "jdQc>JKeCV7KfI4'b!!lu^aY+:KmMf:_0XKa^j='Za?PA,`%K:`Kf.40_C3>^^iZa");
    } else if (args[idx] == h("Lj.<*f.+]s") || args[idx] == h("Lj.<*f.+]seh,#m_#") || args[idx] == h("Lj.<*f.+]sa#:k\"^]")) {
      s = h("r&)iI_)9Dad<a&!dm&Nrbs-mjasbe%aa3,.`'1tYY5");
    } else if (args[idx] == h("Lj.E7dm)\"")) {
      s = h("p-7\"jbBh)T^j=B`^a\"\\^ej%VJbBh)T^j=B`^a\"M/lnjDKbs-nBbra**^c$m(_g9]c^j<g]^"~
        "ab$e^b!8XKfdmNKeCV7gj?4<bBhPk^j=To_)'\"d`]o/7_g9Ql`%8\\\\Kdjeg^bga,f0=sAbs1!1Y7o**d5&"~
        "L``\\PIhb<O*paukA1bs-nG^d$@qbVY#8Kdt#!`&YVZ_C32OKeCV7gj7<sMbrV!aZkJ3bs1!0`$iEIb;I[jKfm"~
        ".0`&>DWd5&K^KfdmNKf$q)MbrasKeCV7Kf@4>eh2Y6_g9]c^j<g]^ab%\"f0=sAbs0E#Kdt\"nbpeQInh\\0JK"~
        "eCV7gj74,dm&OCatANsehtWsd5&K^KegRt^j=0]d<)Q,f06tuKfdIAKe^e._)/qVep>Uuat8<\"bs1!0b;I[hY"~
        "6rlCgcsD:`&,;Veh>Q'aYUpud5/p%KegG*f6Yh\"^a\"OiatATKnM7n+`]MX6f//1;bBhPk^j='Zcns-3bVY#+"~
        "bs1!0^bjG'f6\"2,b:^q`m6AO8`$iEId5&K^Kege\"d5/m/Mbrq!^j=B`f/:`9LcE13f0@k9Kf-bod5&L`_DT."~
        "YKfdmNKeCV7giT_@bULpmf/Uu2_KsTb^j=*YaWlKqbBhPk^j=Bcf-nEkd4'c2f0=sAbs1!0ej%Q(Y8G29^c]t%"~
        "KfdmNKeCV7gieB2`&>Cnco-S>f0=sAbs1!*Y6huq^bga,f0=sAbs1!,Kdjeg^bga,f0=sAbs1!,Kdjeg^bg`td"~
        "PJm-Kfd.5bVY#8f0=sAbs1!*Y8>,%KeCV7KdkP.f/(f'Kf-bobs0,t^bg`n^c0%fa$81!Kf?ppKeCV7KdkP.f/"~
        "(f'Kf-bo^cU.#c$Iu+a?Ra*^aP+;");
    } else if (args[idx] == h("Lj-lsf0'")) {
      s = h("niqgif0=sAbs-n!asb,KKbhf_^aOm]atM@mehYH6aZe:%`&,;Veh>W'^biklf6Z40cmXSsbqmm!Mbrt/"~
        "Ke:M,aa1lh_0XlocmXT-bVdL\"_DB1naa1l^_0XKn`\\P^paYgpk`]M@i_)'XpbV[<lbUgOd^a[i\\d5?V8cmXT*"~
        "dPQkYr&)hLKfRL<d4a*9Ke:M,aa1lh`%'/\\d5?V,^aRYf`]o/1eo\\)$^a[JVd6#N3^j=3RKfm^7bVm7YY5");
    } else if (args[idx] == h("Lj-]nauI0bec") || args[idx] == h("Lcrm<a$80)")) {
    }
    if (s.length) {
      import core.stdc.stdio : printf;
      printf("%.*s", cast(uint)s.length, s.ptr);
      foreach (immutable c; idx+1..args.length) args[c-1] = args[c];
      args.length = args.length-1;
      --idx;
      return true;
    }
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
public final class ActorDef {
  StrId classtype;
  StrId classname;

  // animation sequences for stated; keyed by state strid
  StrId[][][uint] anims; // [statestrid][dir][pos]

  FuncPool.FuncInfo fiAnimInit;
  FuncPool.FuncInfo fiInit;
  FuncPool.FuncInfo fiThink;

  ImgSprite[][][uint] animSprites;

  // load actor graphics
  void loadGraphics () {
    //conwriteln("loading graphics for '", fullname, "'; anims.length=", anims.length");
    if (anims.length == 0) return; // no graphics here
    // load sprites
    foreach (auto anma2; anims.byKeyValue) {
      //if (anma2.key !in animSprites) animSprites[anma2.key] = new ImgSprite[][](2); // 2 dirs
      //auto aspx = animSprites[anma2.key];
      if (anma2.key in animSprites) continue;
      ImgSprite[][] aspx;
      foreach (immutable dir, auto anma; anma2.value) {
        ImgSprite[] dsp;
        foreach (StrId ssid; anma) {
          //if (dir) { conwriteln("dir: ", dir, "; [", ssid.get, "]"); }
          dsp ~= loadSprite(ssid.get);
        }
        aspx ~= dsp;
      }
      animSprites[anma2.key] = aspx;
    }
  }

  // unload actor graphics
  void releaseGraphics () {
    animSprites = null;
  }

  int nextAnimIdx (StrId state, uint dir, int curidx) {
    if (++curidx <= 0) return 0;
    if (auto spra = state.id in animSprites) {
      if (dir != 0) dir = 1;
      return (curidx < (*spra).ptr[dir].length ? curidx : 0);
    }
    return 0;
  }

  ImgSprite animSpr (StrId state, uint dir, int curidx) {
    if (curidx < 0) return null;
    if (auto spra = state.id in animSprites) {
      if (dir != 0) dir = 1;
      auto px = (*spra).ptr[dir];
      return (curidx < px.length ? px.ptr[curidx] : null);
    }
    return null;
  }

  this (string ctype, string cname) {
    classtype = StrPool.intern(ctype);
    classname = StrPool.intern(cname);
  }

  void clearAllFrames (StrId state) {
    anims.clear();
  }

  void clearFrames (StrId state) {
    anims.remove(state.id);
  }

  void addFrame (StrId state, uint dir, StrId sprname) {
    //conwriteln("new frame for '", fullname, "'; state=", state.get, "; sprname=", sprname.get, "; dir=", dir);
    if (dir > 1) throw new Exception("addFrame: invalid dir");
    if (state.id !in anims) anims[state.id] = new StrId[][](2);
    auto aa = anims[state.id];
    aa[dir] ~= sprname;
  }

  void setAnimInitFunc (FuncPool.FuncInfo fi) {
    if (fi is null) return;
    if (fi.mgtype != ":void") throw new Exception("invalid actor animation init function");
    fiAnimInit = fi;
  }

  void setInitFunc (FuncPool.FuncInfo fi) {
    if (fi is null) return;
    if (fi.mgtype != ":void:Actor") throw new Exception("invalid actor init function");
    fiInit = fi;
  }

  void setThinkFunc (FuncPool.FuncInfo fi) {
    if (fi is null) return;
    if (fi.mgtype != ":void:Actor") throw new Exception("invalid actor think function");
    fiThink = fi;
  }

  void callAnimInit () {
    if (fiAnimInit !is null) fiAnimInit();
  }

  void callInit (ActorId aid) {
    if (fiInit is null || !aid.valid) return;
    fiInit(aid);
  }

  void callThink (ActorId aid) {
    if (fiThink is null || !aid.valid) return;
    fiThink(aid);
  }

static:
  // return `true` to stop
  bool forEach (bool delegate (ActorDef d) dg) {
    foreach (ActorDef adef; actordefs.byValue) if (dg(adef)) return true;
    return false;
  }

  void forEach (void delegate (ActorDef d) dg) {
    foreach (ActorDef adef; actordefs.byValue) dg(adef);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public ActorDef findD2DActorDef (ushort mapid) {
  if (mapid == 0) assert(0);
  if (mapid == 1 || mapid == 2) return findActorDef("monster", "Player");
  if (auto dad = mapid in d2dactordefsById) {
    auto adef = findActorDef(dad.classtype, dad.classname);
    if (adef is null) throw new Exception("can't find DACS actor definition for D2D actor '"~dad.classtype.get~":"~dad.classname.get~"'");
    return adef;
  }
  import std.conv : to;
  throw new Exception("can't find D2D ActorDef for mapid "~to!string(mapid));
}


// ////////////////////////////////////////////////////////////////////////// //
import dacs.actor;

__gshared ActorDef[ulong] actordefs; // by (name<<32)|type

mixin(Actor.FieldGetMixin!("classtype", StrId)); // fget_classtype
mixin(Actor.FieldGetMixin!("classname", StrId)); // fget_classname


ulong ctn2idx (uint ctid, uint cnid) { pragma(inline, true); return ((cast(ulong)cnid)<<32)|ctid; }
ulong ctn2idx (StrId ct, StrId cn) { pragma(inline, true); return ctn2idx(ct.id, cn.id); }


public ActorDef findActorDef (ActorId aid) {
  pragma(inline, true);
  if (!aid.valid) return null;
  return findActorDef(aid.fget_classtype, aid.fget_classname);
}


public ActorDef findActorDef (StrId ctype, StrId cname) { /*pragma(inline, true);*/ return actordefs.get(ctn2idx(ctype.id, cname.id), null); }


public ActorDef findActorDef (const(char)[] ctype, const(char)[] cname) {
  auto ctid = ctype in StrPool;
  auto cnid = cname in StrPool;
  if (!ctid || !cnid) return null;
  return actordefs.get(ctn2idx(ctid, cnid), null);
}


public ActorDef registerActorDef (string classtype, string classname) {
  auto ct = StrPool.intern(classtype);
  auto cn = StrPool.intern(classname);
  if (findActorDef(ct, cn) is null) actordefs[ctn2idx(ct, cn)] = new ActorDef(classtype, classname);
  return actordefs[ctn2idx(ct, cn)];
}
