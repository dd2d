module wadarc is aliced;
private:

static import core.sync.mutex;
import std.stdio : File;
//import arsd.characterencodings;

import console;

import iv.encoding;


// ////////////////////////////////////////////////////////////////////////// //
public string koi8lotranslit (string s) {
  usize pos = 0;
  while (pos < s.length && s.ptr[pos] < 0x80) ++pos;
  if (pos >= s.length) return s; // nothing to do
  string res = s[0..pos];
  while (pos < s.length) {
    char ch = s.ptr[pos++];
         if (ch == '\xe1' || ch == '\xc1') res ~= "a";
    else if (ch == '\xe2' || ch == '\xc2') res ~= "b";
    else if (ch == '\xf7' || ch == '\xd7') res ~= "v";
    else if (ch == '\xe7' || ch == '\xc7') res ~= "g";
    else if (ch == '\xe4' || ch == '\xc4') res ~= "d";
    else if (ch == '\xe5' || ch == '\xc5') res ~= "e";
    else if (ch == '\xb3' || ch == '\xa3') res ~= "yo";
    else if (ch == '\xf6' || ch == '\xd6') res ~= "zh";
    else if (ch == '\xfa' || ch == '\xda') res ~= "z";
    else if (ch == '\xe9' || ch == '\xc9') res ~= "i";
    else if (ch == '\xea' || ch == '\xca') res ~= "j";
    else if (ch == '\xeb' || ch == '\xcb') res ~= "k";
    else if (ch == '\xec' || ch == '\xcc') res ~= "l";
    else if (ch == '\xed' || ch == '\xcd') res ~= "m";
    else if (ch == '\xee' || ch == '\xce') res ~= "n";
    else if (ch == '\xef' || ch == '\xcf') res ~= "o";
    else if (ch == '\xf0' || ch == '\xd0') res ~= "p";
    else if (ch == '\xf2' || ch == '\xd2') res ~= "r";
    else if (ch == '\xf3' || ch == '\xd3') res ~= "s";
    else if (ch == '\xf4' || ch == '\xd4') res ~= "t";
    else if (ch == '\xf5' || ch == '\xd5') res ~= "u";
    else if (ch == '\xe6' || ch == '\xc6') res ~= "f";
    else if (ch == '\xe8' || ch == '\xc8') res ~= "h";
    else if (ch == '\xe3' || ch == '\xc3') res ~= "c";
    else if (ch == '\xfe' || ch == '\xde') res ~= "ch";
    else if (ch == '\xfb' || ch == '\xdb') res ~= "sh";
    else if (ch == '\xfd' || ch == '\xdd') res ~= "sch";
    else if (ch == '\xff' || ch == '\xdf') res ~= "x";
    else if (ch == '\xf9' || ch == '\xd9') res ~= "y";
    else if (ch == '\xf8' || ch == '\xd8') res ~= "w";
    else if (ch == '\xfc' || ch == '\xdc') res ~= "e";
    else if (ch == '\xe0' || ch == '\xc0') res ~= "hu";
    else if (ch == '\xf1' || ch == '\xd1') res ~= "ja";
    else res ~= ch;
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
public immutable char[256] koi8from866Table = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\xe1','\xe2','\xf7','\xe7','\xe4','\xe5','\xf6','\xfa','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef','\xf0',
  '\xf2','\xf3','\xf4','\xf5','\xe6','\xe8','\xe3','\xfe','\xfb','\xfd','\xff','\xf9','\xf8','\xfc','\xe0','\xf1',
  '\xc1','\xc2','\xd7','\xc7','\xc4','\xc5','\xd6','\xda','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf','\xd0',
  '\x90','\x91','\x92','\x81','\x87','\xb2','\x3f','\x3f','\x3f','\xb5','\xa1','\xa8','\xae','\x3f','\xac','\x83',
  '\x84','\x89','\x88','\x86','\x80','\x8a','\xaf','\xb0','\xab','\xa5','\xbb','\xb8','\xb1','\xa0','\xbe','\xb9',
  '\xba','\x3f','\x3f','\xaa','\xa9','\xa2','\x3f','\x3f','\xbc','\x85','\x82','\x8d','\x8c','\x8e','\x8f','\x8b',
  '\xd2','\xd3','\xd4','\xd5','\xc6','\xc8','\xc3','\xde','\xdb','\xdd','\xdf','\xd9','\xd8','\xdc','\xc0','\xd1',
  '\xb3','\xa3','\xb4','\xa4','\xb7','\xa7','\x3f','\x3f','\x9c','\x95','\x9e','\x96','\x3f','\x3f','\x94','\x9a',
];

public immutable char[256] koi8from1251Table = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f',
  '\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f','\x3f',
  '\x9a','\x3f','\x3f','\x3f','\x3f','\xbd','\x3f','\x3f','\xb3','\xbf','\xb4','\x3f','\x3f','\x3f','\x3f','\xb7',
  '\x9c','\x3f','\xb6','\xa6','\xad','\x3f','\x3f','\x9e','\xa3','\x3f','\xa4','\x3f','\x3f','\x3f','\x3f','\xa7',
  '\xe1','\xe2','\xf7','\xe7','\xe4','\xe5','\xf6','\xfa','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef','\xf0',
  '\xf2','\xf3','\xf4','\xf5','\xe6','\xe8','\xe3','\xfe','\xfb','\xfd','\xff','\xf9','\xf8','\xfc','\xe0','\xf1',
  '\xc1','\xc2','\xd7','\xc7','\xc4','\xc5','\xd6','\xda','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf','\xd0',
  '\xd2','\xd3','\xd4','\xd5','\xc6','\xc8','\xc3','\xde','\xdb','\xdd','\xdf','\xd9','\xd8','\xdc','\xc0','\xd1',
];

// char toupper/tolower, koi8
public immutable char[256] koi8tolowerTable = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8a','\x8b','\x8c','\x8d','\x8e','\x8f',
  '\x90','\x91','\x92','\x93','\x94','\x95','\x96','\x97','\x98','\x99','\x9a','\x9b','\x9c','\x9d','\x9e','\x9f',
  '\xa0','\xa1','\xa2','\xa3','\xa4','\xa5','\xa6','\xa7','\xa8','\xa9','\xaa','\xab','\xac','\xad','\xae','\xaf',
  '\xb0','\xb1','\xb2','\xa3','\xa4','\xb5','\xa6','\xa7','\xb8','\xb9','\xba','\xbb','\xbc','\xad','\xbe','\xbf',
  '\xc0','\xc1','\xc2','\xc3','\xc4','\xc5','\xc6','\xc7','\xc8','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf',
  '\xd0','\xd1','\xd2','\xd3','\xd4','\xd5','\xd6','\xd7','\xd8','\xd9','\xda','\xdb','\xdc','\xdd','\xde','\xdf',
  '\xc0','\xc1','\xc2','\xc3','\xc4','\xc5','\xc6','\xc7','\xc8','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf',
  '\xd0','\xd1','\xd2','\xd3','\xd4','\xd5','\xd6','\xd7','\xd8','\xd9','\xda','\xdb','\xdc','\xdd','\xde','\xdf',
];

public immutable char[256] koi8toupperTable = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8a','\x8b','\x8c','\x8d','\x8e','\x8f',
  '\x90','\x91','\x92','\x93','\x94','\x95','\x96','\x97','\x98','\x99','\x9a','\x9b','\x9c','\x9d','\x9e','\x9f',
  '\xa0','\xa1','\xa2','\xb3','\xb4','\xa5','\xb6','\xb7','\xa8','\xa9','\xaa','\xab','\xac','\xbd','\xae','\xaf',
  '\xb0','\xb1','\xb2','\xb3','\xb4','\xb5','\xb6','\xb7','\xb8','\xb9','\xba','\xbb','\xbc','\xbd','\xbe','\xbf',
  '\xe0','\xe1','\xe2','\xe3','\xe4','\xe5','\xe6','\xe7','\xe8','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef',
  '\xf0','\xf1','\xf2','\xf3','\xf4','\xf5','\xf6','\xf7','\xf8','\xf9','\xfa','\xfb','\xfc','\xfd','\xfe','\xff',
  '\xe0','\xe1','\xe2','\xe3','\xe4','\xe5','\xe6','\xe7','\xe8','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef',
  '\xf0','\xf1','\xf2','\xf3','\xf4','\xf5','\xf6','\xf7','\xf8','\xf9','\xfa','\xfb','\xfc','\xfd','\xfe','\xff',
];

public immutable ubyte[32] koi8alphaTable = [
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xfe,0xff,0xff,0x07,0xfe,0xff,0xff,0x07,
  0x00,0x00,0x00,0x00,0xd8,0x20,0xd8,0x20,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
];

public char dos2koi8 (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return koi8from866Table.ptr[cast(int)ch];
}

public char koi8lower (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return koi8tolowerTable.ptr[cast(int)ch];
}

public char koi8upper (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return koi8toupperTable.ptr[cast(int)ch];
}

public bool koi8isAlpha (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return ((koi8alphaTable.ptr[ch/8]&(1<<(ch%8))) != 0);
}


// ////////////////////////////////////////////////////////////////////////// //
public __gshared WadArchive[] wadList;
__gshared core.sync.mutex.Mutex glock;
__gshared string dataPath;


shared static this () {
  glock = new core.sync.mutex.Mutex;
}


public void setDataPath (const(char)[] path) {
  while (path.length > 1 && path[0] == '/' && path[1] == '/') path = path[1..$];
  if (path != "/") while (path.length && path[$-1] == '/') path = path[0..$-1];
  dataPath = path.idup~"/";
}


public void addWad (string fname) {
  if (glock !is null) glock.lock();
  scope(exit) if (glock !is null) glock.unlock();
  conwriteln("; adding '", fname, "'...");
  wadList ~= new WadArchive(fname);
}


public File openFile (string fname) {
  if (glock !is null) glock.lock();
  scope(exit) if (glock !is null) glock.unlock();
  try {
    return File(dataPath~fname);
  } catch (Exception) {}
  // now try wads
  foreach_reverse (WadArchive wad; wadList) {
    try {
      return wad.fopen(fname);
    } catch (Exception) {}
  }
  return File(fname); // to throw a correct exception, lol
  /*
  string ofname = fname;
  import std.path;
  fname = fname.baseName.setExtension("");
  foreach_reverse (WadArchive wad; wadList) {
    try {
      return wad.fopen(fname);
    } catch (Exception) {}
  }
  return File(ofname); // to throw a correct exception, lol
  */
}


public string loadTextFile (string fname) {
  auto fl = openFile(fname);
  auto sz = fl.size;
  if (sz < 0 || sz > 1024*1024) throw new Exception("invalid text file size: '"~fname~"'");
  if (sz == 0) return null;
  auto res = new char[](cast(uint)sz);
  if (fl.rawRead(res[]).length != res.length) throw new Exception("error reading text file '"~fname~"'");
  import std.exception : assumeUnique;
  return res.assumeUnique;
}


// ////////////////////////////////////////////////////////////////////////// //
final class WadArchive {
public:
  import std.stdio : File;

private:
  static struct FileInfo {
    uint ofs;
    uint size;
    string path;
    string name;
  }

  // for dir range
  public static struct DirEntry {
    string path;
    string name;
    uint size;
  }

private:
  File zfl;
  ulong flstpos;
  FileInfo[] dir;

public:
  this (string fname) {
    import std.stdio : File;
    initLock();
    zfl = File(fname);
    open(zfl);
    scope(failure) { zfl.close; zfl = zfl.init; }
  }

  // it now owns the file (if no exception was thrown)
  this (File fl) {
    initLock();
    open(fl);
    scope(success) zfl = fl;
  }

  @property auto files () {
    static struct Range {
    private:
      WadArchive me;
      uint curindex;

    nothrow @safe @nogc:
      this (WadArchive ame, uint aidx=0) { me = ame; curindex = aidx; }

    public:
      @property bool empty () const { return (curindex >= me.dir.length); }
      @property DirEntry front () const {
        return DirEntry(
          (curindex < me.dir.length ? me.dir[cast(usize)curindex].path : null),
          (curindex < me.dir.length ? me.dir[cast(usize)curindex].name : null),
          (curindex < me.dir.length ? me.dir[cast(usize)curindex].size : 0));
      }
      @property Range save () { return Range(me, curindex); }
      void popFront () { if (curindex < me.dir.length) ++curindex; }
      @property uint length () const { return me.dir.length; }
      @property uint position () const { return curindex; } // current position
      @property void position (uint np) { curindex = np; }
      void rewind () { curindex = 0; }
    }
    return Range(this);
  }

  File fopen (ref in DirEntry de) {
    foreach (immutable idx, ref fi; dir) {
      if (fi.path == de.path && fi.name == de.name) return openDirEntry(idx, fi.name);
    }
    throw new NamedException!"WadArchive"("file not found");
  }

  File fopen (const(char)[] fname) {
    DirEntry de;
    auto pos = fname.length;
    while (pos > 0 && fname[pos-1] != '/') --pos;
    if (pos) {
      de.path = cast(string)fname[0..pos]; // it's safe here
      de.name = cast(string)fname[pos..$]; // it's safe here
      //conwriteln("[", de.path, "] [", de.name, "]");
    } else {
      de.name = cast(string)fname; // it's safe here
    }
    return fopen(de);
  }

private:
  void cleanup () {
    dir.length = 0;
    if (zfl.isOpen) zfl.close;
    zfl = zfl.init;
  }

  void open (File fl) {
    import std.uni : icmp;

    immutable string[$] msn = [
      "SARG", "TROO", "POSS", "SPOS", "CYBR", "CPOS", "BOSS", "BOS2", "HEAD", "SKUL",
      "PAIN", "SPID", "BSPI", "FATT", "SKEL", "VILE", "FISH", "BAR1", "ROBO", "PLAY"
    ];

    string curpath;

    string fixName (const(char)[] name) {
      if (name.length >= 4 && name[0..4] == "stcf") return name.idup~".vga";
      if (name.length >= 4 && name[0..4] == "stbf") return name.idup~".vga";
      if (name.length >= 5 && name[0..5] == "winum") return name.idup~".vga";
      if (name == "wicolon" || name == "wiminus" || name == "wipcnt") return name.idup~".vga";
      if (name.length > 3 && name[0..3] == "map") return name.idup~".d2m";
      if (name == "playpal") return "playpal.pal";
      switch (name) {
        case "endoom": return "endoom.b80";
        case "endanim": return "endanim.a8";
        case "end2anim": return "end2anim.a8";
        case "darts": return "darts.a8";
        case "colormap": return "colormap.tbl";
        case "mixmap": return "mixmap.tbl";
        case "titlepic": return "titlepic.vga";
        case "interpic": return "interpic.vga";
        case "cd1pic": return "cd1pic.vga";
        case "endpic": return "endpic.vraw";
        case "m_therml": return "m_therml.vga";
        case "m_thermm": return "m_thermm.vga";
        case "m_thermo": return "m_thermo.vga";
        case "m_thermr": return "m_thermr.vga";
        case "m_lscntr": return "m_lscntr.vga";
        case "m_lsleft": return "m_lsleft.vga";
        case "m_lsrght": return "m_lsrght.vga";
        default:
      }

      import std.algorithm;
      if (curpath == "sounds/") return name.idup~".snd";
      if (curpath == "music/") return (name.length > 3 && name[0..3] == "dmi" ? name.idup~".dmi" : name.idup~".dmm");
      if (curpath == "tilegfx/") return name.idup~".vga";
      if (curpath.startsWith("sprites/")) return name.idup~".vga";
      return name.idup;
    }

    string fixPath (const(char)[] name) {
      switch (name) {
        case "m_therml":
        case "m_thermm":
        case "m_thermo":
        case "m_thermr":
        case "m_lscntr":
        case "m_lsleft":
        case "m_lsrght":
          return "menugfx/";
        case "rsky1":
        case "rsky2":
        case "rsky3":
          return "sprites/sky/";
        case "endanim":
        case "end2anim":
        case "darts":
          return "movies/";
        default:
      }
      if (name.length >= 4 && name[0..4] == "stcf") return "fonts/stcf/";
      if (name.length >= 4 && name[0..4] == "stbf") return "fonts/stbf/";
      if (name.length >= 5 && name[0..5] == "winum") return "fonts/winum/";
      if (name.length >= 3 && name[0..3] == "stt") return "fonts/stt/";
      if (name == "wicolon" || name == "wiminus" || name == "wipcnt") return "fonts/winum/";
      if (name.length > 3 && name[0..3] == "map") return "maps/";
      if (name == "d_start") curpath = "sounds/";
      if (name == "m_start") curpath = "music/";
      if (name == "w_start") curpath = "tilegfx/";
      if (name == "s_start") curpath = "sprites/";
      if (curpath == "sprites/" && name.length > 4) {
        switch (name[0..4]) {
          case "sarg": return "sprites/monsters/demon/";
          case "troo": return "sprites/monsters/imp/";
          case "poss": return "sprites/monsters/zombie/";
          case "spos": return "sprites/monsters/sergeant/";
          case "cybr": return "sprites/monsters/cyberdemon/";
          case "cpos": return "sprites/monsters/chaingunner/";
          case "boss": return "sprites/monsters/baron/";
          case "bos2": return "sprites/monsters/knight/";
          case "head": return "sprites/monsters/cacodemon/";
          case "skul": return "sprites/monsters/soul/";
          case "pain": return "sprites/monsters/painel/";
          case "spid": return "sprites/monsters/mastermind/";
          case "bspi": return "sprites/monsters/arachnotron/";
          case "fatt": return "sprites/monsters/mancubus/";
          case "skel": return "sprites/monsters/revenant/";
          case "vile": return "sprites/monsters/archvile/";
          case "fish": return "sprites/monsters/fish/";
          case "bar1": return "sprites/monsters/barrel/";
          case "robo": return "sprites/monsters/robot/";
          case "play": return "sprites/monsters/player/";
          default:
        }
      }
      return curpath;
    }

    import core.stdc.stdio : SEEK_CUR, SEEK_END;
    scope(failure) cleanup();

    uint readU32 () {
      ubyte[4] data;
      if (fl.rawRead(data[]).length != data.length) throw new NamedException!"WadArchive"("reading error");
      return cast(uint)(data[0]+0x100*data[1]+0x10000*data[2]+0x1000000*data[3]);
    }

    uint lmpofs;
    uint lmpsize;
    char[8] lmpname;

    flstpos = fl.tell;

    if (fl.rawRead(lmpname[0..4]).length != 4) throw new NamedException!"WadArchive"("reading error");
    if (lmpname[0..4] != "PWAD" && lmpname[0..4] != "IWAD") throw new NamedException!"WadArchive"("not a WAD file");
    auto count = readU32();
    auto dofs = readU32();
    if (count == 0) return;
    if (dofs < 3*4 || count == uint.max) throw new NamedException!"WadArchive"("invalid WAD file");
    fl.seek(dofs-3*4, SEEK_CUR);
    while (count-- > 0) {
      lmpofs = readU32();
      lmpsize = readU32();
      if (fl.rawRead(lmpname[]).length != 8) throw new NamedException!"WAD"("reading error");
      int pos = 0;
      while (pos < 8 && lmpname[pos] != 0) {
        if (lmpname[pos] >= 'A' && lmpname[pos] <= 'Z') lmpname[pos] += 32;
        ++pos;
      }
      if (pos == 0) continue;
      auto nm = lmpname[0..pos];
      if (nm.length > 6 && nm[$-6..$] == "_start") {
        fixPath(nm);
        continue;
      }
      if (nm.length > 4 && nm[$-4..$] == "_end") {
        curpath = null;
        continue;
      }
      uint fidx = uint.max;
      foreach (immutable idx, ref de; dir) if (de.name == lmpname[0..pos]) { fidx = cast(uint)idx; break; }
      if (fidx >= dir.length) {
        fidx = cast(uint)dir.length;
        dir ~= FileInfo();
      }
      dir[fidx].ofs = lmpofs;
      dir[fidx].size = lmpsize;
      //{ import std.stdio : stderr; stderr.writeln("[", nm, "]"); }
      {
        import std.uni : toLower;
        if (nm.length > 4 && nm[0..5] == "stbf_") {
          // `stbf_a`
          assert(nm.length == 6);
          dir[fidx].name = recodeToKOI8(recode(fixName(nm), "utf-8", "cp866").toLower, "utf-8");
          dir[fidx].path = recodeToKOI8(recode(fixPath(nm), "utf-8", "cp866").toLower, "utf-8");
          import std.string : format;
          dir[fidx].name = "stbf_x%02x.vga".format(cast(ubyte)(nm[$-1]));
        } else if (nm.length == 8 && nm[0..5] == "stcfn") {
          import std.conv : to;
          ubyte cc = cast(ubyte)(koi8from866Table[to!ubyte(nm[5..$], 10)]);
          import std.string : format;
          dir[fidx].name = "stcfnx%02x.vga".format(cc);
          dir[fidx].path = recodeToKOI8(recode(fixPath(nm), "utf-8", "cp866").toLower, "utf-8");
        } else {
          dir[fidx].name = koi8lotranslit(recodeToKOI8(recode(fixName(nm), "utf-8", "cp866").toLower, "utf-8"));
          dir[fidx].path = koi8lotranslit(recodeToKOI8(recode(fixPath(nm), "utf-8", "cp866").toLower, "utf-8"));
        }
      }
      //debug conwriteln(dir[fidx].path, " : ", dir[fidx].name);
    }
    debug conwriteln(dir.length, " files found");
  }


  // ////////////////////////////////////////////////////////////////////// //
  static import core.sync.mutex;

  core.sync.mutex.Mutex lock;

  void initLock () {
    lock = new core.sync.mutex.Mutex;
  }

  auto openDirEntry (uint idx, string filename) {
    import core.sys.linux.stdio : fopencookie;
    import core.stdc.stdio : FILE;
    import core.stdc.stdio : fopen, fclose;
    import core.stdc.stdlib : calloc, free;
    import etc.c.zlib;
    import std.internal.cstring : tempCString;
    import core.memory : GC;

    if (!zfl.isOpen) throw new NamedException!"WadArchive"("archive wasn't opened");
    if (idx >= dir.length) throw new NamedException!"WadArchive"("invalid dir index");

    // create cookied `FILE*`
    auto fc = cast(InnerFileCookied*)calloc(1, InnerFileCookied.sizeof);
    scope(exit) if (fc !is null) free(fc);
    if (fc is null) {
      import core.exception : onOutOfMemoryErrorNoGC;
      onOutOfMemoryErrorNoGC();
    }
    (*fc) = InnerFileCookied.init;
    (*fc).stpos = flstpos+dir[idx].ofs;
    (*fc).size = cast(uint)dir[idx].size;
    (*fc).lock = lock;
    GC.addRange(fc, InnerFileCookied.sizeof);
    (*fc).xfl = zfl;
    // open `cooked` file
    FILE* fres = fopencookie(cast(void*)fc, "r", fcdatpkCallbacks);
    if (fres is null) {
      // alas
      if ((*fc).fl !is null) fclose((*fc).fl);
      try { (*fc).xfl.detach(); } catch (Exception) {}
      throw new NamedException!"WadArchive"("can't open cookied file");
    }
    // ok
    fc = null;
    return File(fres, filename);
  }


  // ////////////////////////////////////////////////////////////////////// //
  // "inner" file processor; processes both packed and unpacked files
  // can be used as normal disk file processor too
  static struct InnerFileCookied {
    private import core.sys.posix.sys.types : ssize_t, off64_t = off_t;
    private import core.stdc.stdio : FILE;

    enum ibsize = 32768;

    core.sync.mutex.Mutex lock;
    // note that either one of `fl` or `xfl` must be opened and operational
    FILE* fl; // disk file, can be `null`
    File xfl; // disk file, can be closed
    long stpos; // starting position
    uint size; // unpacked size
    uint pos; // current file position
    //uint prpos; // previous file position
    //uint pkpos; // current position in DAT
    //ubyte[] pkb; // packed data
    bool eoz;

    @disable this (this);

  nothrow:
    ~this () { close(); }

    @property bool isOpen () @safe /*@nogc*/ { return (fl !is null || xfl.isOpen); }

    void close () {
      import core.memory : GC;
      import core.stdc.stdlib : free;
      {
        if (lock !is null) lock.lock();
        scope(exit) if (lock !is null) lock.unlock();
        if (fl !is null) {
          import core.stdc.stdio : fclose;
          fclose(fl);
          fl = null;
        }
        try { xfl.detach(); } catch (Exception) {} // it's safe to detach closed File
      }
      eoz = true;
    }

    ssize_t read (void* buf, size_t count) {
      if (buf is null) return -1;
      if (count == 0 || size == 0) return 0;
      lock.lock();
      scope(exit) lock.unlock();
      if (!isOpen) return -1; // read error
      if (pos >= size) return 0; // EOF
      {
        import core.stdc.stdio : ferror, fread;
        import core.sys.posix.stdio : fseeko;
        if (size-pos < count) count = cast(size_t)(size-pos);
        if (fl !is null) {
          // `FILE*`
          if (fseeko(fl, stpos+pos, 0) < 0) return -1;
          auto rd = fread(buf, 1, count, fl);
          if (rd != count && (rd < 0 || ferror(fl))) rd = -1;
          if (rd > 0) pos += rd;
          return rd;
        } else {
          // std.stdio.File
          try {
            xfl.seek(stpos+pos, 0);
            auto rd = xfl.rawRead(buf[0..count]);
            pos += rd.length;
            return  (rd.length == count ? rd.length : -1);
          } catch (Exception) {} //BAD DOGGY!
          return -1;
        }
      }
    }

    long seek (long ofs, int whence) {
      lock.lock();
      scope(exit) lock.unlock();
      if (!isOpen) return -1;
      //TODO: overflow checks
      switch (whence) {
        case 0: // SEEK_SET
          break;
        case 1: // SEEK_CUR
          ofs += pos;
          break;
        case 2: // SEEK_END
          if (ofs > 0) ofs = 0;
          ofs += size;
          break;
        default:
          return -1;
      }
      if (ofs < 0) return -1;
      if (ofs > size) ofs = size;
      pos = cast(uint)ofs;
      return ofs;
    }
  }


static:
  // ////////////////////////////////////////////////////////////////////// //
  extern(C) nothrow {
    import core.sys.linux.stdio : cookie_io_functions_t;
    import core.sys.posix.sys.types : ssize_t, off64_t = off_t;

    ssize_t fcdatpkRead (void* cookie, char* buf, size_t count) {
      //conwriteln("reading ", count, " bytes");
      import core.stdc.errno;
      auto fc = cast(InnerFileCookied*)cookie;
      auto res = fc.read(buf, count);
      if (res < 0) { errno = EIO; return -1; }
      return res;
    }

    ssize_t fcdatpkWrite (void* cookie, const(char)* buf, size_t count) {
      //conwriteln("writing ", count, " bytes");
      import core.stdc.errno;
      errno = EIO; //FIXME: find better code
      return 0; // error; write should not return `-1`
    }

    int fcdatpkSeek (void* cookie, off64_t* offset, int whence) {
      //conwriteln("seeking ", *offset, " bytes, whence=", whence);
      import core.stdc.errno;
      auto fc = cast(InnerFileCookied*)cookie;
      auto res = fc.seek(*offset, whence);
      if (res < 0) { errno = EIO; return -1; }
      *offset = cast(off64_t)res;
      return 0;
    }

    int fcdatpkClose (void* cookie) {
      import core.memory : GC;
      import core.stdc.stdlib : free;
      //conwriteln("closing");
      auto fc = cast(InnerFileCookied*)cookie;
      //fc.close();
      GC.removeRange(cookie);
      try { fc.__dtor(); } catch (Exception) {}
      // no need to run finalizers, we SHOULD NOT have any
      //try { GC.runFinalizers(cookie[0..InnerFileCookied.sizeof]); } catch (Exception) {}
      //fc.xfl.__dtor();
      free(cookie);
      //conwriteln("closed");
      return 0;
    }
  }

  __gshared cookie_io_functions_t fcdatpkCallbacks = cookie_io_functions_t(
    /*.read =*/ &fcdatpkRead,
    /*.write =*/ &fcdatpkWrite,
    /*.seek =*/ &fcdatpkSeek,
    /*.close =*/ &fcdatpkClose,
  );

static:
  T[] xalloc(T) (size_t len) {
    import core.stdc.stdlib : malloc;
    if (len < 1) return null;
    auto res = cast(T*)malloc(len*T.sizeof);
    if (res is null) {
      import core.exception : onOutOfMemoryErrorNoGC;
      onOutOfMemoryErrorNoGC();
    }
    res[0..len] = T.init;
    return res[0..len];
  }

  void xfree(T) (ref T[] slc) {
    if (slc.ptr !is null) {
      import core.stdc.stdlib : free;
      free(slc.ptr);
    }
    slc = null;
  }
}
