module d2dmap is aliced;
private:

import console;
import wadarc;

import iv.stream;
import iv.encoding;


// ////////////////////////////////////////////////////////////////////////// //
public final class LevelMap {
private:
  private import std.stdio : File;

  enum MapVersion = 2; // ����� ��������� ������ �����
  public enum MapSize = 100;

public:
  // tile type
  enum : ubyte {
    TILE_EMPTY = 0,
    TILE_WALL = 1,
    TILE_DOORC = 2, // closed door
    TILE_DOORO = 3, // opened door
    TILE_STEP = 4,
    TILE_WATER = 5,
    TILE_ACID1 = 6,
    TILE_ACID2 = 7,
    TILE_MBLOCK = 8, // just blocks monsters
    TILE_LIFTU = 9,
    TILE_LIFTD = 10,
    //
    TILE_ACTTRAP = 255,
  }

  enum : short {
    MB_COMMENT = -1,
    MB_END = 0,
    MB_WALLNAMES,
    MB_BACK,
    MB_WTYPE,
    MB_FRONT,
    MB_THING,
    MB_SWITCH,
    MB_MUSIC, // usually 8 bytes
    MB_SKY, // ushort: [1..3]
    MB_SWITCH2,
  }

  enum {
    SW_PL_PRESS = 1<<0,
    SW_MN_PRESS = 1<<1,
    SW_PL_NEAR  = 1<<2,
    SW_MN_NEAR  = 1<<3,
    SW_KEY_R    = 1<<4,
    SW_KEY_G    = 1<<5,
    SW_KEY_B    = 1<<6,
  }

  enum {
    SW_NONE,
    SW_EXIT,
    SW_EXITS,
    SW_OPENDOOR,
    SW_SHUTDOOR,
    SW_SHUTTRAP,
    SW_DOOR,
    SW_DOOR5,
    SW_PRESS,
    SW_TELE,
    SW_SECRET,
    SW_LIFTUP,
    SW_LIFTDOWN,
    SW_TRAP,
    SW_LIFT,
    SW_WINGAME = 64,
  }

  static struct MapThing {
    // thing flags
    enum : ushort {
      DirRight   = 0x0001,
      DeathMatch = 0x0010, // ���������� ������ � DeathMatch'�
    }

    short x, y;   // ����������
    ushort type;  // ���
    ushort flags; // �����

    @property const pure nothrow @safe @nogc {
      bool left () => ((flags&DirRight) == 0);
      bool right () => ((flags&DirRight) != 0);
      bool dmonly () => ((flags&DeathMatch) != 0);
    }
  }

  static struct MapSwitch {
    ubyte x, y;  // ����������/8
    ubyte type;  // ���
    ubyte tm;    // ������ ���� 0
    ubyte a, b;  // ������ - ����������/8 �����
    ushort c;    // �� ������������ (����� ��)
    ubyte flags; // ����� (SW_*)
  }

public:
  enum { Type, Front, Back, Water, Lava, Acid, LightMask } // tile types, Front+: megatexture types
  ubyte[][3] tiles; // Type, Front, Back
  string[] wallnames; // "_water_0": water, "_water_1": acid, "_water_2": lava
  int width, height;
  string skytex;

  MapThing[] things;
  MapSwitch[] switches;

  this (string fname) { load(fname); }
  this (File fl) { load(fl); }

  void clear () {
    tiles[] = null;
    wallnames = null;
    skytex = null;
    things = null;
    switches = null;
    width = height = 0;
  }

  void dump (int idx) {
    static char to62 (ubyte b) { pragma(inline, true); return cast(char)(b < 10 ? '0'+b : (b-10 < 26 ? 'A'+(b-10) : 'a'+(b-10-26))); }
    foreach (immutable y; 0..MapSize) {
      foreach (immutable x; 0..MapSize) {
        conwrite(to62(tiles[idx][y*MapSize+x]));
      }
      conwriteln;
    }
  }

  // true: found
  bool getThingPos (ushort id, int* x=null, int* y=null, ushort* flags=null) {
    foreach (ref th; things[]) {
      if (th.type == id) {
        if (x !is null) *x = th.x;
        if (y !is null) *y = th.y;
        if (flags !is null) *flags = th.flags;
        return true;
      }
    }
    if (x !is null) *x = 0;
    if (y !is null) *y = 0;
    if (flags !is null) *flags = 0;
    return false;
  }

private:
  void calcMapSize () {
    bool isEmpty(string dir) (int x, int y) if (dir == "col" || dir == "row") {
      while (x < MapSize && y < MapSize) {
        if (tiles[0][y*MapSize+x] || tiles[1][y*MapSize+x] || tiles[2][y*MapSize+x]) return false;
        static if (dir == "row") ++x; else ++y;
      }
      return true;
    }
    width = height = MapSize;
    // fix width
    while (width > 0 && isEmpty!"col"(width-1, 0)) --width;
    // fix height
    while (height > 0 && isEmpty!"row"(0, height-1)) --height;
  }

  void load (string fname) {
    import std.stdio : File;
    load(openFile(fname));
  }

  void load(ST) (auto ref ST st) if (isReadableStream!ST) {
    clear();
    scope(failure) clear;

    char[8] sign;
    st.rawReadExact(sign[]);
    if (sign != "Doom2D\x1a\x00") throw new Exception("invalid map signature");
    if (st.readNum!ushort() != MapVersion) throw new Exception("invalid map version");

    // load map blocks
    foreach (ref a; tiles[]) a = new ubyte[](MapSize*MapSize);
    char[$] skyname = "sprites/sky/rsky1.vga";
    for (;;) {
      auto btype = st.readNum!ushort();
      if (btype == MB_END) break; // no more blocks
      auto bsubtype = st.readNum!ushort();
      auto bsize = st.readNum!uint();
      if (bsize == 0) continue; // skip this block, it has no data (wtf?!)
      // various tile types
      switch (btype) {
        case MB_SKY:
          if (bsize != 2) throw new Exception("invalid sky data size");
          ushort num = st.readNum!ushort();
          if (num >= 1 && num <= 3) skyname[$-5] = cast(char)('0'+num);
          break;
        case MB_BACK:
        case MB_FRONT:
        case MB_WTYPE:
          if (bsubtype > 1) throw new Exception("unknown tile block subtype");
          int idx = (btype == MB_BACK ? Back : (btype == MB_FRONT ? Front : Type));
          //ubyte[MapSize*MapSize] data = 0;
          auto data = tiles[idx];
          if (bsubtype == 0) {
            if (bsize != data.length) throw new Exception("invalid tile data size");
            st.rawReadExact(data[]);
          } else {
            // unpack RLE data
            auto pkdata = new ubyte[](bsize);
            st.rawReadExact(pkdata[]);
            int spos = 0, opos = 0;
            while (spos < pkdata.length) {
              ubyte b = pkdata[spos++];
              if (b != 255) {
                data[opos++] = b;
              } else {
                int count = pkdata[spos++];
                count |= pkdata[spos++]<<8;
                b = pkdata[spos++];
                while (count-- > 0) data[opos++] = b;
              }
            }
            assert(opos == data.length);
          }
          // copy unpacked data
          //foreach (immutable y; 0..MapSize) tiles[idx][y*MapSize] = data[y*MapSize..(y+1)*MapSize];
          break;
        case MB_WALLNAMES:
          wallnames.length = 0;
          wallnames ~= null;
          //wallnames[] = null;
          while (bsize >= 8+1) {
            char[8] texname = 0;
            st.rawReadExact(texname[]);
            auto type = st.readNum!ubyte();
            //char[] tn;
            string tns;
            foreach (char ch; texname) {
              if (ch == 0) break;
              if (ch >= 'A' && ch <= 'Z') ch += 32;
              ch = dos2koi8(ch);
              tns ~= ch;
              //tn = texname[0..idx+1];
            }
            import std.uni : toLower;
            wallnames ~= koi8lotranslit(tns).toLower;
            bsize -= 8+1;
          }
          if (bsize != 0) throw new Exception("invalid texture chunk size");
          debug { conwriteln(wallnames.length, " textures loaded"); }
          break;
        case MB_THING:
          while (bsize >= 8) {
            bsize -= 8;
            MapThing t = void;
            t.x = st.readNum!short();
            t.y = st.readNum!short();
            t.type = st.readNum!ushort();
            t.flags = st.readNum!ushort();
            if (t.type != 0) things ~= t;
          }
          if (bsize != 0) throw new Exception("invalid thing chunk size");
          break;
        case MB_SWITCH2:
          while (bsize >= 9) {
            bsize -= 9;
            MapSwitch sw = void;
            sw.x = st.readNum!ubyte();
            sw.y = st.readNum!ubyte();
            sw.type = st.readNum!ubyte();
            sw.tm = st.readNum!ubyte();
            sw.a = st.readNum!ubyte();
            sw.b = st.readNum!ubyte();
            sw.c = st.readNum!ushort();
            sw.flags = st.readNum!ubyte();
            switches ~= sw;
          }
          if (bsize != 0) throw new Exception("invalid thing chunk size");
          break;
        default:
          auto pkdata = new ubyte[](bsize);
          st.rawReadExact(pkdata[]);
          break;
      }
    }
    calcMapSize();
    skytex = skyname.idup;
    debug { conwriteln(width, "x", height); }
  }
}
