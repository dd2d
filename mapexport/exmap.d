module exmap is aliced;

private:
import console;
import wadarc;

import iv.stream;
import iv.strex;

import d2dmap;
import d2dadefs;


// ////////////////////////////////////////////////////////////////////////// //
import std.stdio : File;

void exportMapScript (string modname, string mapname, LevelMap map, File fo) {

  void writeRects (const(ubyte)[] tilemap, string layer) {
    auto tiles = tilemap.dup;

    ubyte getAt (int x, int y) {
      pragma(inline, true);
      return (x >= 0 && y >= 0 && x < map.width && y < map.height ? tiles[y*map.width+x] : 0);
    }

    void setAt (int x, int y, ubyte v) {
      pragma(inline, true);
      if (x >= 0 && y >= 0 && x < map.width && y < map.height) tiles[y*map.width+x] = v;
    }

    bool isLineFilledWith(string mode) (int x, int y, int len, ubyte tnum) {
      static if (mode == "horizontal") {
        enum dx = 1;
        enum dy = 0;
      } else if (mode == "vertical") {
        enum dx = 0;
        enum dy = 1;
      } else {
        static assert(0, "wtf?!");
      }
      assert(len > 0);
      while (len-- > 0) {
        if (getAt(x, y) != tnum) return false;
        x += dx;
        y += dy;
      }
      return true;
    }

    bool isRectFilledWith (int x, int y, int w, int h, ubyte tnum) {
      assert(w > 0);
      assert(h > 0);
      while (h-- > 0) {
        if (!isLineFilledWith!"horizontal"(x, y, w, tnum)) return false;
        ++y;
      }
      return true;
    }

    void fillRectWith (int x, int y, int w, int h, ubyte tnum) {
      assert(w > 0);
      assert(h > 0);
      while (h-- > 0) {
        foreach (int sx; x..x+w) setAt(sx, y, tnum);
        ++y;
      }
    }

    foreach (int y; 0..map.height) {
      foreach (int x; 0..map.width) {
        ubyte tnum = getAt(x, y);
        if (tnum == 0) continue;
        if (tnum >= map.wallnames.length) {
          import std.string : format;
          throw new Exception("tile type %s exceeds max texture number (%s)".format(tnum, map.wallnames.length-1));
        }
        // stupid brute force
        int bestw = 1, besth = 1;
        foreach (int ey; y..map.height) {
          foreach (int ex; x..map.width) {
            int w = ex-x+1;
            int h = ey-y+1;
            if (isRectFilledWith(x, y, w, h, tnum)) {
              if (w*h > bestw*besth) { bestw = w; besth = h; }
            }
          }
        }
        fillRectWith(x, y, bestw, besth, 0);
        if (bestw == 1 && besth == 1) {
          fo.writeln("  mapSetTile(", map.wallnames[tnum].quote, ", ", layer, ", ", x, ", ", y, ");");
        } else {
          fo.writeln("  mapSetRect(", map.wallnames[tnum].quote, ", ", layer, ", ", x, ", ", y, ", ", bestw, ", ", besth, ");");
        }
      }
    }
  }

  void writeTypeRects (const(ubyte)[] tilemap) {
    auto tiles = tilemap.dup;

    ubyte getAt (int x, int y) {
      pragma(inline, true);
      return (x >= 0 && y >= 0 && x < map.width && y < map.height ? tiles[y*map.width+x] : 0);
    }

    void setAt (int x, int y, ubyte v) {
      pragma(inline, true);
      if (x >= 0 && y >= 0 && x < map.width && y < map.height) tiles[y*map.width+x] = v;
    }

    bool isLineFilledWith(string mode) (int x, int y, int len, ubyte tnum) {
      static if (mode == "horizontal") {
        enum dx = 1;
        enum dy = 0;
      } else if (mode == "vertical") {
        enum dx = 0;
        enum dy = 1;
      } else {
        static assert(0, "wtf?!");
      }
      assert(len > 0);
      while (len-- > 0) {
        if (getAt(x, y) != tnum) return false;
        x += dx;
        y += dy;
      }
      return true;
    }

    bool isRectFilledWith (int x, int y, int w, int h, ubyte tnum) {
      assert(w > 0);
      assert(h > 0);
      while (h-- > 0) {
        if (!isLineFilledWith!"horizontal"(x, y, w, tnum)) return false;
        ++y;
      }
      return true;
    }

    void fillRectWith (int x, int y, int w, int h, ubyte tnum) {
      assert(w > 0);
      assert(h > 0);
      while (h-- > 0) {
        foreach (int sx; x..x+w) setAt(sx, y, tnum);
        ++y;
      }
    }

    foreach (int y; 0..map.height) {
      foreach (int x; 0..map.width) {
        ubyte tnum = getAt(x, y);
        if (tnum == 0) continue;
        string tname;
        switch (tnum) {
          case LevelMap.TILE_WALL: tname = "TILE_WALL"; break;
          case LevelMap.TILE_DOORC: tname = "TILE_DOORC"; break;
          case LevelMap.TILE_DOORO: tname = "TILE_DOORO"; break;
          case LevelMap.TILE_STEP: tname = "TILE_STEP"; break;
          case LevelMap.TILE_WATER: tname = "TILE_WATER"; break;
          case LevelMap.TILE_ACID1: tname = "TILE_ACID1"; break;
          case LevelMap.TILE_ACID2: tname = "TILE_ACID2"; break;
          case LevelMap.TILE_MBLOCK: tname = "TILE_MBLOCK"; break;
          case LevelMap.TILE_LIFTU: tname = "TILE_LIFTU"; break;
          case LevelMap.TILE_LIFTD: tname = "TILE_LIFTD"; break;
          case LevelMap.TILE_ACTTRAP: tname = "TILE_ACTTRAP"; break;
          default:
            import std.string : format;
            throw new Exception("unknown tile type: %s".format(tnum));
        }
        // stupid brute force
        int bestw = 1, besth = 1;
        foreach (int ey; y..map.height) {
          foreach (int ex; x..map.width) {
            int w = ex-x+1;
            int h = ey-y+1;
            if (isRectFilledWith(x, y, w, h, tnum)) {
              if (w*h > bestw*besth) { bestw = w; besth = h; }
            }
          }
        }
        fillRectWith(x, y, bestw, besth, 0);
        if (bestw == 1 && besth == 1) {
          fo.writeln("  mapSetTypeTile(", tname, ", ", x, ", ", y, ");");
        } else {
          fo.writeln("  mapSetTypeRect(", tname, ", ", x, ", ", y, ", ", bestw, ", ", besth, ");");
        }
      }
    }
  }

  void writeThings (string ctype) {
    foreach (ref thing; map.things) {
      //if (thing.dmonly) continue;
      if (auto did = thing.type in d2dactordefsById) {
        if (did.classtype != ctype) continue;
        if (thing.dmonly) fo.write("  if (gameMode == GM_DEATHMATCH) "); else fo.write("  ");
        fo.writeln("actorSpawn(", did.classtype.quote, ", ", did.classname.quote, ", ", cast(int)thing.x, ", ", cast(int)thing.y, ", ACTOR_DIR_", (thing.right ? "RIGHT" : "LEFT"), ");");
      } else {
        conwriteln("ignoring unknown D2D thing with mapid ", thing.type);
        continue;
      }
    }
  }

  void writeSwitches () {
    foreach (ref sw; map.switches) {
      if (sw.type == 0) continue; // SW_NONE
      assert(sw.tm == 0);
      assert(sw.c == 0);
      fo.write("  actorSpawnVanillaSwitch(");
      switch (sw.type) {
        case LevelMap.SW_NONE: assert(0);
        case LevelMap.SW_EXIT: fo.write("exit".quote); break;
        case LevelMap.SW_EXITS: fo.write("exitSecret".quote); break;
        case LevelMap.SW_OPENDOOR: fo.write("doorOpen".quote); break;
        case LevelMap.SW_SHUTDOOR: fo.write("doorClose".quote); break;
        case LevelMap.SW_SHUTTRAP: fo.write("trapClose".quote); break;
        case LevelMap.SW_DOOR: fo.write("door".quote); break;
        case LevelMap.SW_DOOR5: fo.write("doorTimed90".quote); break;
        case LevelMap.SW_PRESS: fo.write("press".quote); break;
        case LevelMap.SW_TELE: fo.write("teleport".quote); break;
        case LevelMap.SW_SECRET: fo.write("secret".quote); break;
        case LevelMap.SW_LIFTUP: fo.write("liftUp".quote); break;
        case LevelMap.SW_LIFTDOWN: fo.write("liftDown".quote); break;
        case LevelMap.SW_TRAP: fo.write("trapClose".quote); break;
        case LevelMap.SW_LIFT: fo.write("liftToggle".quote); break;
        case LevelMap.SW_WINGAME: fo.write("exitWin".quote); break;
        default: assert(0, "invalid switch type");
      }
      fo.write(", ", sw.x*8, ", ", sw.y*8, ", ");
      // flags
      fo.write("0");
      if (sw.flags&LevelMap.SW_PL_PRESS) fo.write("|SWITCH_PLAYER_PRESS");
      if (sw.flags&LevelMap.SW_MN_PRESS) fo.write("|SWITCH_MONSTER_PRESS");
      if (sw.flags&LevelMap.SW_PL_NEAR) fo.write("|SWITCH_PLAYER_PROXIMITY");
      if (sw.flags&LevelMap.SW_MN_NEAR) fo.write("|SWITCH_MONSTER_PROXIMITY");
      if (sw.flags&LevelMap.SW_KEY_R) fo.write("|SWITCH_KEY_RED");
      if (sw.flags&LevelMap.SW_KEY_G) fo.write("|SWITCH_KEY_GREEN");
      if (sw.flags&LevelMap.SW_KEY_B) fo.write("|SWITCH_KEY_BLUE");
      fo.writeln(", ", sw.a, ", ", sw.b, ");");
    }
  }

  fo.writeln("module ", modname, ";");
  fo.writeln();
  fo.writeln("import actor, mapapi, stdlib;");
  fo.writeln();
  fo.writeln();
  fo.writeln("public void buildMap () {");
  fo.writeln("  mapSetSize(", map.width, ", ", map.height, ");");
  fo.writeln("  mapSetName(", mapname.quote, ");");
  fo.writeln("  putTiles();");
  fo.writeln("  putItems();");
  fo.writeln("  putMonsters();");
  fo.writeln("  putSwitches();");
  fo.writeln("}");

  fo.writeln();
  fo.writeln();
  fo.writeln("void putItems () {");
  writeThings("item");
  fo.writeln("}");

  fo.writeln();
  fo.writeln();
  fo.writeln("void putMonsters () {");
  writeThings("monster");
  fo.writeln("}");

  fo.writeln();
  fo.writeln();
  fo.writeln("void putSwitches () {");
  writeSwitches();
  fo.writeln("}");

  fo.writeln();
  fo.writeln();
  fo.writeln("void putTiles () {");
  fo.writeln("  // back");
  writeRects(map.tiles[LevelMap.Back], "LAYER_BACK");
  fo.writeln("  // front");
  writeRects(map.tiles[LevelMap.Front], "LAYER_FRONT");
  fo.writeln("  // types");
  writeTypeRects(map.tiles[LevelMap.Type]);
  fo.writeln("}");
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  string[] wadList;
  string mapName = null;

  for (usize idx = 1; idx < args.length; ++idx) {
    if (args[idx] == "--map" || args[idx] == "-map") {
      import std.algorithm : remove;
      if (mapName !is null) assert(0, "duplicate '--map'");
      mapName = args[idx+1];
      args = args.remove(idx, 2);
      --idx; //hack
    } else {
      wadList ~= args[idx];
    }
  }

  void setDP () {
    version(rdmd) {
      setDataPath("data");
    } else {
      import std.file : thisExePath;
      import std.path : dirName;
      setDataPath(thisExePath.dirName);
    }
    addWad("/home/ketmar/k8prj/doom2d-tl/data/doom2d.wad");
    foreach (string s; wadList) addWad(s);
  }

  setDP();

  if (mapName is null) mapName = "map01";

  conwriteln("loading '", mapName, "'...");
  auto map = new LevelMap("maps/"~mapName~".d2m");

  conwriteln("writing '", mapName, ".dacs'...");
  exportMapScript(mapName, mapName, map, File(mapName~".dacs", "w"));
}
