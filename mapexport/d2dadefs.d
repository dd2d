module d2dadefs is aliced;

private:
import console;


// ////////////////////////////////////////////////////////////////////////// //
// known D2D actors (and pseudoactors)
public struct ActorDefD2D {
  string classtype;
  string classname;
  string fullname;
  ushort mapid; // thing id in map

  this (string ctype, string cname, ushort thid) { classtype = ctype; classname = cname; fullname = classtype~":"~classname; mapid = thid; }

  string toString () const {
    import std.string : format;
    return "ActorDefD2D(%s, %s, %s)".format(classtype/*.quote*/, classname/*.quote*/, mapid);
  }
}

immutable ActorDefD2D[$] d2dactordefs = [
  ActorDefD2D("playerstart", "Player1", 1),
  ActorDefD2D("playerstart", "Player2", 2),
  ActorDefD2D("playerstart", "DMStart", 3),
  ActorDefD2D("item", "Clip", 100),
  ActorDefD2D("item", "Shell", 101),
  ActorDefD2D("item", "Rocket", 102),
  ActorDefD2D("item", "Cell", 103),
  ActorDefD2D("item", "Ammo", 104),
  ActorDefD2D("item", "ShellBox", 105),
  ActorDefD2D("item", "RocketBox", 106),
  ActorDefD2D("item", "CellPack", 107),
  ActorDefD2D("item", "StimPack", 108),
  ActorDefD2D("item", "MediKit", 109),
  ActorDefD2D("item", "BackPack", 110),
  ActorDefD2D("item", "Chainsaw", 111),
  ActorDefD2D("item", "Shotgun", 112),
  ActorDefD2D("item", "SuperShotgun", 113),
  ActorDefD2D("item", "MachineGun", 114),
  ActorDefD2D("item", "RocketLauncher", 115),
  ActorDefD2D("item", "Plasmagun", 116),
  ActorDefD2D("item", "BFG900", 117),
  ActorDefD2D("item", "Armor1", 118),
  ActorDefD2D("item", "Armor2", 119),
  ActorDefD2D("item", "MegaSphere", 120),
  ActorDefD2D("item", "Invulnerability", 121),
  ActorDefD2D("item", "Aqualung", 122),
  ActorDefD2D("item", "RedKey", 123),
  ActorDefD2D("item", "GreenKey", 124),
  ActorDefD2D("item", "BlueKey", 125),
  ActorDefD2D("item", "ProtectionSuit", 126),
  ActorDefD2D("item", "Super", 127),
  ActorDefD2D("item", "RedTorch", 128),
  ActorDefD2D("item", "GreenTorch", 129),
  ActorDefD2D("item", "BlueTorch", 130),
  ActorDefD2D("item", "Gor1", 131),
  ActorDefD2D("item", "FCan", 132),
  ActorDefD2D("item", "Gun2", 133),
  ActorDefD2D("monster", "Demon", 200),
  ActorDefD2D("monster", "Imp", 201),
  ActorDefD2D("monster", "Zombie", 202),
  ActorDefD2D("monster", "Sergeant", 203),
  ActorDefD2D("monster", "Cyberdemon", 204),
  ActorDefD2D("monster", "Chaingunner", 205),
  ActorDefD2D("monster", "BaronOfHell", 206),
  ActorDefD2D("monster", "HellKnight", 207),
  ActorDefD2D("monster", "Cacodemon", 208),
  ActorDefD2D("monster", "LostSoul", 209),
  ActorDefD2D("monster", "PainElemental", 210),
  ActorDefD2D("monster", "SpiderMastermind", 211),
  ActorDefD2D("monster", "Arachnotron", 212),
  ActorDefD2D("monster", "Mancubus", 213),
  ActorDefD2D("monster", "Revenant", 214),
  ActorDefD2D("monster", "Archvile", 215),
  ActorDefD2D("monster", "Fish", 216),
  ActorDefD2D("monster", "Barrel", 217),
  ActorDefD2D("monster", "Robot", 218),
  ActorDefD2D("monster", "Man", 219),
];


public immutable ActorDefD2D[ushort] d2dactordefsById;

shared static this () {
  foreach (const ref ActorDefD2D d2da; d2dactordefs) {
    d2dactordefsById[d2da.mapid] = d2da;
    //d2dactordefsByTN[d2da.fullname] = d2da;
  }
}
