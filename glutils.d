/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module glutils is aliced;
private:
import iv.glbinds;
import iv.vfs;
import arsd.color;
import arsd.png;
import iv.jpegd;

import wadarc;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool glutilsShowShaderWarnings = false; // shut up!

/*
__gshared GLuint glLastUsedTexture = 0;

public void useTexture (GLuint tid) {
  pragma(inline, true);
  if (glLastUsedTexture != tid) {
    glLastUsedTexture = tid;
    glBindTexture(GL_TEXTURE_2D, tid);
  }
}

public void useTexture (Texture tex) { pragma(inline, true); useTexture(tex !is null ? tex.tid : 0); }
*/


public TrueColorImage loadPngFile (string fname) {
  auto fl = VFile(fname);
  auto sz = fl.size;
  if (sz < 4 || sz > 32*1024*1024) throw new Exception("invalid png file size: '"~fname~"'");
  if (sz == 0) return null;
  auto res = new ubyte[](cast(uint)sz);
  if (fl.rawRead(res[]).length != res.length) throw new Exception("error reading png file '"~fname~"'");
  return imageFromPng(readPng(res)).getAsTrueColorImage;
}


// ////////////////////////////////////////////////////////////////////////// //
class OpenGLObject {
  abstract @property uint id () const pure nothrow @nogc;

  final void activate () nothrow @nogc {
    if (gloSP >= gloStack.length) assert(0, "glo stack overflow");
    gloStack.ptr[gloSP++] = this;
    activateObj();
  }

  final void deactivate () nothrow @nogc {
    foreach_reverse (usize idx; 0..gloStack.length) {
      if (gloStack.ptr[idx] is this) {
        // find previous object of this type
        foreach_reverse (usize pidx; 0..idx) {
          if (typeid(gloStack.ptr[pidx]) is typeid(gloStack.ptr[idx])) {
            gloStack.ptr[pidx].activateObj();
            removeFromStack(pidx);
            return;
          }
        }
        deactivateObj();
        removeFromStack(idx);
        return;
      }
    }
    assert(0, "trying to deactivate inactive object");
  }

protected:
  abstract void activateObj () nothrow @nogc;
  abstract void deactivateObj () nothrow @nogc;
}


__gshared OpenGLObject[1024] gloStack;
__gshared uint gloSP = 0;


public void gloStackClear () nothrow @nogc {
  bindTexture(0);
  //glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glUseProgram(0);
}


void removeFromStack (usize idx) nothrow @nogc {
  if (idx >= gloSP) return;
  if (idx != gloSP-1) {
    import core.stdc.string : memmove;
    memmove(gloStack.ptr+idx, gloStack.ptr+idx+1, (gloSP-idx-1)*gloStack[0].sizeof);
  }
  --gloSP;
}


// ////////////////////////////////////////////////////////////////////////// //
public final class Texture : OpenGLObject {
  GLuint tid;
  int width, height;

  override @property uint id () const pure nothrow @nogc => tid;

  // default: repeat, linear
  enum Option : int {
    Repeat,
    Clamp,
    ClampBorder,
    Linear,
    Nearest,
    UByte,
    Float, // create floating point texture
    Depth, // FBO: attach depth buffer
    FBO2, // create two FBO attaches
  }

  this (string fname, in Option[] opts...) { loadImage(fname, opts); }
  this (int w, int h, in Option[] opts...) { createIntr(w, h, null, opts); }
  this (TrueColorImage aimg, Option[] opts...) { createIntr(aimg.width, aimg.height, aimg, opts); }
  ~this () { clear(); }


  void clear () {
    if (tid) {
      //useTexture(tid);
      bindTexture(tid);
      glDeleteTextures(1, &tid);
      //useTexture(0);
      bindTexture(0);
      tid = 0;
      width = 0;
      height = 0;
    }
  }

  private static void processOpt (GLuint* wrapOpt, GLuint* filterOpt, GLuint* ttype, in Option[] opts...) {
    foreach (immutable opt; opts) {
      switch (opt) with (Option) {
        case Repeat: *wrapOpt = GL_REPEAT; break;
        case Clamp: *wrapOpt = GL_CLAMP_TO_EDGE; break;
        case ClampBorder: *wrapOpt = GL_CLAMP_TO_BORDER; break;
        case Linear: *filterOpt = GL_LINEAR; break;
        case Nearest: *filterOpt = GL_NEAREST; break;
        case UByte: *ttype = GL_UNSIGNED_BYTE; break;
        case Float: *ttype = GL_FLOAT; break;
        default:
      }
    }
  }

  void createIntr (int w, int h, TrueColorImage img, in Option[] opts...) {
    import core.stdc.stdlib : malloc, free;
    assert(w > 0);
    assert(h > 0);
    clear();

    GLuint wrapOpt = GL_REPEAT;
    GLuint filterOpt = GL_LINEAR;
    GLuint ttype = GL_UNSIGNED_BYTE;
    processOpt(&wrapOpt, &filterOpt, &ttype, opts);

    glGenTextures(1, &tid);
    //useTexture(tid);
    auto oldtid = boundTexture;
    bindTexture(tid);
    scope(exit) bindTexture(oldtid);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    GLfloat[4] bclr = 0.0;
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);
    if (img !is null && img.width == w && img.height == h) {
      // create straight from image
      glTexImage2D(GL_TEXTURE_2D, 0, (ttype == GL_FLOAT ? GL_RGBA16F : GL_RGBA), w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.imageData.bytes.ptr);
    } else {
      // create empty texture
      ubyte* ptr = null;
      scope(exit) if (ptr !is null) free(ptr);
      {
        import core.stdc.string : memset;
        ptr = cast(ubyte*)malloc(w*h*4);
        if (ptr !is null) memset(ptr, 0, w*h*4);
      }
      glTexImage2D(GL_TEXTURE_2D, 0, (ttype == GL_FLOAT ? GL_RGBA16F : GL_RGBA), w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, ptr);
      if (img !is null && img.width > 0 && img.height > 0) {
        // setup from image
        //TODO: dunno if it's ok to use images bigger than texture here
        glTexSubImage2D(GL_TEXTURE_2D, 0,  0, 0, img.width, img.height, GL_RGBA, GL_UNSIGNED_BYTE, img.imageData.bytes.ptr);
        // the following is ok too
        //bindTexture(0);
        //glTextureSubImage2D(tid, 0,  0, 0, img.width, img.height, GL_RGBA, GL_UNSIGNED_BYTE, img.imageData.bytes.ptr);
      }
    }
    width = w;
    height = h;
  }

  void setFromImage (TrueColorImage img, int x=0, int y=0) {
    if (img is null || !tid || img.height < 1 || img.width < 1) return;
    if (x >= width || y >= height) return;
    if (x+img.width <= 0 || y+img.height <= 0) return; //TODO: overflow
    if (x >= 0 && y >= 0 && x+img.width <= width && y+img.height <= height) {
      // easy case, just copy it
      glTextureSubImage2D(tid, 0, x, y, img.width, img.height, GL_RGBA, GL_UNSIGNED_BYTE, img.imageData.bytes.ptr);
    } else {
      import core.stdc.stdlib : malloc, free;
      import core.stdc.string : memset, memcpy;
      // hard case, have to build the temp region
      uint* src = cast(uint*)img.imageData.bytes.ptr;
      // calc x skip and effective width
      int rwdt = img.width;
      if (x < 0) {
        rwdt += x;
        src -= x; // as `x` is negative here
        x = 0;
      }
      if (x+rwdt > width) rwdt = width-x;
      // calc y skip and effective height
      int rhgt = img.height;
      if (y < 0) {
        rhgt += y;
        src -= y*img.width; // as `y` is negative here
        y = 0;
      }
      if (y+rhgt > height) rhgt = height-y;
      assert(rwdt > 0 && rhgt > 0);
      uint* ptr = null;
      scope(exit) if (ptr !is null) free(ptr);
      ptr = cast(uint*)malloc(rwdt*rhgt*4);
      if (ptr is null) assert(0, "out of memory in `Texture.setFromImage()`");
      // now copy
      auto d = ptr;
      foreach (immutable _; 0..rhgt) {
        memcpy(d, src, rwdt*4);
        src += img.width;
        d += rwdt;
      }
      glTextureSubImage2D(tid, 0, x, y, rwdt, rhgt, GL_RGBA, GL_UNSIGNED_BYTE, ptr);
    }
  }

  void loadPng (VFile fl, in Option[] opts...) {
    auto flsize = fl.size-fl.tell;
    if (flsize < 8 || flsize > 1024*1024*32) throw new Exception("png image too big");
    auto data = new ubyte[](cast(uint)flsize);
    fl.rawReadExact(data);
    auto png = readPng(data);
    auto ximg = imageFromPng(png).getAsTrueColorImage;
    if (ximg is null) throw new Exception("png: wtf?!");
    if (ximg.width < 1 || ximg.height < 1) throw new Exception("png image too small");
    createIntr(ximg.width, ximg.height, ximg, opts);
  }

  void loadJpeg (VFile fl, in Option[] opts...) {
    auto jpg = readJpeg(fl).getAsTrueColorImage;
    if (jpg.width < 1 || jpg.width > 32760) throw new Exception("invalid image width");
    if (jpg.height < 1 || jpg.height > 32760) throw new Exception("invalid image height");
    createIntr(jpg.width, jpg.height, jpg, opts);
  }

  void loadImage (string fname, in Option[] opts...) {
    scope(failure) clear;
    auto fl = VFile(fname);
    scope(failure) fl.seek(0);
    char[8] sign;
    fl.seek(0);
    fl.rawReadExact(sign[]);
    fl.seek(0);
    // png?
    if (sign == "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A") {
      loadPng(fl, opts);
      return;
    }
    // jpeg?
    int width, height, actual_comps;
    if (sign[0..2] == "\xff\xd8" && detectJpeg(fl, width, height, actual_comps)) {
      /*
      fl.seek(-2, Seek.End);
      fl.rawReadExact(sign[0..2]);
      fl.seek(0);
      if (sign[0..2] == "\xff\xd9") {
        loadJpeg(fl, opts);
        return;
      }
      */
      loadJpeg(fl, opts);
      return;
    }
    throw new Exception("invalid texture image format");
  }

protected:
  override void activateObj () nothrow @nogc { /*if (tid)*/ bindTexture(tid); }
  override void deactivateObj () nothrow @nogc { /*if (tid)*/ bindTexture(0); }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class FBO : OpenGLObject {
  int width;
  int height;
  GLuint fbo;
  Texture tex, tex1;
  Texture texdepth;
  //Texture.Option[] xopts;

  override @property uint id () const pure nothrow @nogc => fbo;

  this (Texture atex) { createWithTexture(atex); }

  this (int wdt, int hgt, Texture.Option[] opts...) {
    //xopts = opts.dup;
    createWithTexture(new Texture(wdt, hgt, opts), opts);
  }

  ~this () {
    //FIXME: this may be wrong, as texture may be already destroyed (and it's wrong too); we need refcount for textures
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, 0, 0);
    glDeleteFramebuffersEXT(1, &fbo);
    fbo = 0;
  }

  void clear () {
    if (fbo) {
      // detach texture
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, 0, 0);
      //glDeleteFramebuffersEXT(1, &fbo);
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
      glDeleteFramebuffersEXT(1, &fbo);
      fbo = 0;
    }
    if (tex !is null) tex.clear();
    tex = null;
    if (tex1 !is null) tex1.clear();
    tex1 = null;
    if (texdepth !is null) texdepth.clear();
    texdepth = null;
  }

protected:
  override void activateObj () nothrow @nogc { /*if (fbo)*/ glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo); }
  override void deactivateObj () nothrow @nogc { glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); }

  // this will deactivate current FBO!
  /+
  void replaceTexture (Texture ntex) {
    if (tex !is null) {
      if (ntex !is null && ntex.tid == tex.tid) return;
    } else {
      if (ntex is null) return;
    }
    glGenFramebuffersEXT(1, &fbo);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    scope(exit) glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    // detach texture
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
    glDeleteFramebuffersEXT(1, &fbo);
    fbo = 0;
    tex = ntex;
    // attach texture
    if (tex !is null) {
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex.tid, 0);
      {
        GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
        if (status != GL_FRAMEBUFFER_COMPLETE_EXT) assert(0, "framebuffer fucked!");
      }
    }
  }
  +/

private:
  void createWithTexture (Texture atex, Texture.Option[] opts...) {
    assert(atex !is null && atex.tid);

    bool need2 = false;
    foreach (immutable opt; opts) if (opt == Texture.Option.FBO2) { need2 = true; break; }

    tex = atex;
    glGenFramebuffersEXT(1, &fbo);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    scope(exit) glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    // attach 2D texture to this FBO
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex.tid, 0);
    // GL_COLOR_ATTACHMENT0_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_STENCIL_ATTACHMENT_EXT

    if (need2) {
      tex1 = new Texture(tex.width, tex.height, opts);
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, tex1.tid, 0);
    }

    /+
    void createDepth () {
      uint fboDepthId;
      glGenRenderbuffersEXT(1, &fboDepthId);
      glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, fboDepthId);
      glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT/*24*/, atex.width, atex.height);
      // attach depth buffer to FBO
      glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, fboDepthId);
      // kill it (we don't need it anymore)
      glDeleteRenderbuffersEXT(1, &fboDepthId);
    }
    +/

    /*
    foreach (Texture.Option opt; opts) {
      if (opt == Texture.Option.Depth) {
        createDepth();
        break;
      }
    }
    */
    //createDepth();

    {
      GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
      if (status != GL_FRAMEBUFFER_COMPLETE_EXT) assert(0, "framebuffer fucked!");
    }
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    width = tex.width;
    height = tex.height;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct SVec2I { int x, y; }
public struct SVec3I { int x, y, z; alias r = x; alias g = y; alias b = z; }
public struct SVec4I { int x, y, z, w; alias r = x; alias g = y; alias b = z; alias a = w; }

public struct SVec2F { float x, y; }
public struct SVec3F { float x, y, z; alias r = x; alias g = y; alias b = z; }
public struct SVec4F { float x, y, z, w; alias r = x; alias g = y; alias b = z; alias a = w; }


public final class Shader : OpenGLObject {
  string shaderName;
  GLuint prg = 0;
  GLint[string] vars;

  override @property uint id () const pure nothrow @nogc => prg;

  this (string ashaderName, const(char)[] src) {
    shaderName = ashaderName;
    if (src.length > int.max) {
      import core.stdc.stdio : printf;
      printf("shader '%.*s' code too long!", cast(uint)ashaderName.length, ashaderName.ptr);
      assert(0);
    }
    auto shaderId = glCreateShader(GL_FRAGMENT_SHADER);
    auto sptr = src.ptr;
    GLint slen = cast(int)src.length;
    glShaderSource(shaderId, 1, &sptr, &slen);
    glCompileShader(shaderId);
    GLint success = 0;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
    if (!success || glutilsShowShaderWarnings) {
      import core.stdc.stdio : printf;
      import core.stdc.stdlib : malloc, free;
      GLint logSize = 0;
      glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);
      if (logSize > 0) {
        auto logStrZ = cast(GLchar*)malloc(logSize);
        glGetShaderInfoLog(shaderId, logSize, null, logStrZ);
        printf("shader '%.*s' compilation messages:\n%s\n", cast(uint)ashaderName.length, ashaderName.ptr, logStrZ);
        free(logStrZ);
      }
    }
    if (!success) assert(0);
    prg = glCreateProgram();
    glAttachShader(prg, shaderId);
    glLinkProgram(prg);
  }

  GLint varId(NT) (NT vname) if (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])) {
    GLint id = -1;
    if (vname.length > 0 && vname.length <= 128) {
      if (auto vi = vname in vars) {
        id = *vi;
      } else {
        char[129] buf = void;
        buf[0..vname.length] = vname[];
        buf[vname.length] = 0;
        id = glGetUniformLocation(prg, buf.ptr);
        //{ import core.stdc.stdio; printf("[%.*s.%s]=%i\n", cast(uint)shaderName.length, shaderName.ptr, buf.ptr, id); }
        static if (is(NT == immutable(char)[])) {
          vars[vname.idup] = id;
        } else {
          vars[vname.idup] = id;
        }
        if (id < 0) {
          import core.stdc.stdio : printf;
          printf("shader '%.*s': unknown variable '%.*s'\n", cast(uint)shaderName.length, shaderName.ptr, cast(uint)vname.length, vname.ptr);
        }
      }
    }
    return id;
  }

  // get unified var id
  GLint opIndex(NT) (NT vname) if (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])) {
    auto id = varId(vname);
    if (id < 0) {
      import core.stdc.stdio : printf;
      printf("shader '%.*s': unknown variable '%.*s'\n", cast(uint)shaderName.length, shaderName.ptr, cast(uint)vname.length, vname.ptr);
      assert(0);
    }
    return id;
  }

  private import std.traits;
  void opIndexAssign(T, NT) (in auto ref T v, NT vname)
  if (((isIntegral!T && T.sizeof <= 4) || (isFloatingPoint!T && T.sizeof == float.sizeof) || isBoolean!T ||
       is(T : SVec2I) || is(T : SVec3I) || is(T : SVec4I) ||
       is(T : SVec2F) || is(T : SVec3F) || is(T : SVec4F)) &&
      (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])))
  {
    auto id = varId(vname);
    if (id < 0) return;
    //{ import core.stdc.stdio; printf("setting '%.*s' (%d)\n", cast(uint)vname.length, vname.ptr, id); }
         static if (isIntegral!T || isBoolean!T) glUniform1i(id, cast(int)v);
    else static if (isFloatingPoint!T) glUniform1f(id, cast(float)v);
    else static if (is(SVec2I : T)) glUniform2i(id, cast(int)v.x, cast(int)v.y);
    else static if (is(SVec3I : T)) glUniform3i(id, cast(int)v.x, cast(int)v.y, cast(int)v.z);
    else static if (is(SVec4I : T)) glUniform4i(id, cast(int)v.x, cast(int)v.y, cast(int)v.z, cast(int)v.w);
    else static if (is(SVec2F : T)) glUniform2f(id, cast(float)v.x, cast(float)v.y);
    else static if (is(SVec3F : T)) glUniform3f(id, cast(float)v.x, cast(float)v.y, cast(float)v.z);
    else static if (is(SVec4F : T)) glUniform4f(id, cast(float)v.x, cast(float)v.y, cast(float)v.z, cast(float)v.w);
    else static assert(0, "wtf?!");
  }

protected:
  override void activateObj () nothrow @nogc { /*if (prg)*/ glUseProgram(prg); }
  override void deactivateObj () nothrow @nogc { glUseProgram(0); }
}


// ////////////////////////////////////////////////////////////////////////// //
//private import std.traits;

public:
void exec(TO) (TO obj, scope void delegate () dg) if (is(typeof(() { obj.activate(); obj.deactivate(); }))) {
  obj.activate();
  scope(exit) obj.deactivate();
  dg();
}

void exec(TO, TG) (TO obj, scope TG dg) if (is(typeof((TO obj) { dg(obj); })) && is(typeof(() { obj.activate(); obj.deactivate(); }))) {
  obj.activate();
  scope(exit) obj.deactivate();
  dg(obj);
}


// ////////////////////////////////////////////////////////////////////////// //
void orthoCamera (int wdt, int hgt) {
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  // left, right, bottom, top, near, far
  //glOrtho(0, wdt, 0, hgt, -1, 1); // bottom-to-top
  glOrtho(0, wdt, hgt, 0, -1, 1); // top-to-bottom
  glViewport(0, 0, wdt, hgt);
}
//glTranslatef(-cx, -cy, 0.0f);


// origin is texture left top
void drawAtXY (GLuint tid, int x, int y, int w, int h, bool mirrorX=false, bool mirrorY=false) {
  if (!tid || w < 1 || h < 1) return;
  w += x;
  h += y;
  if (mirrorX) { int tmp = x; x = w; w = tmp; }
  if (mirrorY) { int tmp = y; y = h; h = tmp; }
  bindTexture(tid);
  glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex2i(x, y); // top-left
    glTexCoord2f(1.0f, 0.0f); glVertex2i(w, y); // top-right
    glTexCoord2f(1.0f, 1.0f); glVertex2i(w, h); // bottom-right
    glTexCoord2f(0.0f, 1.0f); glVertex2i(x, h); // bottom-left
  glEnd();
}


// origin is texture center
void drawAtXYC (Texture tex, int x, int y, bool mirrorX=false, bool mirrorY=false) {
  if (tex is null || !tex.tid) return;
  x -= tex.width/2;
  y -= tex.height/2;
  drawAtXY(tex.tid, x, y, tex.width, tex.height, mirrorX, mirrorY);
}


// origin is texture left top
void drawAtXY (Texture tex, int x, int y, bool mirrorX=false, bool mirrorY=false) {
  if (tex is null || !tex.tid) return;
  drawAtXY(tex.tid, x, y, tex.width, tex.height, mirrorX, mirrorY);
}


private __gshared GLuint boundTexture = 0;

// make sure that texture unit 0 is active!
void bindTexture (GLuint tid) nothrow @trusted @nogc {
  if (tid != boundTexture) {
    boundTexture = tid;
    glBindTexture(GL_TEXTURE_2D, tid);
  }
}
