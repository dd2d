/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// uniform grid to speed up collision detection
module d2dunigrid is aliced;

private import dacs.actor;
private import d2dadefs : AF_NOCOLLISION;


// ////////////////////////////////////////////////////////////////////////// //
public: // api


void ugInit (int width, int height, int cellSizeX=32, int cellSizeY=32) {
  assert(width > 0 && height > 0);
  assert(cellSizeX > 0 && cellSizeY > 0);
  gwidth = (width+cellSizeX-1)/cellSizeX;
  gheight = (height+cellSizeY-1)/cellSizeY;
  gcw = cellSizeX;
  gch = cellSizeY;
  grid.length = gwidth*gheight;
  grid[] = GridBucket.init;
}


void ugClear () {
  foreach (ref bt; grid) bt.actCount = bt.mactCount = 0;
}


// x, y: center
void ugModifyRadius(bool doput) (uint aid, int x, int y, int radius) {
  pragma(inline, true);
  doModifyAABB!doput(aid, x-radius, y-radius, x+radius, y+radius);
}


// `x1` and `y1` are included
void ugModifyAABB(bool doput) (uint aid, int x0, int y0, int x1, int y1) {
  pragma(inline, true);
  doModifyAABB!doput(aid, x0, y0, x1, y1);
}


uint[] ugHitListRadius (uint[] dest, int x, int y, int radius) {
  pragma(inline, true);
  return getHitListAABB(dest, x-radius, y-radius, x+radius, y+radius);
}


uint[] ugHitListAABB (uint[] dest, int x0, int y0, int x1, int y1) {
  pragma(inline, true);
  return getHitListAABB(dest, x0, y0, x1, y1);
}


// i HAET copypasta!
void ugActorModify(bool doput) (ActorId a) {
  if (!a.valid) return;

  if (a.fget_flags&AF_NOCOLLISION) return;

  int ax = a.fget_x;
  int ay = a.fget_y;
  int ar = a.fget_radius;
  int ah = a.fget_height;
  if (ar < 1 || ah < 1) return;

  ugModifyAABB!doput(a.id, ax-ar, ay-ah+1, ax+ar, ay);
}


// i HAET copypasta!
uint[] ugActorPossibleHitList (ActorId a, uint[] dest) {
  if (!a.valid) return null;

  if (a.fget_flags&AF_NOCOLLISION) return null;

  int ax = a.fget_x;
  int ay = a.fget_y;
  int ar = a.fget_radius;
  int ah = a.fget_height;
  if (ar < 1 || ah < 1) return null;

  return ugHitListAABB(dest, ax-ar, ay-ah+1, ax+ar, ay);
}


// i HAET copypasta!
uint[] ugActorHitList (ActorId a, uint[] dest) {
  auto res = ugActorPossibleHitList(a, dest);

  // and filter it
  for (usize pos = 0; pos < res.length; ++pos) {
    bool killIt = (res.ptr[pos] == a.id);
    if (!killIt) {
      // check for real collision
      import dengapi : actorsOverlap;
      killIt = !actorsOverlap(a, ActorId(res.ptr[pos]));
    }
    if (killIt) {
      // no collision, remove this actor from list
      foreach (immutable c; pos+1..res.length) res.ptr[c-1] = res.ptr[c];
      res.length -= 1;
      --pos; // compensate loop increment
    }
  }

  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
private: // api

mixin(Actor.FieldGetMixin!("x", int));
mixin(Actor.FieldGetMixin!("y", int));
mixin(Actor.FieldGetMixin!("height", int));
mixin(Actor.FieldGetMixin!("radius", int));
mixin(Actor.FieldGetMixin!("flags", uint));


struct GridBucket {
  uint[32] acts; // actorids
  uint actCount;
  uint[] macts; // dynamically grows
  uint mactCount; // actorids

  @disable this (this); // no copies
}

__gshared int gwidth, gheight; // in cells
__gshared int gcw, gch; // cell size
__gshared GridBucket[] grid;


GridBucket* calcNormAABB (ref int x0, ref int y0, ref int x1, ref int y1) {
  x0 /= gcw;
  x1 /= gcw;
  y0 /= gch;
  y1 /= gch;
  if (x0 < 0) x0 = 0;
  if (y0 < 0) y0 = 0;
  if (x1 >= gwidth) x1 = gwidth-1;
  if (y1 >= gheight) y1 = gheight-1;
  if (x0 > x1 || y0 > y1 || x1 < 0 || y1 < 0 || x0 >= gwidth || y0 >= gheight) return null;
  return grid.ptr+y0*gwidth+x0;
}


// `x1` and `y1` are included
void doModifyAABB(bool doadd) (uint aid, int x0, int y0, int x1, int y1) @trusted {
  GridBucket* gdata = calcNormAABB(ref x0, ref y0, ref x1, ref y1);
  if (gdata is null) return;
  for (; y0 <= y1; ++y0, gdata += gwidth) {
    auto p = gdata;
    for (int x = x0; x <= x1; ++x, ++p) {
      bool found = false;
      static if (doadd) {
        // add
        foreach (uint id; p.acts[0..p.actCount]) if (id == aid) { found = true; break; }
        if (!found) foreach (uint id; p.macts[0..p.mactCount]) if (id == aid) { found = true; break; }
        if (!found) {
          if (p.actCount < p.acts.length) {
            // easy case
            p.acts.ptr[p.actCount++] = aid;
          } else if (p.mactCount < p.macts.length) {
            // second easy case
            p.macts.ptr[p.mactCount++] = aid;
          } else {
            assert(p.mactCount == p.macts.length);
            // alas, we have to allocate
            p.macts.assumeSafeAppend; // no, really
            p.macts.length += 1;
            p.macts.ptr[p.mactCount++] = aid;
          }
        }
      } else {
        // remove
        foreach (immutable idx, uint id; p.acts[0..p.actCount]) {
          if (id == aid) {
            // kill the clown!
            found = true;
            foreach (immutable c; idx+1..p.actCount) p.acts.ptr[c-1] = p.acts.ptr[c];
            break;
          }
        }
        if (!found) {
          foreach (immutable idx, uint id; p.macts[0..p.mactCount]) {
            if (id == aid) {
              // kill the clown!
              foreach (immutable c; idx+1..p.mactCount) p.macts.ptr[c-1] = p.macts.ptr[c];
              break;
            }
          }
        }
      }
    }
  }
}


// i HAET copypasta!
// `x1` and `y1` are included
uint[] getHitListAABB (uint[] dest, int x0, int y0, int x1, int y1) @trusted {
  // we have alot of memory!
  __gshared uint[65536] wasSeen = 0;
  __gshared uint hitCount = 0;

  GridBucket* gdata = calcNormAABB(ref x0, ref y0, ref x1, ref y1);
  if (gdata is null) return null;

  if (++hitCount == 0) {
    // rare case of overflow
    hitCount = 1;
    wasSeen[] = 0;
  }

  uint dpos = 0;

  bool addToRes (uint id) {
    if (wasSeen.ptr[id&0xffff] != hitCount) {
      if (dpos >= dest.length) return false;
      wasSeen.ptr[id&0xffff] = hitCount;
      dest.ptr[dpos++] = id;
    }
    return true;
  }

  mainloop: for (; y0 <= y1; ++y0, gdata += gwidth) {
    auto p = gdata;
    for (int x = x0; x <= x1; ++x, ++p) {
      foreach (uint id; p.acts[0..p.actCount]) if (!addToRes(id)) break mainloop;
      foreach (uint id; p.macts[0..p.mactCount]) if (!addToRes(id)) break mainloop;
    }
  }
  return dest[0..dpos];
}
