/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module render is aliced;

private:
import core.atomic;
import core.thread;
import core.time;

import std.concurrency;

import iv.cmdcon /*: conwriteln, consoleLock, consoleUnlock*/;
import iv.glbinds;
import glutils;
import wadarc;

import iv.vfs;

import d2dmap;
import d2dadefs;
import d2dimage;
import d2dfont;
import dacs;

import d2dunigrid;

// `map` is there
import dengapi;

import d2dparts;


// ////////////////////////////////////////////////////////////////////////// //
import arsd.simpledisplay : SimpleWindow, KeyEvent, MouseEvent;
import arsd.color;
import arsd.png;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared bool cheatNoDoors = false;
public __gshared bool cheatNoWallClip = false;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared SimpleWindow sdwindow;


public enum vlWidth = 800;
public enum vlHeight = 800;
__gshared int scale = 2;

public int getScale () nothrow @trusted @nogc { pragma(inline, true); return scale; }


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool levelLoaded = false;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool scanlines = false;
__gshared bool doLighting = true;
__gshared bool gamePaused = false;
__gshared bool rConsoleVisible = false;
__gshared int rConsoleHeight = 16*3;
__gshared uint rConTextColor = 0x00ff00; // rgb
__gshared uint rConCursorColor = 0xff7f00; // rgb
__gshared uint rConInputColor = 0xffff00; // rgb
__gshared uint rConPromptColor = 0xffffff; // rgb
__gshared bool renderVBL = true;
__gshared bool oldRenderVBL = false;
shared bool editMode = false;
shared bool vquitRequested = false;

public @property bool inEditMode () nothrow @trusted @nogc { import core.atomic; return atomicLoad(editMode); }
@property void inEditMode (bool v) nothrow @trusted @nogc { import core.atomic; atomicStore(editMode, v); }

public @property bool quitRequested () nothrow @trusted @nogc { import core.atomic; return atomicLoad(vquitRequested); }
public @property bool conVisible () nothrow @trusted @nogc { return rConsoleVisible; }


// ////////////////////////////////////////////////////////////////////////// //
__gshared char[] concmdbuf;
__gshared uint concmdbufpos;
private import core.sync.mutex : Mutex;
shared static this () { concmdbuf.length = 65536; }

__gshared int conskiplines = 0;


void concmdAdd (const(char)[] s) {
  if (s.length) {
    if (concmdbuf.length-concmdbufpos < s.length+1) {
      concmdbuf.assumeSafeAppend.length += s.length-(concmdbuf.length-concmdbufpos)+512;
    }
    if (concmdbufpos > 0 && concmdbuf[concmdbufpos-1] != '\n') concmdbuf.ptr[concmdbufpos++] = '\n';
    concmdbuf[concmdbufpos..concmdbufpos+s.length] = s[];
    concmdbufpos += s.length;
  }
}


// `null`: no more
void concmdDoAll () {
  if (concmdbufpos == 0) return;
  scope(exit) concmdbufpos = 0;
  auto ebuf = concmdbufpos;
  const(char)[] s = concmdbuf[0..concmdbufpos];
  for (;;) {
    while (s.length) {
      auto cmd = conGetCommandStr(s);
      if (cmd is null) break;
      try {
        conExecute(cmd);
      } catch (Exception e) {
        conwriteln("***ERROR: ", e.msg);
      }
    }
    if (concmdbufpos <= ebuf) break;
    s = concmdbuf[ebuf..concmdbufpos];
    ebuf = concmdbufpos;
  }
}


void concliChar (char ch) {
  if (!ch) return;
  consoleLock();
  scope(exit) consoleUnlock();

  if (ch == ConInputChar.PageUp) {
    int lnx = (rConsoleHeight-4)/conCharHeight-2;
    if (lnx < 1) lnx = 1;
    conskiplines += lnx;
    conLastChange = 0;
    return;
  }

  if (ch == ConInputChar.PageDown) {
    if (conskiplines > 0) {
      int lnx = (rConsoleHeight-4)/conCharHeight-2;
      if (lnx < 1) lnx = 1;
      if ((conskiplines -= lnx) < 0) conskiplines = 0;
      conLastChange = 0;
    }
    return;
  }

  if (ch == ConInputChar.Enter) {
    if (conskiplines) { conskiplines = 0; conLastChange = 0; }
    auto s = conInputBuffer;
    if (s.length > 0) {
      concmdAdd(s);
      conInputBufferClear(true); // add to history
      conLastChange = 0;
    }
    return;
  }

  if (ch == '`' && conInputBuffer.length == 0) { concmd("r_console ona"); return; }

  auto pcc = conInputLastChange();
  conAddInputChar(ch);
  if (pcc != conInputLastChange()) conLastChange = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  conRegFunc!((const(char)[] fname) {
    try {
      auto s = loadTextFile(fname);
      concmd(s);
    } catch (Exception e) {
      conwriteln("ERROR loading script \"", fname, "\"");
    }
  })("exec", "execute console script");
  conRegVar!doLighting("r_lighting", "dynamic lighting");
  conRegVar!renderVBL("r_vsync", "sync to vblank");
  conRegVar!gamePaused("g_pause", "pause game");
  conRegVar!rConsoleVisible("r_console", "console visibility");
  conRegVar!rConsoleHeight(16*3, vlHeight, "r_conheight", "console height");
  conRegVar!rConTextColor("r_contextcolor", "console log text color, 0xrrggbb");
  conRegVar!rConCursorColor("r_concursorcolor", "console cursor color, 0xrrggbb");
  conRegVar!rConInputColor("r_coninputcolor", "console input color, 0xrrggbb");
  conRegVar!rConPromptColor("r_conpromptcolor", "console prompt color, 0xrrggbb");
  rConsoleHeight = vlHeight-vlHeight/3;
  rConsoleHeight = vlHeight/2;
  conRegVar!frameInterpolation("r_interpolation", "interpolate camera and sprite movement");
  conRegVar!scale(1, 2, "r_scale", "screen scale");
  conRegFunc!({
    cheatNoDoors = !cheatNoDoors;
    if (cheatNoDoors) conwriteln("player ignores doors"); else conwriteln("player respects doors");
  })("nodoorclip", "ignore doors");
  conRegFunc!({
    cheatNoWallClip = !cheatNoWallClip;
    if (cheatNoWallClip) conwriteln("player ignores walls"); else conwriteln("player respects walls");
  })("nowallclip", "ignore walls");
  conRegFunc!({
    import core.atomic;
    atomicStore(vquitRequested, true);
  })("quit", "quit game");
  conRegFunc!((const(char)[] msg, int pauseMsecs=3000, bool noreplace=false) {
    char[256] buf;
    auto s = buf.conFormatStr(msg);
    if (s.length) postAddMessage(s, pauseMsecs, noreplace);
  })("hudmsg", "show hud message; hudmsg msg [pausemsecs [noreplace]]");
}


// ////////////////////////////////////////////////////////////////////////// //
// interpolation
__gshared ubyte[] prevFrameActorsData;
__gshared uint[65536] prevFrameActorOfs; // uint.max-1: dead; uint.max: last
__gshared MonoTime lastthink = MonoTime.zero; // for interpolator
__gshared MonoTime nextthink = MonoTime.zero;
__gshared bool frameInterpolation = true;


// ////////////////////////////////////////////////////////////////////////// //
// attached lights
struct AttachedLightInfo {
  enum Type {
    Point,
    Ambient,
  }
  Type type;
  int x, y;
  int w, h; // for ambient lights
  float r, g, b;
  bool uncolored;
  int radius;
}

__gshared AttachedLightInfo[65536] attachedLights;
__gshared uint attachedLightCount = 0;


// ////////////////////////////////////////////////////////////////////////// //
enum MaxLightRadius = 256;


// ////////////////////////////////////////////////////////////////////////// //
// for light
__gshared FBO[MaxLightRadius+1] fboDistMap;
__gshared FBO /*fboLSpot,*/ fboLSpotBG, fboLSpotSmall;
__gshared Shader shadLightTrace, shadLightBlur, shadLightGeom, shadLightAmbient;
__gshared TrueColorImage editorImg;
__gshared FBO fboEditor;
__gshared FBO fboConsole;
__gshared Texture texSigil;

__gshared FBO fboLevel, fboLevelLight, fboOrigBack, fboLMaskSmall;
__gshared Shader shadScanlines;
__gshared Shader shadLiquidDistort;


// ////////////////////////////////////////////////////////////////////////// //
// call once!
public void initOpenGL () {
  gloStackClear();

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);

  // create shaders
  shadScanlines = new Shader("scanlines", loadTextFile("shaders/srscanlines.frag"));

  shadLiquidDistort = new Shader("liquid_distort", loadTextFile("shaders/srliquid_distort.frag"));
  shadLiquidDistort.exec((Shader shad) {
    shad["texLqMap"] = 0;
  });

  // lights
  shadLightTrace = new Shader("light_trace", loadTextFile("shaders/srlight_trace.frag"));
  shadLightTrace.exec((Shader shad) {
    //shad["texLMap"] = 0;
    shad["texOccFull"] = 2;
    shad["texOccSmall"] = 3;
  });

  shadLightBlur = new Shader("light_blur", loadTextFile("shaders/srlight_blur.frag"));
  shadLightBlur.exec((Shader shad) {
    shad["texDist"] = 0;
    shad["texBg"] = 1;
    shad["texOccFull"] = 2;
    shad["texOccSmall"] = 3;
  });

  shadLightGeom = new Shader("light_geom", loadTextFile("shaders/srlight_geom.frag"));
  shadLightGeom.exec((Shader shad) {
    shad["texLMap"] = 0;
    shad["texBg"] = 1;
    shad["texOccFull"] = 2;
    shad["texOccSmall"] = 3;
  });

  shadLightAmbient = new Shader("light_ambient", loadTextFile("shaders/srlight_ambient.frag"));
  shadLightAmbient.exec((Shader shad) {
    shad["texBg"] = 1;
    shad["texOccFull"] = 2;
    shad["texOccSmall"] = 3;
  });

  fboLSpotBG = new FBO(MaxLightRadius*2, MaxLightRadius*2, Texture.Option.Clamp, Texture.Option.Linear/*, Texture.Option.FBO2*/);
  //fboLSpotBG = new FBO(fboLSpot.width, fboLSpot.height, Texture.Option.Clamp, Texture.Option.Linear);
  fboLSpotSmall = new FBO(fboLSpotBG.width/8, fboLSpotBG.height/8, Texture.Option.Clamp, Texture.Option.Nearest/*Linear*/);
  //TODO: this sux!
  foreach (int sz; 2..MaxLightRadius+1) {
    fboDistMap[sz] = new FBO(sz*2, 1, Texture.Option.Clamp, Texture.Option.Linear); // create 1d distance map FBO
  }

  editorImg = new TrueColorImage(vlWidth, vlHeight);
  editorImg.imageData.colors[] = Color(0, 0, 0, 0);
  fboEditor = new FBO(vlWidth, vlHeight, Texture.Option.Nearest);

  texSigil = new Texture("console/sigil_of_baphomet.png", Texture.Option.Nearest);
  fboConsole = new FBO(vlWidth, vlHeight, Texture.Option.Nearest);
  loadConFont();

  // setup matrices
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  loadSmFont();
  loadBfFont();
  loadAllMonsterGraphics();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ulong conLastChange = 0;

static void glColorUint (uint c) {
  pragma(inline, true);
  glColor4f(((c>>16)&0xff)/255.0f, ((c>>8)&0xff)/255.0f, (c&0xff)/255.0f, 1.0f);
}


void renderConsoleFBO () {
  enum XOfs = 2;
  if (conLastChange == cbufLastChange) return;
  consoleLock();
  scope(exit) consoleUnlock();
  // rerender console
  conLastChange = cbufLastChange;
  //foreach (auto s; conbufLinesRev) stdout.writeln(s, "|");
  int skipLines = conskiplines;
  fboConsole.exec((FBO me) {
    bindTexture(0);
    orthoCamera(me.width, me.height);
    // clear it
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    /*
    glDisable(GL_BLEND);
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    glRectf(0, 0, me.width-1, me.height-1);
    */
    // text
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    // draw sigil
    glColor4f(1.0f, 1.0f, 1.0f, 0.2f);
    drawAtXY(texSigil, (me.width-texSigil.width)/2, (vlHeight-rConsoleHeight)/2+(me.height-texSigil.height)/2);
    // draw command line
    int y = me.height-conCharHeight-2;
    {
      glPushMatrix();
      scope(exit) glPopMatrix();
      glTranslatef(XOfs, y, 0);
      int w = conCharWidth('>');
      glColorUint(rConPromptColor);
      conDrawChar('>');
      uint spos = conInputBuffer.length;
      while (spos > 0) {
        char ch = conInputBuffer.ptr[spos-1];
        if (w+conCharWidth(ch) > me.width-XOfs*2-12) break;
        w += conCharWidth(ch);
        --spos;
      }
      glColorUint(rConInputColor);
      foreach (char ch; conInputBuffer[spos..conInputBuffer.length]) conDrawChar(ch);
      // cursor
      bindTexture(0);
      glColorUint(rConCursorColor);
      glRectf(0, 0, 12, 16);
      y -= conCharHeight;
    }
    // draw console text
    glColorUint(rConTextColor);
    glPushMatrix();
    scope(exit) glPopMatrix();
    glTranslatef(XOfs, y, 0);

    void putLine(T) (auto ref T line, usize pos=0) {
      if (y+conCharHeight <= 0) return;
      int w = XOfs;
      usize sp = pos;
      while (sp < line.length) {
        char ch = line[sp++];
        int cw = conCharWidth(ch);
        if ((w += cw) > me.width-XOfs) { w -= cw; --sp; break; }
      }
      if (sp < line.length) putLine(line, sp); // recursive put tail
      // draw line
      if (skipLines-- <= 0) {
        while (pos < sp) conDrawChar(line[pos++]);
        glPopMatrix();
        glPushMatrix();
        y -= conCharHeight;
        glTranslatef(XOfs, y, 0);
      }
    }

    foreach (auto line; conbufLinesRev) {
      putLine(line);
      if (y+conCharHeight <= 0) break;
    }
  });
}


// ////////////////////////////////////////////////////////////////////////// //
// should be called when OpenGL is initialized
void loadMap (string mapname) {
  mapscripts.runUnloading(); // "map unloading" script
  clearMapScripts();

  if (map !is null) map.clear();
  map = new LevelMap(mapname);
  curmapname = mapname;

  ugInit(map.width*TileSize, map.height*TileSize);

  map.oglBuildMega();
  mapTilesChanged = 0;

  if (fboLevel !is null) fboLevel.clear();
  if (fboLevelLight !is null) fboLevelLight.clear();
  if (fboOrigBack !is null) fboOrigBack.clear();
  if (fboLMaskSmall !is null) fboLMaskSmall.clear();

  fboLevel = new FBO(map.width*TileSize, map.height*TileSize, Texture.Option.Nearest); // final level render will be here
  fboLevelLight = new FBO(map.width*TileSize, map.height*TileSize, Texture.Option.Nearest); // level lights will be rendered here
  fboOrigBack = new FBO(map.width*TileSize, map.height*TileSize, Texture.Option.Nearest/*, Texture.Option.Depth*/); // background+foreground
  fboLMaskSmall = new FBO(map.width, map.height, Texture.Option.Nearest); // small lightmask

  //shadLightTrace.exec((Shader shad) { shad["mapPixSize"] = SVec2F(map.width*TileSize-1, map.height*TileSize-1); });
  shadLightTrace.exec((Shader shad) { shad["mapPixSize"] = SVec2F(map.width*TileSize, map.height*TileSize); });
  shadLightBlur.exec((Shader shad) { shad["mapPixSize"] = SVec2F(map.width*TileSize, map.height*TileSize); });
  shadLightGeom.exec((Shader shad) { shad["mapPixSize"] = SVec2F(map.width*TileSize, map.height*TileSize); });
  shadLightAmbient.exec((Shader shad) { shad["mapPixSize"] = SVec2F(map.width*TileSize, map.height*TileSize); });

  glActiveTexture(GL_TEXTURE0+0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  orthoCamera(vlWidth, vlHeight);

  Actor.resetStorage();

  setupMapScripts();
  mapscripts.runInit();
  loadMapMonsters();
  dotInit();
  mapscripts.runLoaded();

  // save first snapshot
  if (prevFrameActorsData.length == 0) prevFrameActorsData = new ubyte[](Actor.actorSize*65536); // ~15-20 megabytes
  prevFrameActorOfs[] = uint.max; // just for fun
  Actor.saveSnapshot(prevFrameActorsData[], prevFrameActorOfs.ptr);

  levelLoaded = true;
  rebuildMapMegaTextures();

  { import core.memory : GC; GC.collect(); }
}


// ////////////////////////////////////////////////////////////////////////// //
//FIXME: optimize!
__gshared uint mapTilesChanged = 0;


//WARNING! this can be called only from DACS, so we don't have to sync it!
public void mapDirty (uint layermask) { mapTilesChanged |= layermask; }


void rebuildMapMegaTextures () {
  //fbo.replaceTexture
  //mapTilesChanged = false;
  //map.clearMegaTextures();
  map.oglBuildMega(mapTilesChanged);
  mapTilesChanged = 0;
  dotsAwake(); // let dormant dots fall
  // rebuild small occluders texture
  glDisable(GL_BLEND);
  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  fboLMaskSmall.exec({
    orthoCamera(map.width, map.height);
    drawAtXY(map.texgl.ptr[map.LightMask].tid, 0, 0, map.width, map.height, mirrorY:true);
  });
}


// ////////////////////////////////////////////////////////////////////////// //
// messages
struct Message {
  enum Phase { FadeIn, Stay, FadeOut }
  Phase phase;
  int alpha;
  int pauseMsecs;
  MonoTime removeTime;
  char[256] text;
  usize textlen;
}

//private import core.sync.mutex : Mutex;

__gshared Message[128] messages;
__gshared uint messagesUsed = 0;

//__gshared Mutex messageLock;
//shared static this () { messageLock = new Mutex(); }


void addMessage (const(char)[] msgtext, int pauseMsecs=3000, bool noreplace=false) {
  //messageLock.lock();
  //scope(exit) messageLock.unlock();
  if (msgtext.length == 0) return;
  conwriteln(msgtext);
  if (pauseMsecs <= 50) return;
  if (messagesUsed == messages.length) {
    // remove top message
    foreach (immutable cidx; 1..messagesUsed) messages.ptr[cidx-1] = messages.ptr[cidx];
    messages.ptr[0].alpha = 255;
    --messagesUsed;
  }
  // quick replace
  if (!noreplace && messagesUsed == 1) {
    switch (messages.ptr[0].phase) {
      case Message.Phase.FadeIn:
        messages.ptr[0].phase = Message.Phase.FadeOut;
        break;
      case Message.Phase.Stay:
        messages.ptr[0].phase = Message.Phase.FadeOut;
        messages.ptr[0].alpha = 255;
        break;
      default:
    }
  }
  auto msg = messages.ptr+messagesUsed;
  ++messagesUsed;
  msg.phase = Message.Phase.FadeIn;
  msg.alpha = 0;
  msg.pauseMsecs = pauseMsecs;
  // copy text
  if (msgtext.length > msg.text.length) {
    msg.text = msgtext[0..msg.text.length];
    msg.textlen = msg.text.length;
  } else {
    msg.text[0..msgtext.length] = msgtext[];
    msg.textlen = msgtext.length;
  }
}


void doMessages (MonoTime curtime) {
  //messageLock.lock();
  //scope(exit) messageLock.unlock();

  if (messagesUsed == 0) return;
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  Message* msg;

 again:
  msg = messages.ptr;
  final switch (msg.phase) {
    case Message.Phase.FadeIn:
      if ((msg.alpha += 10) >= 255) {
        msg.phase = Message.Phase.Stay;
        msg.removeTime = curtime+dur!"msecs"(msg.pauseMsecs);
        goto case; // to stay
      }
      glColor4f(1.0f, 1.0f, 1.0f, msg.alpha/255.0f);
      break;
    case Message.Phase.Stay:
      if (msg.removeTime <= curtime) {
        msg.alpha = 255;
        msg.phase = Message.Phase.FadeOut;
        goto case; // to fade
      }
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      break;
    case Message.Phase.FadeOut:
      if ((msg.alpha -= 10) <= 0) {
        if (--messagesUsed == 0) return;
        // remove this message
        foreach (immutable cidx; 1..messagesUsed+1) messages.ptr[cidx-1] = messages.ptr[cidx];
        goto again;
      }
      glColor4f(1.0f, 1.0f, 1.0f, msg.alpha/255.0f);
      break;
  }

  smDrawText(10, 10, msg.text[0..msg.textlen]);
}


// ////////////////////////////////////////////////////////////////////////// //
//mixin(Actor.FieldPropMixin!("0drawlistpos", uint));

mixin(Actor.FieldGetMixin!("classtype", StrId)); // fget_classtype
mixin(Actor.FieldGetMixin!("classname", StrId)); // fget_classname
mixin(Actor.FieldGetMixin!("x", int));
mixin(Actor.FieldGetMixin!("y", int));
mixin(Actor.FieldGetMixin!("s", int));
mixin(Actor.FieldGetMixin!("radius", int));
mixin(Actor.FieldGetMixin!("height", int));
mixin(Actor.FieldGetMixin!("flags", uint));
mixin(Actor.FieldGetMixin!("zAnimstate", StrId));
mixin(Actor.FieldGetMixin!("zAnimidx", int));
mixin(Actor.FieldGetMixin!("dir", uint));
mixin(Actor.FieldGetMixin!("attLightXOfs", int));
mixin(Actor.FieldGetMixin!("attLightYOfs", int));
mixin(Actor.FieldGetMixin!("attLightRGBX", uint));

//mixin(Actor.FieldGetPtrMixin!("classtype", StrId)); // fget_classtype
//mixin(Actor.FieldGetPtrMixin!("classname", StrId)); // fget_classname
mixin(Actor.FieldGetPtrMixin!("x", int));
mixin(Actor.FieldGetPtrMixin!("y", int));
//mixin(Actor.FieldGetPtrMixin!("flags", uint));
//mixin(Actor.FieldGetPtrMixin!("zAnimstate", StrId));
//mixin(Actor.FieldGetPtrMixin!("zAnimidx", int));
//mixin(Actor.FieldGetPtrMixin!("dir", uint));
//mixin(Actor.FieldGetPtrMixin!("attLightXOfs", int));
//mixin(Actor.FieldGetPtrMixin!("attLightYOfs", int));
//mixin(Actor.FieldGetPtrMixin!("attLightRGBX", uint));


// ////////////////////////////////////////////////////////////////////////// //
__gshared int vportX0, vportY0, vportX1, vportY1;


// ////////////////////////////////////////////////////////////////////////// //
void renderLightAmbient() (int lightX, int lightY, int lightW, int lightH, in auto ref SVec4F lcol) {
  //conwriteln("!!!000: (", lightX, ",", lightY, ")-(", lightW, ",", lightH, "): r=", cast(uint)(255.0f*lcol.r), "; g=", cast(uint)(255.0f*lcol.g), "; b=", cast(uint)(255.0f*lcol.b), "; a=", cast(uint)(255.0f*lcol.a));
  if (lightW < 1 || lightH < 1) return;
  int lightX1 = lightX+lightW-1;
  int lightY1 = lightY+lightH-1;
  // clip light to viewport
  if (lightX < vportX0) lightX = vportX0;
  if (lightY < vportY0) lightY = vportY0;
  if (lightX1 > vportX1) lightX1 = vportX1;
  if (lightY1 > vportY1) lightY1 = vportY1;
  // is this light visible?
  //conwriteln("!!!001: (", lightX, ",", lightY, ")-(", lightX1, ",", lightY1, "): r=", cast(uint)(255.0f*lcol.r), "; g=", cast(uint)(255.0f*lcol.g), "; b=", cast(uint)(255.0f*lcol.b), "; a=", cast(uint)(255.0f*lcol.a));
  if (lightX1 < lightX || lightY1 < lightY || lightX > vportX1 || lightY > vportY1 || lightX1 < vportX0 || lightY1 < vportY0) return;
  //conwriteln("!!!002: (", lightX, ",", lightY, ")-(", lightX1, ",", lightY1, "): r=", cast(uint)(255.0f*lcol.r), "; g=", cast(uint)(255.0f*lcol.g), "; b=", cast(uint)(255.0f*lcol.b), "; a=", cast(uint)(255.0f*lcol.a));

  fboLevelLight.exec({
    bindTexture(0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    //glDisable(GL_BLEND);
    orthoCamera(map.width*TileSize, map.height*TileSize);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    shadLightAmbient.exec((Shader shad) {
      shad["lightColor"] = SVec4F(lcol.x, lcol.y, lcol.z, lcol.w);
      //shad["lightPos"] = SVec2F(lightX, lightY);
      glRectf(lightX, lightY, lightX1, lightY1);
    });
  });
}


// ////////////////////////////////////////////////////////////////////////// //
void renderLight() (int lightX, int lightY, in auto ref SVec4F lcol, int lightRadius) {
  if (lightRadius < 2) return;
  if (lightRadius > MaxLightRadius) lightRadius = MaxLightRadius;
  int lightSize = lightRadius*2;
  // is this light visible?
  if (lightX <= -lightRadius || lightY <= -lightRadius || lightX-lightRadius >= map.width*TileSize || lightY-lightRadius >= map.height*TileSize) return;

  // out of viewport -- do nothing
  if (lightX+lightRadius < vportX0 || lightY+lightRadius < vportY0) return;
  if (lightX-lightRadius > vportX1 || lightY-lightRadius > vportY1) return;

  if (lightX >= 0 && lightY >= 0 && lightX < map.width*TileSize && lightY < map.height*TileSize &&
      map.teximgs[map.LightMask].imageData.colors.ptr[lightY*(map.width*TileSize)+lightX].a > 190) return;

  // common color for all the following
  glDisable(GL_BLEND);
  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

  // build 1d distance map to fboShadowMapId
  fboDistMap.ptr[lightRadius].exec({
    // no need to clear it, shader will take care of that
    shadLightTrace.exec((Shader shad) {
      shad["lightTexSize"] = SVec2F(lightSize, lightSize);
      shad["lightPos"] = SVec2F(lightX, lightY);
      orthoCamera(lightSize, 1);
      // it doesn't matter what we will draw here, so just draw filled rect
      glRectf(0, 0, lightSize, 1);
    });
  });

  // build light texture for blending
  fboLSpotBG.exec({
    // no need to clear it, shader will take care of that
    // debug
    // need to clear it, for "small"
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    shadLightBlur.exec((Shader shad) {
      shad["lightTexSize"] = SVec2F(lightSize, MaxLightRadius*2); // x: size of distmap; y: size of this texture
      shad["lightColor"] = SVec4F(lcol.x, lcol.y, lcol.z, lcol.w);
      shad["lightPos"] = SVec2F(lightX-lightRadius, lightY-lightRadius);
      orthoCamera(fboLSpotBG.tex.width, fboLSpotBG.tex.height);
      //drawAtXY(fboDistMap[lightRadius].tex.tid, 0, 0, lightSize, lightSize);
      bindTexture(fboDistMap.ptr[lightRadius].tex.tid);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f); glVertex2i(0, 0); // top-left
        glTexCoord2f(1.0f, 0.0f); glVertex2i(lightSize, 0); // top-right
        glTexCoord2f(1.0f, 1.0f); glVertex2i(lightSize, lightSize); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(0, lightSize); // bottom-left
      glEnd();
    });
  });

  // build "small" light texture for geometry-light shader
  //glUseProgram(0);
  /*version(none)*/ fboLSpotSmall.exec({
    glDisable(GL_BLEND);
    //glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    //orthoCamera(fboLSpotSmall.width, fboLSpotSmall.height);
    //conwriteln(fboLSpotSmall.width, "x", fboLSpotSmall.height, "; ", fboLSpotBG.width, "x", fboLSpotBG.height);
    //orthoCamera(fboLSpotBG.width, fboLSpotBG.height);
    orthoCamera(fboLSpotSmall.width, fboLSpotSmall.height);
    bindTexture(fboLSpotBG.tex.tid);
    float occe = 1.0f*lightSize/(MaxLightRadius*2);
    float occs = 1.0f-occe;
    int w = fboLSpotSmall.width;
    int h = fboLSpotSmall.height;
    int h0 = fboLSpotSmall.height;
    int h1 = fboLSpotSmall.height-h;
    occe = 1.0f;
    occs = 0.0f;
    float occt = 1.0f;
    glBegin(GL_QUADS);
      glTexCoord2f(0.0f, occt); glVertex2i(0, h0); // top-left
      glTexCoord2f(occe, occt); glVertex2i(w, h0); // top-right
      glTexCoord2f(occe, occs); glVertex2i(w, h1); // bottom-right
      glTexCoord2f(0.0f, occs); glVertex2i(0, h1); // bottom-left
    glEnd();
    /*
    bindTexture(0);
    glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
    //glRectf(0, 0, lightSize, lightSize);
    glRectf(0, fboLSpotSmall.height-8, 8, fboLSpotSmall.height);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    */
  });

  // blend light texture
  fboLevelLight.exec({
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    orthoCamera(fboLevelLight.tex.width, fboLevelLight.tex.height);
    int x0 = lightX-lightRadius+0;
    int y1 = lightY-lightRadius+0;
    int x1 = lightX+lightRadius-1+1;
    int y0 = lightY+lightRadius-1+1;
    float occe = 1.0f*lightSize/(MaxLightRadius*2);
    float occs = 1.0f-occe;
    version(smspot) {
      //glDisable(GL_BLEND);
      float occn = 1.0f*lightSize/(MaxLightRadius*2);
      //float occt = 1.0f-occn;
      x0 = ((x0+3)/8)*8;
      y1 = ((y1+3)/8)*8;
      x1 = ((x1+3)/8)*8;
      y0 = ((y0+3)/8)*8;
      //x1 = x0+lightSize;
      //y0 = y1+lightSize;
      bindTexture(fboLSpotSmall.tex.tid);
      glBegin(GL_QUADS);
        /*
        glTexCoord2f(0.0f, 0.0f); glVertex2i(x0, y0); // top-left
        glTexCoord2f(occn, 0.0f); glVertex2i(x1, y0); // top-right
        glTexCoord2f(occn, occn); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, occn); glVertex2i(x0, y1); // bottom-left
        */
        glTexCoord2f(0.0f, occn); glVertex2i(x0, y0); // top-left
        glTexCoord2f(occn, occn); glVertex2i(x1, y0); // top-right
        glTexCoord2f(occn, 0.0f); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, 0.0f); glVertex2i(x0, y1); // bottom-left
      glEnd();
    } else {
      bindTexture(fboLSpotBG.tex.tid);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, occs); glVertex2i(x0, y0); // top-left
        glTexCoord2f(occe, occs); glVertex2i(x1, y0); // top-right
        glTexCoord2f(occe, 1.0f); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(x0, y1); // bottom-left
      glEnd();
    }
    /*
    bindTexture(0);
    glRectf(x0, y0, x1, y1);
    */
    // and blend it again, with the shader that will touch only occluders
    shadLightGeom.exec((Shader shad) {
      //shad["lightTexSize"] = SVec2F(lightSize, lightSize);
      shad["lightTexSize"] = SVec2F(lightSize, fboLSpotSmall.height);
      //shad["lightTexSize"] = SVec2F(lightSize, fboLSpotBG.tex.height);
      shad["lightColor"] = SVec4F(lcol.x, lcol.y, lcol.z, lcol.w);
      shad["lightPos"] = SVec2F(lightX, lightY);
      //shad["lightPos"] = SVec2F(lightX-lightRadius, lightY-lightRadius);
      bindTexture(fboLSpotSmall.tex.tid);
      //bindTexture(fboLSpotBG.tex.tid);
      float occn = 1.0f*lightSize/(MaxLightRadius*2);
      //x0 = ((x0+4)/8)*8;
      //y1 = ((y1+4)/8)*8;
      //x1 = x0+lightSize;
      //y0 = y1+lightSize;
      /*
      x0 = ((x0+4)/8)*8;
      y1 = ((y1+4)/8)*8;
      x1 = ((x1+4)/8)*8;
      y0 = ((y0+4)/8)*8;
      */
      x0 &= ~7;
      y1 = ((y1+7)/8)*8;
      x1 = ((x1+7)/8)*8;
      y0 &= ~7;
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, occn); glVertex2i(x0, y0); // top-left
        glTexCoord2f(occn, occn); glVertex2i(x1, y0); // top-right
        glTexCoord2f(occn, 0.0f); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, 0.0f); glVertex2i(x0, y1); // bottom-left
        /*
        glTexCoord2f(0.0f, occs); glVertex2i(x0, y0); // top-left
        glTexCoord2f(occe, occs); glVertex2i(x1, y0); // top-right
        glTexCoord2f(occe, 1.0f); glVertex2i(x1, y1); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(x0, y1); // bottom-left
        */
      glEnd();
      //drawAtXY(fboLSpotBG.tex.tid, lightX-lightRadius, lightY-lightRadius, lightSize, lightSize, mirrorY:true);
    });
  });
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int testLightX = vlWidth/2, testLightY = vlHeight/2;
__gshared bool testLightMoved = false;
//__gshared int mapOfsX, mapOfsY;
//__gshared bool movement = false;
__gshared float iLiquidTime = 0.0;
//__gshared bool altMove = false;


void renderScene (MonoTime curtime) {
  //enum BackIntens = 0.05f;
  enum BackIntens = 0.0f;

  gloStackClear();
  float atob = (curtime > lastthink ? cast(float)((curtime-lastthink).total!"msecs")/cast(float)((nextthink-lastthink).total!"msecs") : 1.0f);
  if (gamePaused || inEditMode) atob = 1.0f;
  //{ import core.stdc.stdio; printf("atob=%f\n", cast(double)atob); }
  /*
  {
    int framelen = cast(int)((nextthink-lastthink).total!"msecs");
    int curfp = cast(int)((curtime-lastthink).total!"msecs");
    { import core.stdc.stdio; printf("framelen=%d; curfp=%d\n", framelen, curfp); }
  }
  */

  int mofsx, mofsy; // camera offset, will be set in background layer builder

  if (mapTilesChanged != 0) rebuildMapMegaTextures();

  // build background layer
  fboOrigBack.exec({
    //glDisable(GL_BLEND);
    //glClearDepth(1.0f);
    //glDepthFunc(GL_LESS);
    //glDepthFunc(GL_NEVER);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT/*|GL_DEPTH_BUFFER_BIT*/);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    orthoCamera(map.width*TileSize, map.height*TileSize);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // draw sky
    /*
    drawAtXY(map.skytexgl.tid, mapOfsX/2-map.MapSize*TileSize, mapOfsY/2-map.MapSize*TileSize, map.MapSize*TileSize, map.MapSize*TileSize);
    drawAtXY(map.skytexgl.tid, mapOfsX/2-map.MapSize*TileSize, mapOfsY/2, map.MapSize*TileSize, map.MapSize*TileSize);
    drawAtXY(map.skytexgl.tid, mapOfsX/2, mapOfsY/2-map.MapSize*TileSize, map.MapSize*TileSize, map.MapSize*TileSize);
    drawAtXY(map.skytexgl.tid, mapOfsX/2, mapOfsY/2, map.MapSize*TileSize, map.MapSize*TileSize);
    */
    drawAtXY(map.skytexgl.tid, 0, 0, map.MapSize*TileSize, map.MapSize*TileSize);
    // draw background
    drawAtXY(map.texgl.ptr[map.Back], 0, 0);
    // draw distorted liquid areas
    shadLiquidDistort.exec((Shader shad) {
      shad["iDistortTime"] = iLiquidTime;
      drawAtXY(map.texgl.ptr[map.AllLiquids], 0, 0);
    });
    // monsters, items; we'll do linear interpolation here
    glColor3f(1.0f, 1.0f, 1.0f);
    //glEnable(GL_DEPTH_TEST);
    attachedLightCount = 0;

    // who cares about memory?!
    // draw order: players, items, monsters, other
    static struct DrawInfo {
      ActorDef adef;
      ActorId aid;
      int actorX, actorY;
      @disable this (this); // no copies
    }
    enum { Pixels, Players, Items, Monsters, Other }
    __gshared DrawInfo[65536][4] drawlists;
    __gshared uint[4] dlpos;
    DrawInfo camchickdi;

    dlpos[] = 0;

    Actor.forEach((ActorId me) {
      //me.fprop_0drawlistpos = 0;
      if (auto adef = findActorDef(me)) {
        uint dlnum = Other;
        switch (adef.classtype.get) {
          case "monster": dlnum = (adef.classname.get != "Player" ? Monsters : Players); break;
          case "item": dlnum = Items; break;
          default: dlnum = Other; break;
        }
        if (me.fget_flags&AF_PIXEL) dlnum = Pixels;
        int actorX, actorY; // current actor position
        {
          auto ofs = prevFrameActorOfs.ptr[me.id&0xffff];
          if (frameInterpolation && ofs < uint.max-1 && (me.fget_flags&AF_TELEPORT) == 0 && Actor.isSameSavedActor(me.id, prevFrameActorsData.ptr, ofs)) {
            import core.stdc.math : roundf;
            auto xptr = prevFrameActorsData.ptr+ofs;
            int ox = xptr.fgetp_x;
            int nx = me.fget_x;
            int oy = xptr.fgetp_y;
            int ny = me.fget_y;
            actorX = cast(int)(ox+roundf((nx-ox)*atob));
            actorY = cast(int)(oy+roundf((ny-oy)*atob));
            //conwriteln("actor ", me.id, "; o=(", ox, ",", oy, "); n=(", nx, ",", ny, "); p=(", x, ",", y, ")");
          } else {
            actorX = me.fget_x;
            actorY = me.fget_y;
          }
        }
        if (me.id == cameraChick.id) {
          camchickdi.adef = adef;
          camchickdi.aid = me;
          camchickdi.actorX = actorX;
          camchickdi.actorY = actorY;
        }
        // draw sprite
        if ((me.fget_flags&AF_NODRAW) == 0) {
          //auto dl = &drawlists[dlnum][dlpos.ptr[dlnum]];
          //me.fprop_0drawlistpos = (dlpos.ptr[dlnum]&0xffff)|((dlnum&0xff)<<16);
          auto dl = drawlists.ptr[dlnum].ptr+dlpos.ptr[dlnum];
          ++dlpos.ptr[dlnum];
          dl.adef = adef;
          dl.aid = me;
          dl.actorX = actorX;
          dl.actorY = actorY;
        }
        // process attached lights
        if ((me.fget_flags&AF_NOLIGHT) == 0) {
          uint alr = me.fget_attLightRGBX;
          bool isambient = (me.fget_classtype.get == "light" && me.fget_classname.get == "Ambient");
          if ((alr&0xff) >= 4 || (isambient && me.fget_radius >= 1 && me.fget_height >= 1)) {
            //if (isambient) conwriteln("isambient: ", isambient, "; x=", me.fget_x, "; y=", me.fget_y, "; w=", me.fget_radius, "; h=", me.fget_height);
            // yep, add it
            auto li = attachedLights.ptr+attachedLightCount;
            ++attachedLightCount;
            li.type = (!isambient ? AttachedLightInfo.Type.Point : AttachedLightInfo.Type.Ambient);
            li.x = actorX+me.fget_attLightXOfs;
            li.y = actorY+me.fget_attLightYOfs;
            li.r = ((alr>>24)&0xff)/255.0f; // red or intensity
            if ((alr&0x00_ff_ff_00U) == 0x00_00_01_00U) {
              li.uncolored = true;
            } else {
              li.g = ((alr>>16)&0xff)/255.0f;
              li.b = ((alr>>8)&0xff)/255.0f;
              li.uncolored = false;
            }
            li.radius = (alr&0xff);
            if (li.radius > MaxLightRadius) li.radius = MaxLightRadius;
            if (isambient) {
              li.w = me.fget_radius;
              li.h = me.fget_height;
            }
          }
        }
      } else {
        conwriteln("not found actor ", me.id, " (", me.classtype!string, ":", me.classname!string, ")");
      }
    });

    // draw actor lists
    foreach_reverse (uint dlnum; 0..drawlists.length) {
      if (dlnum == Pixels) continue;
      auto dl = drawlists.ptr[dlnum].ptr;
      if (dlnum == Players) dl += dlpos.ptr[dlnum]-1;
      foreach (uint idx; 0..dlpos.ptr[dlnum]) {
        auto me = dl.aid;
        if (auto isp = dl.adef.animSpr(me.fget_zAnimstate, me.fget_dir, me.fget_zAnimidx)) {
          //drawAtXY(isp.tex, dl.actorX-isp.vga.sx, dl.actorY-isp.vga.sy);
          isp.drawAtXY(dl.actorX, dl.actorY);
        }
        if (dlnum != Players) ++dl; else --dl;
      }
    }
    // draw pixels
    if (dlpos[Pixels]) {
      bindTexture(0);
      bool pointsStarted = false;
      Color lastColor = Color(0, 0, 0, 0);
      auto dl = drawlists.ptr[Pixels].ptr;
      foreach (uint idx; 0..dlpos.ptr[Pixels]) {
        auto me = dl.aid;
        auto s = me.fget_s;
        if (s < 0 || s > 255) continue; //FIXME
        Color clr = d2dpal.ptr[s&0xff];
        if (clr.a == 0) continue;
        if (clr != lastColor) {
          if (pointsStarted) glEnd();
          glColor4f(clr.r/255.0f, clr.g/255.0f, clr.b/255.0f, clr.a/255.0f);
          lastColor = clr;
          pointsStarted = false;
        }
        if (!pointsStarted) {
          glBegin(GL_POINTS);
          pointsStarted = true;
        }
        glVertex2i(dl.actorX, dl.actorY);
        ++dl;
      }
      if (pointsStarted) {
        glEnd();
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      }
    }

    // camera movement
    if (/*altMove || movement ||*/ scale == 1 || !cameraChick.valid) {
      mofsx = 0;
      mofsy = 0;
      vportX0 = 0;
      vportY0 = 0;
      vportX1 = map.width*TileSize;
      vportY1 = map.height*TileSize;
    } else {
      int tiltHeight = /*getMapViewHeight()*/(vlHeight/scale)/4;
      int vy = cameraChick.looky!int;
      if (vy < -tiltHeight) vy = -tiltHeight; else if (vy > tiltHeight) vy = tiltHeight;
      int swdt = vlWidth/scale;
      int shgt = vlHeight/scale;
      int x = camchickdi.actorX-swdt/2;
      int y = (camchickdi.actorY+vy)-shgt/2;
      if (x < 0) x = 0; else if (x >= map.width*TileSize-swdt) x = map.width*TileSize-swdt-1;
      if (y < 0) y = 0; else if (y >= map.height*TileSize-shgt) y = map.height*TileSize-shgt-1;
      mofsx = x*2;
      mofsy = y*2;
      vportX0 = mofsx/scale;
      vportY0 = mofsy/scale;
      vportX1 = vportX0+vlWidth/scale;
      vportY1 = vportY0+vlHeight/scale;
    }

    //glDisable(GL_DEPTH_TEST);
    // draw dots
    dotDraw(atob);
    // do liquid coloring
    drawAtXY(map.texgl.ptr[map.LiquidMask], 0, 0);
    // foreground -- hide secrets, draw lifts and such
    drawAtXY(map.texgl.ptr[map.Front], 0, 0);
    /+
    {
      enum r = 255;
      enum g = 0;
      enum b = 0;
      enum a = 255;
      bindTexture(0);
      glColor4f(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
      glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
      if (cameraChick.valid) {
        glBegin(GL_POINTS);
        glVertex2i(camchickdi.actorX, camchickdi.actorY-70);
        glEnd();
        //glRectf(camchickdi.actorX, camchickdi.actorY-70, camchickdi.actorX+4, camchickdi.actorY-70+4);
      }
      //glRectf(0, 0, 300, 300);
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    }
    +/
  });


  if (doLighting) {
    glDisable(GL_BLEND);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

    // make smaller occluder texture, so we can trace faster
    //assert(fboLMaskSmall.tex.width == map.width);
    //assert(fboLMaskSmall.tex.height == map.height);
    /+!!!
    fboLMaskSmall.exec({
      orthoCamera(map.width, map.height);
      drawAtXY(map.texgl.ptr[map.LightMask].tid, 0, 0, map.width, map.height, mirrorY:true);
    });
    +/

    // clear light layer
    fboLevelLight.exec({
      glClearColor(BackIntens, BackIntens, BackIntens, 1.0f);
      //glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
      ////glColor4f(1.0f, 1.0f, 1.0f, 0.0f);
      glClear(GL_COLOR_BUFFER_BIT);
    });

    // texture 1 is background
    glActiveTexture(GL_TEXTURE0+1);
    glBindTexture(GL_TEXTURE_2D, fboOrigBack.tex.tid);
    // texture 2 is occluders
    glActiveTexture(GL_TEXTURE0+2);
    glBindTexture(GL_TEXTURE_2D, map.texgl.ptr[map.LightMask].tid);
    // texture 3 is small occluder map
    glActiveTexture(GL_TEXTURE0+3);
    glBindTexture(GL_TEXTURE_2D, fboLMaskSmall.tex.tid);
    // done texture assign
    glActiveTexture(GL_TEXTURE0+0);

    /*
    enum LYOfs = 1;

    renderLight( 27, 391-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 100);
    renderLight(542, 424-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 100);
    renderLight(377, 368-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f),  32);
    renderLight(147, 288-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f),  64);
    renderLight( 71, 200-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 128);
    renderLight(249, 200-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 128);
    renderLight(426, 200-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 128);
    renderLight(624, 200-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 128);
    renderLight(549, 298-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f),  64);
    renderLight( 74, 304-0+LYOfs, SVec4F(0.0f, 0.0f, 0.0f, 1.0f),  32);

    renderLight(24*TileSize+4, (24+18)*TileSize-2+LYOfs, SVec4F(0.6f, 0.0f, 0.0f, 1.0f), 128);

    renderLight(280, 330, SVec4F(0.0f, 0.0f, 0.0f, 1.0f), 64);
    */

  foreach (; 0..1) {
    // attached lights
    foreach (ref li; attachedLights[0..attachedLightCount]) {
      if (li.type == AttachedLightInfo.Type.Ambient) {
        //conwriteln("ambient: x=", li.x, "; y=", li.y, "; w=", li.w, "; h=", li.h);
        // ambient light
        if (li.uncolored) {
          renderLightAmbient(li.x, li.y, li.w, li.h, SVec4F(0.0f, 0.0f, 0.0f, li.r));
        } else {
          renderLightAmbient(li.x, li.y, li.w, li.h, SVec4F(li.r, li.g, li.b, 1.0f));
        }
      } else if (li.type == AttachedLightInfo.Type.Point) {
        // point light
        if (li.uncolored) {
          renderLight(li.x, li.y, SVec4F(0.0f, 0.0f, 0.0f, li.r), li.radius);
        } else {
          renderLight(li.x, li.y, SVec4F(li.r, li.g, li.b, 1.0f), li.radius);
        }
      }
    }
  }

    {
      if (testLightMoved) {
        testLightX = testLightX/scale+mofsx/scale;
        testLightY = testLightY/scale+mofsy/scale;
        testLightMoved = false;
      }
      foreach (immutable _; 0..1) {
        renderLight(testLightX, testLightY, SVec4F(0.3f, 0.3f, 0.0f, 1.0f), 96);
      }
    }

    glActiveTexture(GL_TEXTURE0+1);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0+2);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0+3);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0+0);


    // draw scaled level
    /+
    shadScanlines.exec((Shader shad) {
      shad["scanlines"] = scanlines;
      glClearColor(BackIntens, BackIntens, BackIntens, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      orthoCamera(vlWidth, vlHeight);
      //orthoCamera(map.width*TileSize*scale, map.height*TileSize*scale);
      //glMatrixMode(GL_MODELVIEW);
      //glLoadIdentity();
      //glTranslatef(0.375, 0.375, 0); // to be pixel-perfect
      // somehow, FBO objects are mirrored; wtf?!
      drawAtXY(fboLevel.tex.tid, -mapOfsX, -mapOfsY, map.width*TileSize*scale, map.height*TileSize*scale, mirrorY:true);
      //glLoadIdentity();
    });
    +/

    /*
    fboLevelLight.exec({
      smDrawText(map.width*TileSize/2, map.height*TileSize/2, "Testing...");
    });
    */
  }

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

  glDisable(GL_BLEND);

  /*
  fboOrigBack.exec({
    //auto img = smfont.ptr[0x39];
    auto img = fftest;
    if (img !is null) {
      //conwriteln("img.sx=", img.sx, "; img.sy=", img.sy, "; ", img.width, "x", img.height);
      drawAtXY(img.tex, 10-img.sx, 10-img.sy);
    }
  });
  */


  /+
  { // http://stackoverflow.com/questions/7207422/setting-up-opengl-multiple-render-targets
    GLenum[1] buffers = [ GL_BACK_LEFT, /*GL_COLOR_ATTACHMENT0_EXT*/ ];
    //GLenum[1] buffers = [ GL_NONE, /*GL_COLOR_ATTACHMENT0_EXT*/ ];
    glDrawBuffers(1, buffers.ptr);
  }
  +/


  orthoCamera(vlWidth, vlHeight);
  auto tex = (doLighting ? fboLevelLight.tex.tid : fboOrigBack.tex.tid);
  drawAtXY(tex, -mofsx, -mofsy, map.width*TileSize*scale, map.height*TileSize*scale, mirrorY:true);

  {
    glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    drawAtXY(fboLMaskSmall.tex, 0, 0);
    //drawAtXY(map.texgl.ptr[map.LightMask], 0, 0);
  }

  if (levelLoaded) {
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    //orthoCamera(map.width*TileSize, map.height*TileSize);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    hudScripts.runDraw();
  }

  //drawAtXY(map.texgl.ptr[map.LightMask].tid, -mofsx, -mofsy, map.width*TileSize*scale, map.height*TileSize*scale, mirrorY:true);
  //drawAtXY(fboLMaskSmall.tex.tid, 0, 0, map.width*TileSize, map.height*TileSize);

  if (inEditMode) {
    glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    editorUpdateImage();
    fboEditor.tex.setFromImage(editorImg);
    drawAtXY(fboEditor.tex, 0, 0);
    glDisable(GL_BLEND);
  }

  doMessages(curtime);

  if (rConsoleVisible) {
    renderConsoleFBO();
    orthoCamera(vlWidth, vlHeight);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1.0f, 1.0f, 1.0f, 0.7f);
    drawAtXY(fboConsole.tex, 0, rConsoleHeight-vlHeight, mirrorY:true);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// returns time slept
int sleepAtMaxMsecs (int msecs) {
  if (msecs > 0) {
    import core.sys.posix.signal : timespec;
    import core.sys.posix.time : nanosleep;
    timespec ts = void, tpassed = void;
    ts.tv_sec = 0;
    ts.tv_nsec = msecs*1000*1000+(500*1000); // milli to nano
    nanosleep(&ts, &tpassed);
    return (ts.tv_nsec-tpassed.tv_nsec)/(1000*1000);
  } else {
    return 0;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
mixin(import("editor.d"));


// ////////////////////////////////////////////////////////////////////////// //
// rendering thread
enum D2DFrameTime = 55; // milliseconds
enum MinFrameTime = 1000/60; // ~60 FPS

public void renderThread (Tid starterTid) {
  enum BoolOptVarMsgMixin(string varname) =
    "if (msg.toggle) msg.value = !"~varname~";\n"~
    "if ("~varname~" != msg.value) "~varname~" = msg.value; else msg.showMessage = false;\n";

  send(starterTid, 42);
  try {
    MonoTime curtime = MonoTime.currTime;

    lastthink = curtime; // for interpolator
    nextthink = curtime+dur!"msecs"(D2DFrameTime);
    MonoTime nextvframe = curtime;

    enum MaxFPSFrames = 16;
    float frtimes = 0.0f;
    int framenum = 0;
    int prevFPS = -1;
    int hushFrames = 6; // ignore first `hushFrames` frames overtime
    MonoTime prevFrameStartTime = curtime;

    bool vframeWasLost = false;

    void resetFrameTimers () {
      MonoTime curtime = MonoTime.currTime;
      lastthink = curtime; // for interpolator
      nextthink = curtime+dur!"msecs"(D2DFrameTime);
      nextvframe = curtime;
    }

    void loadNewLevel (string name) {
      /*
      if (levelLoaded) {
        conwriteln("ERROR: can't load new levels yet");
        return;
      }
      */
      if (name.length == 0) {
        conwriteln("ERROR: can't load empty level!");
      }
      conwriteln("loading map '", name, "'");
      loadMap(name);
      resetFrameTimers();
    }

    conRegFunc!({
      string mn = genNextMapName(0);
      if (mn.length) {
        nextmapname = null; // clear "exit" flag
        loadNewLevel(mn);
      } else {
        conwriteln("can't skip level");
      }
    })("skiplevel", "skip current level");

    conRegFunc!({
      inEditMode = !inEditMode;
      if (inEditMode) sdwindow.hideCursor(); else sdwindow.showCursor();
    })("ed_toggle", "toggle editor");

    conRegFunc!({
      if (inEditMode) {
        inEditMode = false;
        sdwindow.showCursor();
      }
    })("ed_exit", "exit from editor");

    conRegFunc!((string mapname) {
      nextmapname = null; // clear "exit" flag
      loadNewLevel(mapname);
    })("map", "load map");

    void receiveMessages () {
      for (;;) {
        import core.time : Duration;
        //conwriteln("rendering thread: waiting for messages...");
        auto got = receiveTimeout(
          Duration.zero, // don't wait
          (TMsgMessage msg) {
            addMessage(msg.text[0..msg.textlen], msg.pauseMsecs, msg.noreplace);
          },
          (TMsgTestLightMove msg) {
            testLightX = msg.x;
            testLightY = msg.y;
            testLightMoved = true;
          },
          (TMsgMouseEvent msg) { if (atomicLoad(editMode)) editorMouseEvent(msg); },
          (TMsgKeyEvent msg) { if (atomicLoad(editMode)) editorKeyEvent(msg); },
          (TMsgChar msg) { concliChar(msg.ch); },
          (Variant v) {
            conwriteln("WARNING: unknown thread message received and ignored");
          },
        );
        if (!got) {
          // no more messages
          //conwriteln("rendering thread: no more messages");
          break;
        }
      }
      if (nextmapname.length) {
        string mn = nextmapname;
        nextmapname = null; // clear "exit" flag
        loadNewLevel(mn);
      }
    }

    void processConsoleCommands () {
      consoleLock();
      scope(exit) consoleUnlock();
      concmdDoAll();
    }

    // "D2D frames"; curtime should be set; return `true` if frame was processed; will fix `curtime`
    bool doThinkFrame () {
      if (curtime >= nextthink) {
        lastthink = curtime;
        while (nextthink <= curtime) nextthink += dur!"msecs"(D2DFrameTime);
        if (levelLoaded) {
          // save snapshot and other data for interpolator
          Actor.saveSnapshot(prevFrameActorsData[], prevFrameActorOfs.ptr);
          if (!gamePaused && !inEditMode) {
            // process actors
            doActorsThink();
            dotThink();
          }
        }
        // some timing
        auto tm = MonoTime.currTime;
        int thinkTime = cast(int)((tm-curtime).total!"msecs");
        if (thinkTime > 9) { import core.stdc.stdio; printf("spent on thinking: %d msecs\n", thinkTime); }
        curtime = tm;
        return true;
      } else {
        return false;
      }
    }

    // "video frames"; curtime should be set; return `true` if frame was processed; will fix `curtime`
    bool doVFrame () {
      bool doCheckTime = void;
      if (!renderVBL) {
        // timer
        doCheckTime = true;
      } else {
        // vsync
        __gshared bool prevLost = false;
        doCheckTime = vframeWasLost;
        if (vframeWasLost) {
          if (!prevLost) {
            { import core.stdc.stdio; printf("frame was lost!\n"); }
          }
          prevLost = true;
        } else {
          prevLost = false;
        }
      }
      if (doCheckTime) {
        if (curtime < nextvframe) return false;
        if (!renderVBL) {
          if (curtime > nextvframe) {
            auto overtime = cast(int)((curtime-nextvframe).total!"msecs");
            if (overtime > 2500) {
              if (hushFrames) {
                --hushFrames;
              } else {
                { import core.stdc.stdio; printf("  spent whole %d msecs\n", overtime); }
              }
            }
          }
        }
      }
      while (nextvframe <= curtime) nextvframe += dur!"msecs"(MinFrameTime);
      bool ctset = false;
      {
        sdwindow.mtLock();
        scope(exit) sdwindow.mtUnlock();
        ctset = sdwindow.setAsCurrentOpenGlContextNT;
      }
      // if we can't set context, pretend that videoframe was processed; this should solve problem with vsync and invisible window
      if (ctset) {
        if (oldRenderVBL != renderVBL) {
          oldRenderVBL = renderVBL;
          sdwindow.vsync = renderVBL;
        }
        // render scene
        iLiquidTime = cast(float)((curtime-MonoTime.zero).total!"msecs"%10000000)/18.0f*0.04f;
        receiveMessages(); // here, 'cause we need active OpenGL context for some messages
        processConsoleCommands();
        if (levelLoaded) {
          renderScene(curtime);
        } else {
          //renderLoading(curtime);
        }
        sdwindow.mtLock();
        scope(exit) sdwindow.mtUnlock();
        sdwindow.swapOpenGlBuffers();
        glFinish();
        sdwindow.releaseCurrentOpenGlContext();
        vframeWasLost = false;
      } else {
        vframeWasLost = true;
        { import core.stdc.stdio; printf("xframe was lost!\n"); }
      }
      curtime = MonoTime.currTime;
      return true;
    }

    while (!quitRequested && !sdwindow.closed) {
      curtime = MonoTime.currTime;
      auto fstime = curtime;

      doThinkFrame(); // this will fix curtime if necessary
      if (doVFrame()) {
        if (!vframeWasLost) {
          // fps
          auto frameTime = cast(float)(curtime-prevFrameStartTime).total!"msecs"/1000.0f;
          prevFrameStartTime = curtime;
          frtimes += frameTime;
          if (++framenum >= MaxFPSFrames || frtimes >= 3.0f) {
            import std.string : format;
            int newFPS = cast(int)(cast(float)MaxFPSFrames/frtimes+0.5);
            if (newFPS != prevFPS) {
              sdwindow.title = "%s / FPS:%s".format("D2D", newFPS);
              prevFPS = newFPS;
            }
            framenum = 0;
            frtimes = 0.0f;
          }
        }
      }

      curtime = MonoTime.currTime;

      // now sleep until next "video" or "think" frame
      if (nextthink > curtime && nextvframe > curtime) {
        if (sdwindow.closed || quitRequested) break;
        // let's decide
        immutable nextVideoFrameSleep = cast(int)((nextvframe-curtime).total!"msecs");
        immutable nextThinkFrameSleep = cast(int)((nextthink-curtime).total!"msecs");
        immutable sleepTime = (nextVideoFrameSleep < nextThinkFrameSleep ? nextVideoFrameSleep : nextThinkFrameSleep);
        sleepAtMaxMsecs(sleepTime);
        //curtime = MonoTime.currTime;
      }
    }
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    import core.memory : GC;
    GC.disable(); // yeah
    thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    abort(); // die, you bitch!
  }
  atomicStore(vquitRequested, true);
  sdwindow.close();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared Tid renderTid;
shared bool renderThreadStarted = false;


public void startRenderThread () {
  if (!cas(&renderThreadStarted, false, true)) {
    assert(0, "render thread already started!");
  }
  renderTid = spawn(&renderThread, thisTid);
  setMaxMailboxSize(renderTid, 1024, OnCrowding.throwException); //FIXME
  // wait for "i'm ready" signal
  receive(
    (int ok) {
      if (ok != 42) assert(0, "wtf?!");
    },
  );
  conwriteln("rendering thread started");
}


// ////////////////////////////////////////////////////////////////////////// //
// thread messages


// ////////////////////////////////////////////////////////////////////////// //
struct TMsgMouseEvent {
  private import arsd.simpledisplay : MouseEventType, MouseButton;
  MouseEventType type;
  int x, y;
  int dx, dy;
  MouseButton button; /// See $(LREF MouseButton)
  int modifierState; /// See $(LREF ModifierState)
}

public void postMouseEvent() (in auto ref MouseEvent evt) {
  if (!atomicLoad(renderThreadStarted)) return;
  TMsgMouseEvent msg;
  msg.type = evt.type;
  msg.x = evt.x;
  msg.y = evt.y;
  msg.dx = evt.dx;
  msg.dy = evt.dy;
  msg.button = evt.button;
  msg.modifierState = evt.modifierState;
  send(renderTid, msg);
}


// ////////////////////////////////////////////////////////////////////////// //
struct TMsgKeyEvent {
  private import arsd.simpledisplay : Key;
  Key key;
  uint hardwareCode;
  bool pressed;
  dchar character;
  uint modifierState;
}

public void postKeyEvent() (in auto ref KeyEvent evt) {
  if (!atomicLoad(renderThreadStarted)) return;
  TMsgKeyEvent msg;
  msg.key = evt.key;
  msg.pressed = evt.pressed;
  msg.character = evt.character;
  msg.modifierState = evt.modifierState;
  send(renderTid, msg);
}


// ////////////////////////////////////////////////////////////////////////// //
struct TMsgTestLightMove {
  int x, y;
}

public void postTestLightMove (int x, int y) {
  if (!atomicLoad(renderThreadStarted)) return;
  auto msg = TMsgTestLightMove(x, y);
  send(renderTid, msg);
}


// ////////////////////////////////////////////////////////////////////////// //
struct TMsgMessage {
  char[256] text;
  uint textlen;
  int pauseMsecs;
  bool noreplace;
}

public void postAddMessage (const(char)[] msgtext, int pauseMsecs=3000, bool noreplace=false) {
  if (!atomicLoad(renderThreadStarted)) return;
  if (msgtext.length > TMsgMessage.text.length) msgtext = msgtext[0..TMsgMessage.text.length];
  TMsgMessage msg;
  msg.textlen = cast(uint)msgtext.length;
  if (msg.textlen) msg.text[0..msg.textlen] = msgtext[0..msg.textlen];
  msg.pauseMsecs = pauseMsecs;
  msg.noreplace = noreplace;
  send(renderTid, msg);
}


// ////////////////////////////////////////////////////////////////////////// //
struct TMsgChar {
  char ch;
}

public void postChar (char ch) {
  if (!atomicLoad(renderThreadStarted)) return;
  TMsgChar msg;
  msg.ch = ch;
  send(renderTid, msg);
}


// ////////////////////////////////////////////////////////////////////////// //
// add console command to execution queue
public void concmd (const(char)[] cmd) {
  //if (!atomicLoad(renderThreadStarted)) return;
  consoleLock();
  scope(exit) consoleUnlock();
  concmdAdd(cmd);
}

// get console variable value; doesn't do complex conversions!
public T convar(T) (const(char)[] s) {
  consoleLock();
  scope(exit) consoleUnlock();
  return conGetVar!T(s);
}

// set console variable value; doesn't do complex conversions!
public void convar(T) (const(char)[] s, T val) {
  consoleLock();
  scope(exit) consoleUnlock();
  conSetVar!T(s, val);
}
