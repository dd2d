/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xmain_d2d is aliced;

private:
import core.atomic;
import core.thread;
import core.time;

import std.concurrency;

import arsd.simpledisplay;

import iv.glbinds : GL_NUM_SHADING_LANGUAGE_VERSIONS; // please rdmd

import iv.cmdcon : ConInputChar;
import glutils;
import wadarc;

import iv.vfs;

import d2dmap;
import d2dadefs;
import d2dimage;
import d2dfont;
import dacs;

import d2dunigrid;

// `map` is there
import dengapi;

import d2dparts;

import render;


// ////////////////////////////////////////////////////////////////////////// //
import arsd.color;
import arsd.png;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  FuncPool.dumpCode = false;
  FuncPool.dumpCodeSize = false;
  dacsDumpSemantic = false;
  dacsOptimize = 9;
  //version(rdmd) { dacsOptimize = 0; }
  bool compileOnly = false;
  bool doVBL = true;

  for (usize idx = 1; idx < args.length; ++idx) {
    bool remove = true;
    if (args[idx].length < 1) continue;
         if (args[idx] == "--dump-code") FuncPool.dumpCode = true;
    else if (args[idx] == "--dump-code-size") FuncPool.dumpCodeSize = true;
    else if (args[idx] == "--dump-semantic") dacsDumpSemantic = true;
    else if (args[idx] == "--dump-all") { FuncPool.dumpCode = true; FuncPool.dumpCodeSize = true; dacsDumpSemantic = true; }
    else if (args[idx] == "--compile") compileOnly = true;
    else if (args[idx] == "--compile-only") compileOnly = true;
    else if (args[idx] == "--messages") ++dacsMessages;
    else if (args[idx] == "--no-copro") dacsOptimizeNoCoPro = true;
    else if (args[idx] == "--no-deadass") dacsOptimizeNoDeadAss = true;
    else if (args[idx] == "--no-purekill") dacsOptimizeNoPureKill = true;
    else if (args[idx] == "--no-vsync") doVBL = false;
    else if (args[idx] == "--vsync") doVBL = true;
    else if (args[idx].length > 2 && args[idx][0..2] == "-O") {
      import std.conv : to;
      ubyte olevel = to!ubyte(args[idx][2..$]);
      dacsOptimize = olevel;
    } else if (args[idx][0] == '-') {
      assert(0, "wut?!");
    }
    else remove = false;
    if (remove) {
      foreach (immutable c; idx+1..args.length) args.ptr[c-1] = args.ptr[c];
      args.length -= 1;
      --idx; //hack
    }
  }
  if (processCL(args)) return;

  addInternalActorFields();

  static void setDP () {
    version(rdmd) {
      setDataPath("data");
    } else {
      import std.file : thisExePath;
      import std.path : dirName;
      setDataPath(thisExePath.dirName~"/data");
    }
    addPak(getDataPath~"base.pk3"); registerWadScripts();
    //addWad("/home/ketmar/k8prj/doom2d-tl/data/doom2d.wad"); registerWadScripts();
    //addWad("/home/ketmar/k8prj/doom2d-tl/data/meat.wad"); registerWadScripts();
    //addWad("/home/ketmar/k8prj/doom2d-tl/data/megadm.wad"); registerWadScripts();
    //addWad("/home/ketmar/k8prj/doom2d-tl/data/megadm1.wad"); registerWadScripts();
    //addWad("/home/ketmar/k8prj/doom2d-tl/data/superdm.wad"); registerWadScripts();
    //addWad("/home/ketmar/k8prj/doom2d-tl/data/zadoomka.wad"); registerWadScripts();
    loadWadScripts();
  }

  setDP();

  if (compileOnly) return;

  try {
    registerAPI();
    loadD2DPalette();

    setOpenGLContextVersion(3, 2); // up to GLSL 150
    //openGLContextCompatible = false;

    curmapname = "maps/map01.d2m";
    //conwriteln(genNextMapName());

    sdwindow = new SimpleWindow(vlWidth, vlHeight, "D2D", OpenGlOptions.yes, Resizability.fixedSize);
    //sdwindow.hideCursor();

    sdwindow.closeQuery = delegate () { concmd("quit"); };

    sdwindow.visibleForTheFirstTime = delegate () {
      import iv.glbinds;
      sdwindow.setAsCurrentOpenGlContext(); // make this window active
      /+
      {
        import core.stdc.stdio;
        printf("GL version: %s\n", glGetString(GL_VERSION));
        GLint l, h;
        glGetIntegerv(GL_MAJOR_VERSION, &h);
        glGetIntegerv(GL_MINOR_VERSION, &l);
        printf("version: %d.%d\n", h, l);
        printf("shader version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        GLint svcount;
        glGetIntegerv(GL_NUM_SHADING_LANGUAGE_VERSIONS, &svcount);
        if (svcount > 0) {
          printf("%d shader versions supported:\n", svcount);
          foreach (GLuint n; 0..svcount) printf("  %d: %s\n", n, glGetStringi(GL_SHADING_LANGUAGE_VERSION, n));
        }
        /*
        GLint ecount;
        glGetIntegerv(GL_NUM_EXTENSIONS, &ecount);
        if (ecount > 0) {
          printf("%d extensions supported:\n", ecount);
          foreach (GLuint n; 0..ecount) printf("  %d: %s\n", n, glGetStringi(GL_EXTENSIONS, n));
        }
        */
      }
      +/
      // check if we have sufficient shader version here
      {
        bool found = false;
        GLint svcount;
        glGetIntegerv(GL_NUM_SHADING_LANGUAGE_VERSIONS, &svcount);
        if (svcount > 0) {
          foreach (GLuint n; 0..svcount) {
            import core.stdc.string : strncmp;
            auto v = glGetStringi(GL_SHADING_LANGUAGE_VERSION, n);
            if (v is null) continue;
            if (strncmp(v, "130", 3) != 0) continue;
            if (v[3] > ' ') continue;
            found = true;
            break;
          }
        }
        if (!found) assert(0, "can't find OpenGL GLSL 120");
        {
          auto adr = glGetProcAddress("glTexParameterf");
          if (adr is null) assert(0);
        }
      }
      sdwindow.vsync = false;
      sdwindow.useGLFinish = false;
      initOpenGL();
      if (!sdwindow.releaseCurrentOpenGlContext()) { import core.stdc.stdio; printf("can't release OpenGL context(1)\n"); }
      startRenderThread();
      convar("r_vsync", doVBL);
      concmd("exec config.rc");
      concmd("exec autoexec.rc");
      concmd("map '"~curmapname~"'");
      //convar("r_console", true);
    };

    enum { Left, Right, Up, Down }
    bool[4] pressed = false;
    bool testLightLocked = false;
    bool justConsoled = false;

    sdwindow.eventLoop(5000,
      delegate () {
        if (sdwindow.closed) return;
      },
      delegate (KeyEvent event) {
        if (sdwindow.closed) return;
        if (event.pressed) justConsoled = false;
        if (!conVisible && inEditMode) { postKeyEvent(event); return; }
        if (event.pressed && event.key == Key.Escape) {
          //if (convar!bool("r_console")) conwriteln("CONSOLE IS HERE"); else conwriteln("NO CONSOLE");
          if (conVisible) concmd("r_console toggle"); else concmd("quit");
          return;
        }
        if (!conVisible) {
          switch (event.key) {
            case Key.Left: case Key.Pad4: plrKeyUpDown(0, PLK_LEFT, event.pressed); break;
            case Key.Right: case Key.Pad6: plrKeyUpDown(0, PLK_RIGHT, event.pressed); break;
            case Key.Up: case Key.Pad8: plrKeyUpDown(0, PLK_UP, event.pressed); break;
            case Key.Down: case Key.Pad2: plrKeyUpDown(0, PLK_DOWN, event.pressed); break;
            case Key.Alt: plrKeyUpDown(0, PLK_JUMP, event.pressed); break;
            case Key.Ctrl: plrKeyUpDown(0, PLK_FIRE, event.pressed); break;
            case Key.Shift: plrKeyUpDown(0, PLK_USE, event.pressed); break;
            case Key.Grave: if (event.pressed) { justConsoled = !conVisible; concmd("r_console toggle"); } break;
            default:
          }
        } else {
          plrKeyUpDown(0, PLK_LEFT, false);
          plrKeyUpDown(0, PLK_RIGHT, false);
          plrKeyUpDown(0, PLK_UP, false);
          plrKeyUpDown(0, PLK_DOWN, false);
          plrKeyUpDown(0, PLK_JUMP, false);
          plrKeyUpDown(0, PLK_FIRE, false);
          plrKeyUpDown(0, PLK_USE, false);
               if (event.pressed && event.key == Key.Up) postChar(ConInputChar.Up);
          else if (event.pressed && event.key == Key.Down) postChar(ConInputChar.Down);
          else if (event.pressed && event.key == Key.PageUp) postChar(ConInputChar.PageUp);
          else if (event.pressed && event.key == Key.PageDown) postChar(ConInputChar.PageDown);
          else postKeyEvent(event);
        }
      },
      delegate (MouseEvent event) {
        if (inEditMode) { postMouseEvent(event); return; }
        if (!testLightLocked) postTestLightMove(event.x, event.y);
      },
      delegate (dchar ch) {
        if (conVisible) {
          if (justConsoled && ch == '`') ch = 0;
          justConsoled = false;
          if (ch && ch < 128) postChar(cast(char)ch);
          return;
        }
        if (inEditMode) return;
        if (ch == 'q') concmd("quit");
        if (ch == '1') concmd("r_scale 1");
        if (ch == '2') concmd("r_scale 2");
        if (ch == 'D') concmd("nodoorclip");
        if (ch == 'i') concmd("r_interpolation toggle; hudmsg \"Interpolation: $r_interpolation\"");
        if (ch == 'l') concmd("r_lighting toggle");
        if (ch == 'W') concmd("nowallclip");
        if (ch == 'L') testLightLocked = !testLightLocked;
        if (ch == 'p') concmd("g_pause toggle");
        if (ch == '!') concmd("skiplevel");
        if (ch == 'e') concmd("ed_toggle");
      },
    );
  } catch (Exception e) {
    import std.stdio : stderr;
    stderr.writeln("FUUUUUUUUUUUU\n", e.toString);
  }
  flushGui();
}
