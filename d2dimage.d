/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module d2dimage is aliced;
private:

import arsd.color;
import arsd.png;
import iv.vfs;
import iv.cmdcon;
import iv.glbinds;
import iv.jpegd;

import glutils;
import wadarc;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared Color[256] d2dpal;


public void loadD2DPalette () {
  ubyte[768] vgapal;
  {
    auto fl = VFile("playpal.pal");
    fl.rawReadExact(vgapal[]);
    foreach (ref ubyte b; vgapal) if (b > 63) b = 63; // just in case
  }
  foreach (immutable idx; 0..256) {
    d2dpal[idx].r = cast(ubyte)(vgapal[idx*3+0]*255/63);
    d2dpal[idx].g = cast(ubyte)(vgapal[idx*3+1]*255/63);
    d2dpal[idx].b = cast(ubyte)(vgapal[idx*3+2]*255/63);
    d2dpal[idx].a = 255;
  }
  // color 0 is transparent
  d2dpal[0].asUint = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
public final class D2DImage {
public:
  int sx, sy;
  int mwidth, mheight;
  private TrueColorImage mimg;
  private Texture mtex;

  private this () pure nothrow @safe {}

  this (string name) {
    import std.path : extension, setExtension;
    if (name.extension == ".vga") {
      static immutable string[4] extList = [".vga", ".png", ".jpg", ".jpeg"];
      foreach (string ext; extList) {
        try {
          auto nn = name.setExtension(ext);
          auto fl = VFile(nn);
          conwriteln("loading image '", nn, "'");
          load(fl);
          return;
        } catch (Exception e) {
          //conwriteln("ERROR: ", e.msg);
        }
      }
    }
    auto fl = VFile(name);
    conwriteln("loading image '", name, "'");
    load(fl);
  }

  this (int awdt, int ahgt) nothrow @trusted {
    assert(awdt >= 0 && ahgt >= 0);
    sx = sy = 0;
    mwidth = awdt;
    mheight = ahgt;
    if (awdt > 0 && ahgt > 0) {
      try {
        mimg = new TrueColorImage(awdt, ahgt);
      } catch (Exception e) {
        assert(0, e.toString);
      }
      mimg.imageData.bytes[] = 0;
    }
  }

  /// will not clone texture!
  D2DImage clone () const nothrow @trusted {
    auto res = new D2DImage();
    res.sx = sx;
    res.sy = sy;
    res.mwidth = mwidth;
    res.mheight = mheight;
    if (valid) {
      try {
        res.mimg = new TrueColorImage(mwidth, mheight);
      } catch (Exception e) {
        assert(0, e.toString);
      }
      res.mimg.imageData.bytes[] = mimg.imageData.bytes[];
    }
    return res;
  }

  void resize (int awdt, int ahgt) nothrow @trusted {
    assert(awdt >= 0 && ahgt >= 0);
    if (mwidth != awdt || mheight != ahgt) {
      mtex = null;
      mwidth = awdt;
      mheight = ahgt;
      sx = sy = 0;
      if (awdt > 0 && ahgt > 0) {
        try {
          mimg = new TrueColorImage(awdt, ahgt);
        } catch (Exception e) {
          assert(0, e.toString);
        }
        mimg.imageData.bytes[] = 0;
      }
    }
    if (mwidth <= 0 || mheight <= 0) mimg = null;
  }

  void clear () {
    //if (mtex !is null) mtex.clear;
    //if (mimg !is null) mimg.clear;
    mtex = null;
    mimg = null;
    mwidth = mheight = 0;
    sx = sy = 0;
  }

  void removeOffset () { sx = sy = 0; }

  @property const pure nothrow @safe @nogc {
    bool valid () { pragma(inline, true); return (mimg !is null && mwidth > 0 && mheight > 0); }
    int width () { pragma(inline, true); return mwidth; }
    int height () { pragma(inline, true); return mheight; }
  }
  // DO NOT RESIZE!
  @property TrueColorImage img () pure nothrow @safe @nogc { pragma(inline, true); return mimg; }

  Color opIndex (usize y, usize x) const /*pure*/ nothrow @safe @nogc { pragma(inline, true); return getPixel(x, y); }
  void opIndex (Color clr, usize y, usize x) nothrow @safe @nogc { pragma(inline, true); setPixel(x, y, clr); }
  void opIndex (Color clr, usize y, usize x, bool ignoreTransparent) nothrow @safe @nogc { pragma(inline, true); if (!ignoreTransparent || clr.a != 0) setPixel(x, y, clr); }

  Color getPixel (int x, int y) const /*pure*/ nothrow @trusted @nogc {
    pragma(inline, true);
    return (mimg !is null && x >= 0 && y >= 0 && x < mwidth && y < mheight ? (cast(const(Color*))mimg.imageData.bytes.ptr)[y*mwidth+x] : Color(0, 0, 0, 0));
  }

  void setPixel (int x, int y, Color clr) nothrow @trusted @nogc {
    pragma(inline, true);
    if (mimg !is null && x >= 0 && y >= 0 && x < mwidth && y < mheight) (cast(Color*)mimg.imageData.bytes.ptr)[y*mwidth+x] = clr;
  }

  void putPixel (int x, int y, Color clr) nothrow @trusted @nogc {
    pragma(inline, true);
    if (mimg !is null && x >= 0 && y >= 0 && x < mwidth && y < mheight && clr.a != 0) (cast(Color*)mimg.imageData.bytes.ptr)[y*mwidth+x] = clr;
  }

  void toBlackAndWhite () {
    if (!valid) return;
    foreach (int y; 0..mheight) {
      foreach (int x; 0..mwidth) {
        Color clr = getPixel(x, y);
        int i = cast(int)(0.2126*clr.r+0.7152*clr.g+0.0722*clr.b);
        if (i > 255) i = 255; // just in case
        setPixel(x, y, Color(i&0xff, i&0xff, i&0xff, clr.a));
      }
    }
  }

  void toBlackAndWhiteChan(string chan) () {
    static assert(chan == "red" || chan == "r" || chan == "green" || chan == "g" || chan == "blue" || chan == "b", "invalid channel");
    if (!valid) return;
    foreach (int y; 0..mheight) {
      foreach (int x; 0..mwidth) {
        Color clr = getPixel(x, y);
             static if (chan == "red" || chan == "r") ubyte i = clr.r;
        else static if (chan == "green" || chan == "g") ubyte i = clr.g;
        else static if (chan == "blue" || chan == "b") ubyte i = clr.b;
        else static assert(0, "wtf?!");
        setPixel(x, y, Color(i, i, i, clr.a));
      }
    }
  }

  D2DImage blackAndWhite () {
    auto res = this.clone();
    res.toBlackAndWhite;
    return res;
  }

  D2DImage blackAndWhiteChan(string chan) () {
    auto res = this.clone();
    res.toBlackAndWhiteChan!chan;
    return res;
  }

  /// mirror image horizontally
  void mirror () {
    if (!valid) return;
    foreach (int y; 0..height) {
      foreach (int x; 0..width/2) {
        int mx = width-x-1;
        auto c0 = getPixel(x, y);
        auto c1 = getPixel(mx, y);
        setPixel(x, y, c1);
        setPixel(mx, y, c0);
      }
    }
    sx = width-sx-1;
  }

  void createTex () {
    if (mtex is null && valid) mtex = new Texture(mimg, Texture.Option.Nearest);
  }

  void updateTex () {
    if (valid) {
      if (mtex is null) mtex = new Texture(mimg, Texture.Option.Nearest); else mtex.setFromImage(mimg);
    }
  }

  GLuint asTex () {
    if (!valid) return 0;
    if (mtex is null) createTex();
    return (mtex !is null ? mtex.id : 0);
  }

  void savePng (string fname) {
    if (!valid) throw new Exception("can't save empty image");
    auto png = pngFromImage(mimg);

    /*
    void insertChunk (Chunk *chk) {
      foreach (immutable idx; 0..png.chunks.length) {
        if (cast(string)png.chunks[idx].type == "IDAT") {
          // ok, insert before IDAT
          png.chunks.length += 1;
          foreach_reverse (immutable c; idx+1..png.chunks.length) png.chunks[c] = png.chunks[c-1];
          png.chunks[idx] = *chk;
          return;
        }
      }
      png.chunks ~= *chk;
    }
    */

    if (sx != 0 || sy != 0) {
      {
        D2DInfoChunk di;
        di.ver = 0;
        di.sx = sx;
        di.sy = sy;
        di.fixEndian;
        auto chk = Chunk.create("dtDi", (cast(ubyte*)&di)[0..di.sizeof]);
        //png.chunks ~= *chk;
        png.insertChunk(chk);
      }
      // zdoom chunk
      if (sx >= short.min && sx <= short.max && sy >= short.min && sy <= short.max) {
        GrabChunk di;
        di.sx = cast(short)sx;
        di.sy = cast(short)sy;
        di.fixEndian;
        auto chk = Chunk.create("grAb", (cast(ubyte*)&di)[0..di.sizeof]);
        //png.chunks ~= *chk;
        png.insertChunk(chk);
      }
    }
    auto fo = vfsDiskOpen(fname, "w");
    fo.rawWriteExact(writePng(png));
  }

private:
  static align(1) struct D2DInfoChunk {
  align(1):
    ubyte ver; // version; 0 for now; versions should be compatible
    int sx, sy;

    void fixEndian () nothrow @trusted @nogc {
      version(BigEndian) {
        import std.bitmanip : swapEndian;
        sx = swapEndian(sx);
        sy = swapEndian(sy);
      }
    }
  }

  static align(1) struct GrabChunk {
  align(1):
    short sx, sy;

    void fixEndian () nothrow @trusted @nogc {
      version(LittleEndian) {
        import std.bitmanip : swapEndian;
        sx = swapEndian(sx);
        sy = swapEndian(sy);
      }
    }
  }

  void loadPng (VFile fl) {
    auto flsize = fl.size-fl.tell;
    if (flsize < 8 || flsize > 1024*1024*32) throw new Exception("png image too big");
    auto data = new ubyte[](cast(uint)flsize);
    fl.rawReadExact(data);
    auto png = readPng(data);
    auto ximg = imageFromPng(png).getAsTrueColorImage;
    if (ximg is null) throw new Exception("png: wtf?!");
    if (ximg.width < 1 || ximg.height < 1) throw new Exception("png image too small");
    mwidth = ximg.width;
    mheight = ximg.height;
    sx = sy = 0;
    mimg = ximg;
    foreach (ref chk; png.chunks) {
      if (chk.type[] == "dtDi") {
        // d2d info chunk
        if (chk.size >= D2DInfoChunk.sizeof) {
          auto di = *cast(D2DInfoChunk*)chk.payload.ptr;
          di.fixEndian;
          sx = di.sx;
          sy = di.sy;
          version(png_dump_chunks) conwriteln("found 'dtDi' chunk! sx=", di.sx, "; sy=", di.sy);
        }
      } else if (chk.type[] == "grAb") {
        // zdoom info chunk
        if (chk.size >= GrabChunk.sizeof) {
          auto di = *cast(GrabChunk*)chk.payload.ptr;
          di.fixEndian;
          sx = di.sx;
          sy = di.sy;
          version(png_dump_chunks) conwriteln("found 'grAb' chunk! sx=", di.sx, "; sy=", di.sy);
        }
      }
    }
  }

  void loadVga (VFile fl) {
    //conwriteln(" loading .VGA image");
    auto w = fl.readNum!ushort();
    auto h = fl.readNum!ushort();
    auto isx = fl.readNum!short();
    auto isy = fl.readNum!short();
    //conwriteln(" loading .VGA image; w=", w, "; h=", h, "; isx=", isx, "; isy=", isy);
    if (w < 1 || w > 32760) throw new Exception("invalid vga image width");
    if (h < 1 || h > 32760) throw new Exception("invalid vga image height");
    auto data = new ubyte[](w*h);
    fl.rawReadExact(data[]);
    resize(w, h);
    assert(mimg !is null);
    assert(mimg.width == w);
    assert(mimg.height == h);
    assert(mwidth == w);
    assert(mheight == h);
    //conwriteln(" !!!");
    sx = isx;
    sy = isy;
    foreach (int y; 0..height) {
      foreach (int x; 0..width) {
        setPixel(x, y, d2dpal.ptr[data.ptr[y*w+x]]);
      }
    }
  }

  void loadJpeg (VFile fl) {
    auto jpg = readJpeg(fl);
    if (jpg.width < 1 || jpg.width > 32760) throw new Exception("invalid image width");
    if (jpg.height < 1 || jpg.height > 32760) throw new Exception("invalid image height");
    mtex = null;
    mwidth = jpg.width;
    mheight = jpg.height;
    sx = sy = 0;
    mimg = jpg.getAsTrueColorImage;
  }

  void load (VFile fl) {
    scope(failure) fl.seek(0);
    char[8] sign;
    fl.seek(0);
    fl.rawReadExact(sign[]);
    // png?
    if (sign == "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A") {
      fl.seek(0);
      loadPng(fl);
      return;
    }
    // jpeg?
    int width, height, actual_comps;
    if (sign[0..2] == "\xff\xd8" && detectJpeg(fl, width, height, actual_comps)) {
      /*
      fl.seek(-2, Seek.End);
      fl.rawReadExact(sign[0..2]);
      if (sign[0..2] == "\xff\xd9") {
        fl.seek(0);
        loadJpeg(fl);
        return;
      }
      */
      fl.seek(0);
      loadJpeg(fl);
      return;
    }
    // alas, this must be vga
    fl.seek(0);
    loadVga(fl);
  }
}
