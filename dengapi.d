/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module dengapi is aliced;

private:
import core.atomic;
import core.thread;
import core.time;

import iv.cmdcon;
import iv.glbinds;
import glutils;
import wadarc;

import d2dmap;
import d2dadefs;
import d2dimage;
import d2dsprite;
import dacs;

import d2dunigrid;
import render;


// ////////////////////////////////////////////////////////////////////////// //
private extern (C) void _d_print_throwable (Throwable t);


// ////////////////////////////////////////////////////////////////////////// //
import arsd.color;
import arsd.png;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared ActorId cameraChick;


// ////////////////////////////////////////////////////////////////////////// //
public void addInternalActorFields () {
  // &0xffff: position in drawlist
  // (>>16)&0xff: drawlist index
  //Actor.addField("0drawlistpos", Actor.Field.Type.Uint);
}


// ////////////////////////////////////////////////////////////////////////// //
public {
  enum PLK_UP    = (1<<0);
  enum PLK_DOWN  = (1<<1);
  enum PLK_LEFT  = (1<<2);
  enum PLK_RIGHT = (1<<3);
  enum PLK_FIRE  = (1<<4);
  enum PLK_JUMP  = (1<<5);
  enum PLK_USE   = (1<<6);
}

__gshared uint plrKeysLast;
__gshared uint plrKeyState;


public void plrKeyDown (uint plidx, uint mask) {
  if (mask == 0 || plidx > 0) return;
  plrKeysLast |= mask;
  plrKeyState |= mask;
}


public void plrKeyUp (uint plidx, uint mask) {
  if (mask == 0 || plidx > 0) return;
  plrKeyState &= ~mask;
}


public void plrKeyUpDown (uint plidx, uint mask, bool down) {
  if (down) plrKeyDown(plidx, mask); else plrKeyUp(plidx, mask);
}


void plrKeysFix (uint plidx) {
  if (plidx > 0) return;
  //conwritefln!"plrKeyState=0x%02x; plrKeysLast=0x%02x"(plrKeyState, plrKeysLast);
  foreach (uint n; 0..12) {
    if ((plrKeyState&(1<<n)) == 0) plrKeysLast &= ~(1<<n);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void updateActorClasses () {
  foreach (string ctype; dacsClassTypes) {
    foreach (auto mod; dacsClassModules(ctype)) {
      if (mod.rmoduletype.classtype == "map") {
        // map scripts, not here
      } else {
        auto adef = registerActorDef(mod.rmoduletype.classtype, mod.rmoduletype.classname);
        conwriteln("found info for actor '", mod.rmoduletype.classtype, ":", mod.rmoduletype.classname, "'");
        {
          auto animInitFn = FuncPool.findByFQMG(mod.name~".initializeAnim", ":void");
          if (animInitFn !is null) adef.setAnimInitFunc(animInitFn);
        }
        {
          auto initFn = FuncPool.findByFQMG(mod.name~".initialize", ":void:Actor");
          if (initFn !is null) adef.setInitFunc(initFn);
        }
        {
          auto thinkFn = FuncPool.findByFQMG(mod.name~".think", ":void:Actor");
          if (thinkFn !is null) adef.setThinkFunc(thinkFn);
        }
      }
    }
  }

  // setup HUD scripts
  if (auto mod = dacsModuleFor("engine", "hud")) {
    hudScripts.fiDraw = FuncPool.findByFQMG(mod.name~".drawHud", ":void");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private string modLoader (string modname) {
  static string getTextFile (string fname) {
    try {
      auto res = loadTextFile(fname);
      conwriteln("loading DACS module file '", fname, "' (", res.length, " bytes)");
      if (res is null) res = "";
      return res;
    } catch (Exception) {}
    return null;
  }

  string res;

  //res = getTextFile(modname~".dacs");
  //if (res !is null) return res;

  res = getTextFile("scripts/"~modname~".dacs");
  if (res !is null) return res;
  assert(res is null);

  string[] parts = ["scripts"];
  string mn = modname;
  while (mn.length > 0) {
    int pos = 0;
    if (mn[0] >= 'A' && mn[0] <= 'Z') ++pos;
    while (pos < mn.length && (mn[pos] < 'A' || mn[pos] > 'Z')) ++pos;
    if (mn[0] >= 'A' && mn[0] <= 'Z') {
      parts ~= cast(char)(mn[0]+32)~mn[1..pos];
    } else {
      parts ~= mn[0..pos];
    }
    mn = mn[pos..$];
  }
  {
    import std.array : join;
    string path = parts.join("/")~".dacs";
    res = getTextFile(path);
    if (res !is null) return res;
    assert(res is null);
  }
  assert(res is null);
  throw new Exception("module '"~modname~"' not found");
}


__gshared string[] dacsModules;

public void registerWadScripts () {
  try {
    import std.array : split;
    auto t = loadTextFile("scripts/dacsmain.txt");
    foreach (string line; t.split('\n')) {
      while (line.length && line[0] <= ' ') line = line[1..$];
      if (line.length == 0 || line[0] == ';' || line[0] == '#') continue;
      while (line.length && line[$-1] <= ' ') line = line[0..$-1];
      import std.algorithm : canFind;
      if (!dacsModules.canFind(line)) dacsModules ~= line;
    }
  } catch (Exception e) { return; }
}


public void loadWadScripts () {
  if (moduleLoader is null) moduleLoader = (string modname) => modLoader(modname);
  try {
    //conwriteln("loading main DACS module '", mainmod, "'");
    foreach (string mod; dacsModules) parseModule(mod);
    parseComplete();
    updateActorClasses();
    setupDAPI();
    dacsFinalizeCompiler();
  } catch (CompilerException e) {
    _d_print_throwable(e);
    conwriteln("PARSE ERROR: ", e.file, " at ", e.line);
    conwriteln(e.toString);
    assert(0);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct HudScripts {
  FuncPool.FuncInfo fiDraw;

  void runDraw () {
    if (fiDraw !is null) {
      //conwriteln("HUD SCRIPT!");
      fiDraw();
    }
  }
}


public __gshared HudScripts hudScripts;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared LevelMap map;
//public __gshared DACSVM dvm;
public __gshared uint prngSyncSeed = 0x29a;
public __gshared uint prngSeed = 0x29a; // unsynced
public __gshared string curmapname;
public __gshared string nextmapname; // nonempty: go to next level

struct MapScripts {
  FuncPool.FuncInfo fiInit; // called after level map is loaded
  FuncPool.FuncInfo fiLoaded; // called after mosters set
  FuncPool.FuncInfo fiUnloading; // called before level is unloaded (i.e. player finished the level)
  FuncPool.FuncInfo fiPreThink; // called before monster think
  FuncPool.FuncInfo fiPostThink; // called after monster think

  void runInit () { if (fiInit !is null) fiInit(); }
  void runLoaded () { if (fiLoaded !is null) fiLoaded(); }
  void runUnloading () { if (fiUnloading !is null) fiUnloading(); }
  void runPreThink () { if (fiPreThink !is null) fiPreThink(); }
  void runPostThink () { if (fiPostThink !is null) fiPostThink(); }
}

public __gshared MapScripts mapscripts;


// generate next map name for exit
public string genNextMapName (int lnum) {
  import std.algorithm : endsWith;
  import std.path;
  string ext;
  string nn = curmapname;
  if (nn.endsWith(".d2m")) {
    ext = ".d2m";
    nn = nn[0..$-4];
  }
  if (nn[$-1] < '0' || nn[$-1] > '9') return null;
  uint mapnum = 0, mmul = 1;
  while (nn.length > 0 && nn[$-1] >= '0' && nn[$-1] <= '9') {
    mapnum += (nn[$-1]-'0')*mmul;
    mmul *= 10;
    nn = nn[0..$-1];
  }
  ++mapnum;
  if (lnum > 0 && lnum < 100) mapnum = lnum;
  if (mapnum > 99) return null; // alas
  import std.string : format;
  return "%s%02u%s".format(nn, mapnum, ext);
}


public void clearMapScripts () {
  mapscripts = mapscripts.init; // remove old scripts
}


public void setupMapScripts () {
  void setupFromModule(T) (T mod) {
    mapscripts.fiInit = FuncPool.findByFQMG(mod.name~".initialize", ":void");
    mapscripts.fiLoaded = FuncPool.findByFQMG(mod.name~".loaded", ":void");
    mapscripts.fiUnloading = FuncPool.findByFQMG(mod.name~".unloading", ":void");
    mapscripts.fiPreThink = FuncPool.findByFQMG(mod.name~".prethink", ":void");
    mapscripts.fiPostThink = FuncPool.findByFQMG(mod.name~".postthink", ":void");
  }

  import std.path : baseName, setExtension;
  clearMapScripts();
  string mapname = curmapname.baseName.setExtension("");
  // normal map scripts
  if (auto mod = dacsModuleFor("map", mapname)) {
    conwriteln("found module for map '", mapname, "'");
    setupFromModule(mod);
    return;
  }
  // not found, try "unnamed map"
  if (auto mod = dacsModuleFor("map", " ")) {
    conwriteln("using module for unknownmap '", mapname, "'");
    setupFromModule(mod);
    return;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// Park-Miller-Carta Pseudo-Random Number Generator, based on David G. Carta paper
// 31 bit of randomness
// seed is previous result, as usual
public uint prngR31next (uint seed) {
  if (seed == 0) seed = 0x29a;
  uint lo = 16807*(seed&0xffff);
  uint hi = 16807*(seed>>16);
  lo += (hi&0x7fff)<<16;
  lo += hi>>15;
  //if (lo > 0x7fffffff) lo -= 0x7fffffff; // should be >=, actually
  lo = (lo&0x7FFFFFFF)+(lo>>31); // same as previous code, but branch-less
  return lo;
}


// "synced" random, using common seed for all players
public uint syncrandu31 () {
  pragma(inline, true);
  return (prngSyncSeed = prngR31next(prngSyncSeed));
}


// "unsynced" random, seed isn't saved
public uint unsyncrandu31 () {
  pragma(inline, true);
  return (prngSeed = prngR31next(prngSeed));
}


// ////////////////////////////////////////////////////////////////////////// //
version(dacs_use_vm) {
// this is VAT function, argument order is reversed
void doWrite(bool donl) (FuncPool.FuncInfo fi, DACSVM vm) {
  import std.stdio;
  auto count = vm.popInt();
  while (count-- > 0) {
    auto type = vm.popInt();
    switch (cast(VATArgType)type) {
      case VATArgType.Int: write(vm.popInt()); break;
      case VATArgType.Uint: write(vm.popUint()); break;
      case VATArgType.Float: write(vm.popFloat()); break;
      case VATArgType.StrId: write(StrId(vm.popUint()).get); break;
      case VATArgType.ActorId: write("<actor>"); vm.popUint(); break; //TODO
      default: write("<invalid-type>"); vm.popUint(); break; //TODO
    }
  }
  static if (donl) writeln();
  // push dummy return value
  vm.pushInt(0);
}
} else {
extern(C) void doWrite(bool donl) (uint argc, ...) {
  import core.vararg;
  import std.stdio;
  mainloop: while (argc >= 2) {
    argc -= 2;
    int tp = va_arg!int(_argptr);
    switch (tp) {
      case VATArgType.Int:
        auto v = va_arg!int(_argptr);
        write(v);
        break;
      case VATArgType.Uint:
        auto v = va_arg!uint(_argptr);
        write(v);
        break;
      case VATArgType.Float:
        auto v = va_arg!float(_argptr);
        write(v);
        break;
      case VATArgType.StrId:
        auto v = StrId(va_arg!uint(_argptr)).get;
        write(v);
        break;
      case VATArgType.ActorId:
        auto v = ActorId(va_arg!uint(_argptr));
        write("<actor:", v.valid, ":", v.id, ">");
        //write("<actor>");
        break;
      default: write("<invalid-type>"); break mainloop;
    }
  }
  static if (donl) writeln();
}
}


// ////////////////////////////////////////////////////////////////////////// //
void animClearFrames (StrId classtype, StrId classname, StrId state) {
  auto adef = findActorDef(classtype.get, classname.get);
  if (adef is null) throw new Exception("animClearFrames: actor '"~classtype.get~":"~classname.get~"' not found!");
  adef.clearFrames(state);
}


void animAddFrame (StrId classtype, StrId classname, StrId state, uint dir, StrId sprname) {
  auto adef = findActorDef(classtype.get, classname.get);
  if (adef is null) throw new Exception("animAddFrame: actor '"~classtype.get~":"~classname.get~"' not found!");
  if (dir != 0) dir = 1; //TODO: process mirror flag here
  adef.addFrame(state, dir, sprname);
}


// ////////////////////////////////////////////////////////////////////////// //
void setupDAPI () {
  conwriteln("setting up D API");

  FuncPool["write"] = &doWrite!false;
  FuncPool["writeln"] = &doWrite!true;

  FuncPool["syncrandu31"] = &syncrandu31;

  FuncPool["animClearFrames"] = &animClearFrames;
  FuncPool["animAddFrame"] = &animAddFrame;

  FuncPool["actorSetAnimation"] = function void (ActorId me, StrId state) {
    if (!me.valid) return;
    if (auto adef = findActorDef(me.classtype!string, me.classname!string)) {
      me.zAnimstate = state;
      me.zAnimidx = 0;
    }
  };

  //FuncPool["getPlayer"] = function ActorId () { assert(0); };
  //FuncPool["isPlayer"] = function int (ActorId me) { return (me == players.ptr[0] ? 1 : 0); };

  FuncPool["getPlayerCount"] = function int () => 1;

  FuncPool["getPlayerActor"] = function ActorId (uint pnum) {
    if (pnum == 1) return players.ptr[0];
    return ActorId(0);
  };

  FuncPool["getPlayerButtons"] = function uint (uint pidx) { return (pidx == 1 ? plrKeysLast : 0); };

  FuncPool["mapGetTypeTile"] = function int (int x, int y) {
    int res = 0;
    if (map !is null && x >= 0 && y >= 0 && x < map.width && y < map.height) {
      res = map.tiles.ptr[LevelMap.Type].ptr[y*map.width+x];
      if (res != LevelMap.TILE_ACTTRAP) res &= 0x7f;
    }
    return res;
  };

  FuncPool["mapGetTile"] = function int (uint layer, int x, int y) {
    return (map !is null && x >= 0 && y >= 0 && x < map.width && y < map.height && layer >= 0 && layer <= 1 ? map.tiles.ptr[LevelMap.Front+layer].ptr[y*map.width+x] : 0);
  };

  FuncPool["mapSetTypeTile"] = function void (int x, int y, int tid) {
    if (map is null || x < 0 || y < 0 || x >= map.width || y >= map.height || tid < 0 || tid > 255) return;
    auto p = map.tiles.ptr[LevelMap.Type].ptr+y*map.width+x;
    if (*p != tid) {
      *p = cast(ubyte)tid;
      import render : mapDirty;
      mapDirty((1<<LevelMap.Type)|(1<<LevelMap.LightMask));
    }
  };

  FuncPool["mapSetTile"] = function void (uint layer, int x, int y, int tid) {
    if (map is null || layer < 0 || layer > 1 || x < 0 || y < 0 || x >= map.width || y >= map.height || tid < 0 || tid > 255) return;
    auto p = map.tiles.ptr[LevelMap.Front+layer].ptr+y*map.width+x;
    if (*p != tid) {
      *p = cast(ubyte)tid;
      import render : mapDirty;
      if (layer == 0) {
        // front
        mapDirty((1<<LevelMap.Front)|(1<<LevelMap.AllLiquids)|(1<<LevelMap.LiquidMask));
      } else {
        // back
        mapDirty((1<<LevelMap.Back)|(1<<LevelMap.AllLiquids)|(1<<LevelMap.LiquidMask));
      }
    }
  };

  FuncPool["mapGetWaterTexture"] = function int (int fg) {
    if (fg < 0 || fg >= map.wallnames.length) return 0;
    switch (map.wallnames.ptr[fg]) {
      case "_water_0": return 1;
      case "_water_1": return 2;
      case "_water_2": return 3;
      default:
    }
    return 0;
  };

  FuncPool["getMapViewHeight"] = function int () {
    import render : vlWidth, vlHeight, getScale;
    return vlHeight/getScale;
  };

  FuncPool["actorsOverlap"] = function int (ActorId a, ActorId b) { return (actorsOverlap(a, b) ? 1 : 0); };

  FuncPool["actorRemove"] = function void (ActorId me) {
    if (me.valid) {
      if ((me.fget_flags&AF_NOCOLLISION) == 0) ugActorModify!false(me); // remove from grid
      Actor.remove(me);
    }
  };

  FuncPool["rewindTouchList"] = &rewindTouchList;
  FuncPool["getNextTouchListItem"] = &getNextTouchListItem;

  FuncPool["actorListRewind"] = &xactorListRewind;
  FuncPool["actorListNext"] = &xactorListNext;

  FuncPool["getCheatNoDoors"] = function int () { import render : cheatNoDoors; return (cheatNoDoors ? 1 : 0); };
  FuncPool["getCheatNoWallClip"] = function int () { import render : cheatNoWallClip; return (cheatNoWallClip ? 1 : 0); };
  FuncPool["getCheatNoCeilClip"] = function int () { return 0; };
  FuncPool["getCheatNoLiftClip"] = function int () { return 0; };

  FuncPool["addMessage"] = function void (string text, int pause, bool noreplace) {
    import render : postAddMessage;
    postAddMessage(text, pause, noreplace);
  };

  import d2dparts : dotAddBlood, dotAddSpark, dotAddWater;

  FuncPool["dotAddBlood"] = &dotAddBlood;
  FuncPool["dotAddSpark"] = &dotAddSpark;
  FuncPool["dotAddWater"] = &dotAddWater;
  /*
    function void (int x, int y, int xv, int yv, int n, int color) {
      conwriteln("dotAddWater: x=", x, "; y=", y, "; xv=", xv, "; yv=", yv, "; n=", n, "; color=", color);
      dotAddWater(x, y, xv, yv, n, color);
    };
  */

  FuncPool["gactLevelExit"] = function void (int lnum) {
    if (nextmapname.length == 0) {
      string nmname = genNextMapName(lnum);
      if (nmname.length == 0) assert(0, "no level!");
      nextmapname = nmname;
    }
  };

  FuncPool["actorSpawn"] = &actorSpawn;

  // return previous one
  FuncPool["setCameraChick"] = function ActorId (ActorId act) {
    auto res = cameraChick;
    cameraChick = act;
    return res;
  };

  FuncPool["MapWidth"] = function int () { return (map !is null ? map.width : 1); };
  FuncPool["MapHeight"] = function int () { return (map !is null ? map.height : 1); };

  FuncPool["viewportWidth"] = function int () { return vlWidth; };
  FuncPool["viewportHeight"] = function int () { return vlHeight; };

  FuncPool["spriteWidth"] = function int (string spfile) {
    auto im = loadSprite(spfile, true);
    return (im !is null ? im.vga.width : 0);
  };

  FuncPool["spriteHeight"] = function int (string spfile) {
    auto im = loadSprite(spfile, true);
    return (im !is null ? im.vga.height : 0);
  };

  FuncPool["drawSpriteAt"] = function void (string spfile, int x, int y) {
    auto im = loadSprite(spfile, true);
    if (im !is null) {
      im.drawAtXY(x, y);
    }
  };
  FuncPool["drawSpriteAtNoOfs"] = function void (string spfile, int x, int y) {
    auto im = loadSprite(spfile, true);
    if (im !is null) {
      im.drawAtXY(x+im.vga.sx, y+im.vga.sy-im.vga.height);
    }
  };

  FuncPool["drawTextRGBA"] = function void (string text, int x, int y, int r, int g, int b, int a) {
    import d2dfont : smbwDrawText;
    if (r < 0) r = 0; else if (r > 255) r = 255;
    if (g < 0) g = 0; else if (g > 255) g = 255;
    if (b < 0) b = 0; else if (b > 255) b = 255;
    if (a < 0) a = 0; else if (a > 255) a = 255;
    glColor4f(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
    smbwDrawText(x, y, text);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  };

  FuncPool["drawTextBigRGBA"] = function void (string text, int x, int y, int r, int g, int b, int a) {
    import d2dfont : bfDrawText;
    if (r < 0) r = 0; else if (r > 255) r = 255;
    if (g < 0) g = 0; else if (g > 255) g = 255;
    if (b < 0) b = 0; else if (b > 255) b = 255;
    if (a < 0) a = 0; else if (a > 255) a = 255;
    glColor4f(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
    bfDrawText(x, y, text);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  };

  FuncPool["textWidth"] = function int (string text) { import d2dfont : smTextWidth; return smTextWidth(text); };
  FuncPool["textHeight"] = function int (string text) { import d2dfont : smTextHeight; return smTextHeight(text); };

  FuncPool["textWidthBig"] = function int (string text) { import d2dfont : bfTextWidth; return bfTextWidth(text); };
  FuncPool["textHeightBig"] = function int (string text) { import d2dfont : bfTextHeight; return bfTextHeight(text); };

  FuncPool["drawRectRGBA"] = function void (int x, int y, int w, int h, int r, int g, int b, int a) {
    if (w < 1 || h < 1) return;
    if (r < 0) r = 0; else if (r > 255) r = 255;
    if (g < 0) g = 0; else if (g > 255) g = 255;
    if (b < 0) b = 0; else if (b > 255) b = 255;
    if (a < 0) a = 0; else if (a > 255) a = 255;
    glColor4f(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
    bindTexture(0);
    glRectf(x, y, x+w-1, y+h-1);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  };
}


public void registerAPI () {
  //Actor.actorSize += 256;
  conwriteln("actor size is ", Actor.actorSize, " bytes");

  version(dacs_use_vm) FuncPool.vm = new DACSVM();
  //dvm = new DACSVM();

  // initialize actor animation
  ActorDef.forEach((adef) => adef.callAnimInit());

  conwriteln("API registered");
}


// ////////////////////////////////////////////////////////////////////////// //
mixin(Actor.FieldGetMixin!("classtype", StrId)); // fget_classtype
mixin(Actor.FieldGetMixin!("classname", StrId)); // fget_classname
mixin(Actor.FieldGetMixin!("x", int));
mixin(Actor.FieldGetMixin!("y", int));
mixin(Actor.FieldGetMixin!("dir", uint));
mixin(Actor.FieldGetMixin!("height", int));
mixin(Actor.FieldGetMixin!("radius", int));
mixin(Actor.FieldGetMixin!("flags", uint));
mixin(Actor.FieldGetMixin!("zAnimstate", StrId));
mixin(Actor.FieldGetMixin!("zAnimidx", int));

mixin(Actor.FieldSetMixin!("zAnimidx", int));
mixin(Actor.FieldSetMixin!("flags", uint));


public bool actorsOverlap (ActorId a, ActorId b) {
  if (!a.valid || !b.valid) return false;
  if (a.id == b.id) return false; // no self-overlap

  int ax = a.fget_x;
  int bx = b.fget_x;
  int ar = a.fget_radius;
  int br = b.fget_radius;

  if (ax-ar > bx+br || ax+ar < bx-br) return false;

  int ay = a.fget_y;
  int by = b.fget_y;
  int ah = a.fget_height;
  int bh = b.fget_height;

  //return (ay > by-bh && ay-ah < by);
  return (by-bh <= ay && by >= ay-ah);
}


// ////////////////////////////////////////////////////////////////////////// //
public __gshared ActorId[2] players;


public void loadAllMonsterGraphics () {
  ActorDef.forEach((adef) {
    conwriteln("loading graphics for '", adef.classtype.get, ":", adef.classname.get, "'");
    adef.loadGraphics();
  });
  //Actor.dumpActors();
  realiseSpriteAtlases();
}


ActorId actorSpawn (StrId classtype, StrId classname, int x, int y, uint dir) {
  auto adef = findActorDef(classtype, classname);
  if (adef is null) return ActorId(0);
  auto aid = Actor.alloc;
  aid.classtype = classtype;
  aid.classname = classname;
  aid.state = StrPool.MNST_SLEEP;
  aid.x = x;
  aid.y = y;
  aid.dir = (dir ? 1 : 0);
  adef.callInit(aid);
  if ((aid.fget_flags&AF_NOCOLLISION) == 0) ugActorModify!true(aid);
  return aid;
}


public void loadMapMonsters () {
  assert(map !is null);
  ugClear();
  players[] = ActorId(0);
  //conwriteln(players[0].valid, "; ", players[0].id);
  foreach (ref thing; map.things) {
    if (thing.dmonly) continue;
    auto did = (thing.type&0x7fff) in d2dactordefsById;
    if (did !is null && did.classtype == "playerstart") {
      if ((thing.type&0x7fff) == 1 || (thing.type&0x7fff) == 2) {
        int pnum = thing.type-1;
        if (!players[pnum].valid) {
          auto aid = Actor.alloc;
          aid.classtype = StrPool.intern("monster");
          aid.classname = StrPool.intern("Player");
          aid.plrnum = cast(uint)(pnum+1);
          aid.state = StrPool.MNST_SLEEP;
          aid.x = cast(int)thing.x;
          aid.y = cast(int)thing.y;
          aid.dir = cast(uint)(thing.right ? 1 : 0);
          auto adef = findD2DActorDef(thing.type);
          if (adef is null) assert(0);
          adef.callInit(aid);
          if ((aid.fget_flags&AF_NOCOLLISION) == 0) ugActorModify!true(aid);
          players[pnum] = aid;
          conwriteln("player #", pnum+1, " aid is ", aid.id);
        }
      }
      continue;
    }
    auto adef = findD2DActorDef(thing.type&0x7fff);
    if (adef is null) {
      if (did !is null) {
        conwriteln("ignoring D2D thing '", did.classtype.get, ":", did.classname.get, "'");
      } else {
        conwriteln("ignoring unknown D2D thing with mapid ", thing.type);
      }
      continue;
    }
    // create actor and initialize it
    auto aid = Actor.alloc;
    aid.classtype = StrPool.intern(adef.classtype);
    aid.classname = StrPool.intern(adef.classname);
    //conwriteln("found '", aid.classtype.get, ":", aid.classname.get, "'");
    if (did !is null) {
      assert(did.classtype == adef.classtype);
      assert(did.classname == adef.classname);
      conwriteln("mapid=", thing.type, "; ", adef.classtype, ":", adef.classname, "; id=", aid.id);
      assert(aid.classtype!string == adef.classtype);
      assert(aid.classname!string == adef.classname);
    } else {
      assert(0);
    }
    aid.state = StrPool.MNST_SLEEP;
    aid.x = cast(int)thing.x;
    aid.y = cast(int)thing.y;
    aid.dir = cast(uint)(thing.right ? 1 : 0);
    if (thing.type&0x8000) aid.flags = aid.fget_flags|AF_NOGRAVITY;
    //if (aid.classtype!string == "item" && aid.x!int < 64) { aid.x = 92; aid.y = aid.y!int-16; conwriteln("!!!"); }
    adef.callInit(aid);
    if ((aid.fget_flags&AF_NOCOLLISION) == 0) ugActorModify!true(aid);
  }

  // create switches
  foreach (ref sw; map.switches) {
    if (sw.type == 0) continue; // just in case
    auto swname = getD2DSwitchClassName(sw.type);
    if (swname.length == 0) {
      conwriteln("unknown switch type ", sw.type);
      continue;
    }
    if (auto adef = findActorDef("switch", swname)) {
      auto aid = Actor.alloc;
      aid.classtype = StrPool.intern("switch");
      aid.classname = StrPool.intern(swname);
      // nocollision, 'cause collision checking will be processed in switch thinker, but other actors should not touch switches
      aid.flags = /*AF_NOCOLLISION|*/AF_NOGRAVITY|/*AF_NOONTOUCH|*/AF_NODRAW|AF_NOLIGHT|AF_NOANIMATE;
      aid.x = sw.x*TileSize;
      aid.y = sw.y*TileSize;
      aid.switchabf = (sw.a<<16)|(sw.b<<8)|sw.flags;
      // should be enough
      aid.radius = 16;
      aid.height = 16;
      adef.callInit(aid);
      if ((aid.fget_flags&AF_NOCOLLISION) == 0) ugActorModify!true(aid); // just in case
    } else {
      conwriteln("switch definition 'switch:", swname, "' not found");
    }
  }

  conwriteln("initial snapshot size: ", Actor.snapshotSize, " bytes");
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ActorId[65536] xactorList; // aids
__gshared uint xactorListCount, xactorListIndex = uint.max;


// dacs API
void xactorListRewind () {
  if (!touchListAllowed) return; // it's ok to reuse it here
  xactorListCount = xactorListIndex = 0;
  Actor.forEach((ActorId me) {
    if (me.fget_classtype != StrPool.X_X) xactorList.ptr[xactorListCount++] = me;
  });
}


// dacs API
ActorId xactorListNext () {
  if (!touchListAllowed) return ActorId(0); // it's ok to reuse it here
  if (xactorListIndex == uint.max) xactorListRewind();
  while (xactorListIndex < xactorListCount) {
    auto aid = xactorList.ptr[xactorListIndex];
    ++xactorListIndex;
    if (aid.fget_classtype == StrPool.X_X) continue;
    return aid;
  }
  return ActorId(0);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared uint[65536] touchList; // aids
__gshared uint[] realTouchList;
__gshared uint realTouchListIndex = uint.max;
__gshared ActorId curThinkingActor;
__gshared bool touchListAllowed = false;


// dacs API
void rewindTouchList () {
  if (!touchListAllowed) return;
  realTouchListIndex = 0;
  realTouchList = ugActorHitList(curThinkingActor, touchList[]);
}


// dacs API
ActorId getNextTouchListItem () {
  if (!touchListAllowed) return ActorId(0);
  if (realTouchListIndex == uint.max) rewindTouchList();
  while (realTouchListIndex < realTouchList.length) {
    auto aid = ActorId(realTouchList.ptr[realTouchListIndex]);
    ++realTouchListIndex;
    if (aid.fget_classtype == StrPool.X_X) continue;
    if (aid.valid && (aid.fget_flags&AF_NOONTOUCH) == 0) return aid;
  }
  return ActorId(0);
}


public void doActorsThink () {
  // we have too much memory!
  __gshared uint[65536] validActorsList;
  __gshared ActorId[65536] postponedDeath;
  __gshared uint pdcount;

  touchListAllowed = true;
  scope(exit) touchListAllowed = false;
  pdcount = 0;
  // clear flags
  Actor.forEach((ActorId me) { me.fset_flags(me.fget_flags&~AF_TELEPORT); });
  mapscripts.runPreThink();
  foreach (uint xid; Actor.getValidList(validActorsList[])) {
    auto me = ActorId(xid);
    if (me.valid) {
      if (me.fget_classtype == StrPool.X_X) { postponedDeath.ptr[pdcount++] = me; continue; }
      auto flags = me.fget_flags;
      if ((flags&AF_NOCOLLISION) == 0) ugActorModify!false(me); // remove from grid
      if (auto adef = findActorDef(me)) {
        if ((flags&AF_NOTHINK) == 0) {
          realTouchListIndex = uint.max;
          xactorListIndex = uint.max;
          curThinkingActor = me;
          adef.callThink(me);
          //if (me.x!int < 32) conwriteln("actor: ", me.id, "; attLightRGBX=", me.attLightRGBX!uint);
          if (!me.valid) continue; // we are dead
          if (me.fget_classtype == StrPool.X_X) { postponedDeath.ptr[pdcount++] = me; continue; }
          flags = me.fget_flags; // in case script updated flags
        }
        if ((flags&AF_NOANIMATE) == 0) {
          int aidx = me.fget_zAnimidx;
          int nidx = adef.nextAnimIdx(me.fget_zAnimstate, me.fget_dir, aidx);
          //conwriteln("actor ", me.id, " (", me.classtype!string, me.classname!string, "): state=", me.zAnimstate!string, "; aidx=", aidx, "; nidx=", nidx);
          me.fset_zAnimidx = nidx;
          //assert(me.fget_zAnimidx == nidx);
        }
        // put back to grid
        if ((flags&AF_NOCOLLISION) == 0) ugActorModify!true(me);
      }
    }
  }
  mapscripts.runPostThink();
  // process scheduled death
  foreach (ActorId aid; postponedDeath[0..pdcount]) {
    /*if ((flags&AF_NOCOLLISION) == 0)*/ ugActorModify!false(aid); // remove from grid
    Actor.remove(aid);
  }
  // reset player keys
  plrKeysFix(0);
  //{ import std.stdio : stdout; stdout.writeln("========================================="); }
  //Actor.dumpActors();
}
