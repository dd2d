/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#version 130

#define HUMANIOD_THETA

#define INTO_DEPTH  (4)

#define BIGSTEP  (1)

#define PI  (3.14159265)

//uniform sampler2D texOcc;
uniform vec2 lightTexSize;

uniform sampler2D texOccFull; // occluders map, full
uniform sampler2D texOccSmall; // occluders, small map
uniform vec2 lightPos; // light coords in occluders map
uniform vec2 mapPixSize; // size of occluders map

// alpha threshold for our occlusion map
#define THRESHOLD  (0.75)


void main (void) {
  //float dmposx = gl_TexCoord[0].x;
  //vec2 ltPos = lightPos/mapPixSize;
  /*if (texture2D(texOccFull, ltPos).a > THRESHOLD) {
    gl_FragColor = vec4(vec3(1.0), 1.0);
  } else*/
  {
    float dmposx = gl_FragCoord.x/lightTexSize.x;
    // theta: 90 and greater
#ifdef HUMANIOD_THETA
    float theta = 360.0*dmposx;
    theta += 90; // turn it
    //theta = PI*theta/180.0; // to radians
    theta = radians(theta);
#else
    float theta = PI*1.5+(dmposx*2.0-1.0)*PI;
#endif
    float nsint = -sin(theta);
    float ncost = cos(theta);
    vec2 dir = normalize(vec2(nsint, ncost));

    float dst = 1.0;

    vec2 a = lightPos;
    vec2 b = a+dir*(lightTexSize.x/2);
    vec2 step;
    float len;
    if (abs(b.x-a.x) > abs(b.y-a.y)) {
      // horizontal
      len = abs(b.x-a.x);
      step = vec2(sign(b.x-a.x), (b.y-a.y)/len);
    } else {
      // vertical
      len = abs(b.y-a.y);
      step = vec2((b.x-a.x)/len, sign(b.y-a.y));
    }

    while (len > 0) {
      // do big step
      vec2 aa = a;
      a += step*BIGSTEP;
      vec2 ctxa = a/mapPixSize;
      vec2 ctxaa = aa/mapPixSize;
      if (texture2D(texOccSmall, ctxa, 0).a > THRESHOLD ||
          texture2D(texOccSmall, ctxaa, 0).a > THRESHOLD ||
          texture2D(texOccSmall, vec2(ctxaa.x, ctxa.y), 0).a > THRESHOLD ||
          texture2D(texOccSmall, vec2(ctxa.x, ctxaa.y), 0).a > THRESHOLD)
      {
        // do small steps
        int xlen = (len >= BIGSTEP ? BIGSTEP : int(len));
        while (xlen-- > 0) {
          if (texture2D(texOccFull, aa/mapPixSize, 0).a > THRESHOLD) {
            #ifdef INTO_DEPTH
            for (int stp = INTO_DEPTH; stp > 0; --stp) {
              vec2 b = aa+step;
              if (texture2D(texOccFull, b/mapPixSize, 0).a <= THRESHOLD) break;
              aa = b;
            }
            #endif
            dst = distance(lightPos, aa)/(lightTexSize.x/2.0);
            //xlen = -42;
            len = -42;
            break;
          }
          --xlen;
          aa += step;
        }
      }
      len -= BIGSTEP;
    }

    // done
    dst = max(min(dst, 1.0), 0.0);
    gl_FragColor = vec4(vec3(dst), 1.0);
  }
}
