/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#version 130

#define PI 3.1415926

uniform sampler2D texBg; // background
uniform sampler2D texOcc; // occluders
uniform sampler2D texOccSmall; // occluders, small map
//uniform vec2 mapPixSize;
uniform vec4 lightColor;
//uniform vec2 lightPos;


//gl_FragCoord: [0..lightTexSize)

#define lit (1.0)


void main (void) {
  vec2 btxy = gl_FragCoord.xy;
  //btxy.y = mapPixSize.y-btxy.y-1.0;
  //vec2 ocxy = vec2(btxy.x, mapPixSize.y-btxy.y)/mapPixSize;
  vec4 color;

  // has some light here
  vec4 bcolor = texelFetch(texBg, ivec2(int(btxy.x), int(btxy.y)), 0);
  if (lightColor.r+lightColor.g+lightColor.b == 0.0) {
    color = bcolor*vec4(vec3(lightColor.a), lit);
  } else {
    color = lightColor*lit;
    color += bcolor*lit;
    color.a = lit/2.0;
  }

  gl_FragColor = color;
}
