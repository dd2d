/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#version 130

//#define TWO_TEXTURES

// alpha threshold for our occlusion map
#define THRESHOLD  (0.75)

#define PI 3.1415926

uniform sampler2D texDist; // 1D distance
uniform sampler2D texBg; // background
uniform sampler2D texOccFull; // occluders
//uniform sampler2D texOccSmall; // occluders, small map
uniform vec2 lightTexSize; // x: size of distmap; y: size of this texture
uniform vec2 mapPixSize;
uniform vec4 lightColor;
uniform vec2 lightPos; // lefttop
#define SHADOW_DO_BLUR

#define SOFT_SHADOWS  (1.0f)


// sample from the 1D distance map
#define sample(cc_)  step(r, texture2D(texDist, vec2(tc.x+(cc_)*blur, /*tc.y*/0.0)).r)

//gl_FragCoord.x: [0..lightTexSize.x)
//gl_FragCoord.y: somehow it is constant; wtf?!

void main (void) {
  vec2 vTexCoord0 = gl_TexCoord[0].xy;
  // don't even ask me!
  vec2 tctxy = vec2(lightPos.x+gl_FragCoord.x, lightPos.y+gl_TexCoord[0].y*lightTexSize.x);
  ivec2 iocxy = ivec2(int(tctxy.x), int(tctxy.y));
  ivec2 ibtxy = ivec2(iocxy.x, int(mapPixSize.y-tctxy.y));
  vec4 color;

  if (texelFetch(texOccFull, iocxy, 0).a > THRESHOLD) {
    // occluder hit, this will take care of lighted walls (we'll fix wall in next pass)
    color = vec4(0.0, 0.0, 0.0, 0.0);
    #ifdef TWO_TEXTURES
    gl_FragData[0] = color;
    gl_FragData[1] = color;
    #else
    gl_FragColor = color;
    #endif
  } else {
    float ltsxi = 1.0/lightTexSize.x;

    // rectangular to polar
    vec2 norm = vTexCoord0.st*2.0-1.0;
    float theta = atan(norm.y, norm.x);
    float r = length(norm);
    float coord = (theta+PI)/(2.0*PI);

    // the texDist coord to sample our 1D lookup texture
    // always 0.0 on y axis
    vec2 tc = vec2(coord, 0.0);

    // the center texDist coord, which gives us hard shadows
    float center = step(r, texture2D(texDist, tc.xy).r); //sample(vec2(tc.x, tc.y), r);

    // now we use a simple gaussian blur
    #ifdef SHADOW_DO_BLUR
      // we multiply the blur amount by our distance from center
      // this leads to more blurriness as the shadow "fades away"
      float blur = ltsxi*smoothstep(0.0, 1.0, r);

      float sum  = sample(-4.0)*0.05;
            sum += sample(-3.0)*0.09;
            sum += sample(-2.0)*0.12;
            sum += sample(-1.0)*0.15;
            sum += center*0.16;
            sum += sample( 1.0)*0.15;
            sum += sample( 2.0)*0.12;
            sum += sample( 3.0)*0.09;
            sum += sample( 4.0)*0.05;
      // sum of 1.0 -> in light, 0.0 -> in shadow
      float lit = mix(center, sum, SOFT_SHADOWS);
    #else
      float lit = center;
    #endif

    // multiply the summed amount by our distance, which gives us a radial falloff
    lit = lit*smoothstep(1.0, 0.0, r);

    if (lit == 0.0) {
      color = vec4(0.0, 0.0, 0.0, 0.0);
      #ifdef TWO_TEXTURES
      gl_FragData[0] = color;
      gl_FragData[1] = color;
      #else
      gl_FragColor = color;
      #endif
    } else {
      // has some light here
      vec4 bcolor = texelFetch(texBg, ibtxy, 0);
      if (lightColor.r+lightColor.g+lightColor.b == 0.0) {
        color = bcolor*vec4(vec3(lightColor.a), lit);
      } else {
        color = lightColor*lit;
        color += bcolor*lit;
        color.a = lit/2.0;
      }
      #ifdef TWO_TEXTURES
      gl_FragData[0] = color;
      gl_FragData[1] = vec4(1.0, 1.0, 1.0, lit);
      #else
      gl_FragColor = color;
      #endif
    }
  }
}
