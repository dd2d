/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#version 130

uniform sampler2D texLqMap; // liquid map
uniform float iDistortTime;


#define speed      (0.1)
#define frequency  (8.0)

vec2 shift (in vec2 p) {
  vec2 d = vec2(iDistortTime*speed);
  vec2 f = frequency*(p+d);
  return cos(vec2(cos(f.x-f.y)*cos(f.y), sin(f.x+f.y)*sin(f.y)));
}


void main (void) {
  vec2 texCoord = gl_TexCoord[0].xy;
  vec4 clr = texture2D(texLqMap, texCoord);
  if (clr.a == 0.0) { discard; return; }
  // distort
  vec2 r = texCoord.xy;
  vec2 p = shift(r);
  vec2 q = shift(r+1.0);
  float amplitude = 2.0/textureSize(texLqMap, 0).x;
  vec2 s = r+amplitude*(p-q);
  clr = texture2D(texLqMap, s);
  // if distortion moved out of liquid area, dup texel instead
  if (clr.a == 0.0) clr = texture2D(texLqMap, texCoord);
  clr.a = 1.0;
  gl_FragColor = clr;
}
