/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this shader will light level geometry
#version 130

#define PI 3.1415926

// alpha threshold for our occlusion map
#define THRESHOLD  (0.75)

#define SEARCH_RADIUS  (4)
#define MIN_ALPHA  (0.04)

uniform sampler2D texLMap; // light texture of lightTexSize
uniform sampler2D texBg; // background
uniform sampler2D texOccFull; // occluders
uniform sampler2D texOccSmall; // occluders, small map
uniform vec2 lightTexSize; // x: lightSize; y: size of this texture
uniform vec2 mapPixSize;
uniform vec4 lightColor;
uniform vec2 lightPos;


bool lightAtTile (vec2 ltpos, float pixelStep) {
  if (ltpos.x < 0 || ltpos.y < 0 || ltpos.x >= 1.0 || ltpos.y >= 1.0) return false;
  float lx = ltpos.x;
  for (int dy = 8; dy > 0; --dy) {
    for (int dx = 8; dx > 0; --dx) {
      if (texture2D(texLMap, ltpos, 0).a > MIN_ALPHA) return true;
      ltpos.x += pixelStep;
    }
    ltpos = vec2(lx, ltpos.y+pixelStep);
  }
  return false;
}


//gl_FragCoord: in background

void main (void) {
  // light is y-mirrored
  vec2 ourpos = vec2(gl_FragCoord.x, mapPixSize.y-gl_FragCoord.y);

  ivec2 ocxy = ivec2(int(ourpos.x), int(ourpos.y));
  // empty space?
  if (texelFetch(texOccFull, ocxy, 0).a <= THRESHOLD) {
    discard;
    //gl_FragColor = vec4(1.0, 0.0, 0.0, 0.5);
    return;
  }

  // check adjacent tiles
  bool hit = false;
  {
#if 0
    vec2 ltpos = gl_TexCoord[0].xy; // in light texture (texLMap)
    float pixelStep = 1.0/lightTexSize.y; // in light texture
    int move = ocxy.x&0x07;
    ltpos.x -= pixelStep*move;
    move = int(gl_FragCoord.y)&0x07;
    ltpos.y -= pixelStep*move;
    // check adjacent tiles
    ltpos -= pixelStep*8;
    float savedx = ltpos.x;
    for (int dy = -1; dy < 2 && !hit; ++dy) {
      for (int dx = -1; dx < 2 && !hit; ++dx) {
        if (dx != 0 || dy != 0) hit = lightAtTile(ltpos, pixelStep);
        ltpos.x += pixelStep*8;
      }
      ltpos = vec2(savedx, ltpos.y+pixelStep*8);
    }
#else
    /*
    for (int dy = 8; dy > 0; --dy) {
      if (texture2D(texLMap, ltpos, 0).a > MIN_ALPHA) { hit = true; break; }
      ltpos.y -= pixelStep;
    }
    */
    //gl_FragColor = vec4(gl_TexCoord[0].y, 0, 0, 1);
    //return;
    /*
    ocxy.x = ocxy.x&~7;
    ocxy.y = ocxy.y&~7;
    */
    float tlsz = textureSize(texLMap, 0).y;
    vec2 ltpos = gl_TexCoord[0].xy; // in light texture (texLMap)
    float pixelStep = 1.0/tlsz; // in light texture
    //ltpos.y += pixelStep;
    //hit = (texture2D(texLMap, ltpos/tlsz, 0).a > MIN_ALPHA);
    hit = (
      texture2D(texLMap, vec2(ltpos.x-pixelStep, ltpos.y-pixelStep), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x          , ltpos.y-pixelStep), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x+pixelStep, ltpos.y-pixelStep), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x-pixelStep, ltpos.y          ), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x+pixelStep, ltpos.y          ), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x-pixelStep, ltpos.y+pixelStep), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x          , ltpos.y+pixelStep), 0).a > MIN_ALPHA ||
      texture2D(texLMap, vec2(ltpos.x+pixelStep, ltpos.y+pixelStep), 0).a > MIN_ALPHA ||
      false
      );
#endif
  }

  if (hit) {
    vec4 color;
    // background
    //vec2 btxy = gl_FragCoord.xy/mapPixSize;
    //vec4 bcolor = texture2D(texBg, btxy);
    vec4 bcolor = texelFetch(texBg, ivec2(int(gl_FragCoord.x), int(gl_FragCoord.y)), 0);
    // distance from this point to light origin
    float lit = clamp(1.0-(distance(ourpos, lightPos)/(lightTexSize.x/2.0)), 0.0, 1.0);
    lit = smoothstep(0.1, 1.0, lit);
    if (lightColor.a == 0.0) {
      color = bcolor*vec4(vec3(1.0), lit);
    } else {
      color = lightColor*lit;
      color += bcolor*lit;
      color.a = lit/2.0;
    }
    gl_FragColor = color;
    //gl_FragColor = bcolor;
  } else {
    discard;
    //gl_FragColor = vec4(0.0, 0.0, 1.0, 0.5);
  }

#if 0
  // occluder hit
  // first, we should check if there is no other occluders in +x, +y, and +x+y directions
  ivec2 a8 = ocxy/8;
  //ivec2 ldxy = ivec2(int(sign(lightPos.x-gl_FragCoord.x)), int(sign(gl_FragCoord.y-lightPos.y)));
  ivec2 ltposd8 = ivec2(int(lightPos.x), int(lightPos.y))/8;
  //ivec2 ltposd8 = ocxy/8;
  ivec2 ldxy = ivec2(sign(ltposd8.x-a8.x), sign(ltposd8.y-a8.y));
#if 0
  if (ldxy.x == 0 && ldxy.y == 0) { discard; return; }
  if (ldxy.x != 0 && ldxy.y != 0) {
    // both directions
    if (/*texelFetch(texOccSmall, ivec2(a8.x+ldxy.x, a8.y), 0).a > THRESHOLD &&
        texelFetch(texOccSmall, ivec2(a8.x, a8.y+ldxy.y), 0).a > THRESHOLD &&*/
        texelFetch(texOccSmall, a8+ldxy, 0).a > THRESHOLD) { discard; return; }
  } else {
    // only horizontal or only vertical
    if (texelFetch(texOccSmall, a8+ldxy, 0).a > THRESHOLD) { discard; return; }
  }
#endif
  // ok, it's not blocked

  // check for light in +x, +y, and +x+y directions
  vec2 ltpos = gl_TexCoord[0].xy*textureSize(texLMap, 0).y/*lightTexSize.y*/; // in light texture (texLMap)
  ivec2 iltpos = ivec2(int(ltpos.x), int(ltpos.y));

  bool hit = false;
  float litlt = 0;
  for (int f = 1; f > 0; --f) {
    float ar = texelFetch(texLMap, iltpos, 0).a;
    ar = max(ar, texelFetch(texLMap, ivec2(iltpos.x+ldxy.x, iltpos.y), 0).a);
    ar = max(ar, texelFetch(texLMap, ivec2(iltpos.x, iltpos.y+ldxy.x), 0).a);
    ar = max(ar, texelFetch(texLMap, ivec2(iltpos.x+ldxy.x, iltpos.y+ldxy.x), 0).a);
    if (ar > MIN_ALPHA) {
      hit = true;
      ar *= 1.5;
      litlt = max(min(ar, 1.0), 0.0);
      break;
    }
    iltpos += ldxy;
  }

  if (hit) {
    vec4 color;
    // background
    //vec2 btxy = gl_FragCoord.xy/mapPixSize;
    //vec4 bcolor = texture2D(texBg, btxy);
    vec4 bcolor = texelFetch(texBg, ivec2(int(gl_FragCoord.x), int(gl_FragCoord.y)), 0);
    // distance from this point to light origin
    float lit = clamp(1.0-(distance(ourpos, lightPos)/(lightTexSize.x/2.0)), 0.0, 1.0);
    lit = smoothstep(0.1, 1.0, lit);
    lit = litlt;
    if (lightColor.a == 0.0) {
      color = bcolor*vec4(vec3(1.0), lit);
    } else {
      color = lightColor*lit;
      color += bcolor*lit;
      color.a = lit/2.0;
    }
    //gl_FragColor = color;
    gl_FragColor = bcolor;
  } else {
    discard;
  }
#endif
}
