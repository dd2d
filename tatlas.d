module tatlas is aliced;
private:

import arsd.color;

import d2dimage;
import glutils;

//version = tatlas_brect;


// ////////////////////////////////////////////////////////////////////////// //
public final class TexAtlas {
public:
  static struct Rect {
    int x, y, w, h;
    alias x0 = x;
    alias y0 = y;
  nothrow @safe @nogc:
    @property bool valid () const pure { pragma(inline, true); return (x >= 0 && y >= 0 && w > 0 && h > 0); }
    @property int area () const pure { pragma(inline, true); return w*h; }
    @property int x1 () const pure { pragma(inline, true); return x+w; }
    @property int y1 () const pure { pragma(inline, true); return y+h; }
    static @property Rect Invalid () pure { Rect res; return res; }
  }

  // texture coords in atlas
  static struct FRect {
    float x0, y0, x1, y1;
  }

private:
  public TrueColorImage img;
  Rect[] rects;
  public Texture tex;

  enum BadRect = uint.max;

public:
  this (int awdt, int ahgt) {
    assert(awdt > 0 && ahgt > 0);
    img = new TrueColorImage(awdt, ahgt);
    rects ~= Rect(0, 0, awdt, ahgt); // one big rect
  }

  @property const /*pure*/ /*nothrow*/ @safe /*@nogc*/ {
    int width () { pragma(inline, true); return img.width; }
    int height () { pragma(inline, true); return img.height; }
    bool hasTexture () { pragma(inline, true); return (tex !is null); }
  }

  void updateTexture () {
    version(tatlas_brect) foreach (const ref rc; rects) imgRect(rc.x, rc.y, rc.w, rc.h, Color(0, 255, 0, 255));
    if (tex is null) {
      tex = new Texture(img, Texture.Option.Clamp, Texture.Option.Nearest);
    } else {
      tex.setFromImage(img);
    }
  }

  FRect texCoords (in Rect rc) {
    FRect res;
    res.x0 = cast(float)rc.x/cast(float)(width);
    res.y0 = cast(float)rc.y/cast(float)(height);
    res.x1 = cast(float)(rc.x+rc.w)/cast(float)(width);
    res.y1 = cast(float)(rc.y+rc.h)/cast(float)(height);
    return res;
  }

  // node id or BadRect
  private uint findBestFit (int w, int h) {
    uint fitW = BadRect, fitH = BadRect, biggest = BadRect;

    foreach (immutable idx, const ref r; rects) {
      if (r.w < w || r.h < h) continue; // absolutely can't fit
      if (r.w == w && r.h == h) return cast(uint)idx; // perfect fit
      if (r.w == w) {
        // width fit
        if (fitW == BadRect || rects.ptr[fitW].h < r.h) fitW = cast(uint)idx;
      } else if (r.h == h) {
        // height fit
        if (fitH == BadRect || rects.ptr[fitH].w < r.w) fitH = cast(uint)idx;
      } else {
        // get biggest rect
        if (biggest == BadRect || rects.ptr[biggest].area > r.area) biggest = cast(uint)idx;
      }
    }
    // both?
    if (fitW != BadRect && fitH != BadRect) return (rects.ptr[fitW].area > rects.ptr[fitH].area ? fitW : fitH);
    if (fitW != BadRect) return fitW;
    if (fitH != BadRect) return fitH;
    return biggest;
  }

  private void imgPutPixel (int x, int y, Color c) {
    if (x >= 0 && y >= 0 && x < img.width && y < img.height) img.imageData.colors.ptr[y*img.width+x] = c;
  }

  private void imgRect (int x, int y, int w, int h, Color c) {
    foreach (int d; 0..w) { imgPutPixel(x+d, y, c); imgPutPixel(x+d, y+h-1, c); }
    foreach (int d; 1..h-1) { imgPutPixel(x, y+d, c); imgPutPixel(x+w-1, y+d, c); }
  }

  private void imgRect4 (int x, int y, int w, int h, Color ct, Color cb, Color cl, Color cr) {
    foreach (int d; 0..w) { imgPutPixel(x+d, y, ct); imgPutPixel(x+d, y+h-1, cb); }
    foreach (int d; 1..h-1) { imgPutPixel(x, y+d, cl); imgPutPixel(x+w-1, y+d, cr); }
  }

  // returns invalid rect if there's no room
  Rect insert (D2DImage di) {
    assert(di !is null && di.width > 0 && di.height > 0);
    auto res = insert(di.img);
    return res;
  }

  Rect insert (TrueColorImage ximg) {
    assert(ximg !is null && ximg.width > 0 && ximg.height > 0);
    auto ri = findBestFit(ximg.width, ximg.height);
    if (ri == BadRect) return Rect.Invalid;
    auto rc = rects.ptr[ri];
    auto res = Rect(rc.x, rc.y, ximg.width, ximg.height);
    // split this rect
    if (rc.w == res.w && rc.h == res.h) {
      // best fit, simply remove this rect
      foreach (immutable cidx; ri+1..rects.length) rects.ptr[cidx-1] = rects.ptr[cidx];
      rects.length -= 1;
      rects.assumeSafeAppend; // for future; we probably won't have alot of best-fitting nodes initially
    } else {
      if (rc.w == res.w) {
        // split vertically
        rc.y += res.h;
        rc.h -= res.h;
      } else if (rc.h == res.h) {
        // split horizontally
        rc.x += res.w;
        rc.w -= res.w;
      } else {
        Rect nr = rc;
        // split in both directions (by longer edge)
        if (rc.w-res.w > rc.h-res.h) {
          // cut the right part
          nr.x += res.w;
          nr.w -= res.w;
          // cut the bottom part
          rc.y += res.h;
          rc.h -= res.h;
          rc.w = res.w;
        } else {
          // cut the bottom part
          nr.y += res.h;
          nr.h -= res.h;
          // cut the right part
          rc.x += res.w;
          rc.w -= res.w;
          rc.h = res.h;
        }
        rects ~= nr;
      }
      rects.ptr[ri] = rc;
    }
    // copy image data
    auto sp = ximg.imageData.colors.ptr;
    auto dp = img.imageData.colors.ptr+res.y*img.width+res.x;
    foreach (immutable dy; 0..ximg.height) {
      dp[0..ximg.width] = sp[0..ximg.width];
      sp += ximg.width;
      dp += img.width;
    }
    version(tatlas_brect) imgRect4(res.x, res.y, res.w, res.h, Color(255, 0, 0), Color(255, 255, 0), Color(0, 0, 255), Color(0, 255, 0));
    return res;
  }
}
