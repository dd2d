/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module vga2png is aliced;

private:
import iv.cmdcon;
import iv.glbinds;
import glutils;
import wadarc;

import iv.vfs;

import d2dimage;


// ////////////////////////////////////////////////////////////////////////// //
import arsd.color;
import arsd.png;


// ////////////////////////////////////////////////////////////////////////// //
int main (string[] args) {
  if (args.length < 2 || args.length > 3) {
    conwriteln("usage: vga2png infile.vga [outfile.png]");
    return -1;
  }

  string ifname = args[1];
  string ofname;
  if (args.length == 3) {
    ofname = args[2];
  } else {
    import std.algorithm : endsWith;
    if (ifname.endsWith(".vga")) {
      ofname = ifname[0..$-4]~".png";
    } else {
      ofname = ifname~".png";
    }
  }

  if (ifname.length == 0) {
    conwriteln("image name?");
    return -1;
  }
  if (ifname[0] != '/') ifname = "./"~ifname;

  static void setDP () {
    /*version(rdmd) {
      setDataPath("data");
    } else*/ {
      import std.file : thisExePath;
      import std.path : dirName;
      setDataPath(thisExePath.dirName~"/data");
    }
    addPak(getDataPath~"base.pk3");
  }

  setDP();

  try {
    loadD2DPalette();
    auto img = new D2DImage(ifname);
    conwriteln("writing image '", ofname, "'");
    img.savePng(ofname);
  } catch (Exception e) {
    import std.stdio : stderr;
    stderr.writeln("FUUUUUUUUUUUU\n", e.toString);
    return -1;
  }
  return 0;
}
