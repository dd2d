/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module d2dmap is aliced;
private:

import arsd.color;
import iv.vfs;
import iv.cmdcon;

import glutils;

import d2dimage;

import iv.vfs;
import iv.vfs.koi8;


// ////////////////////////////////////////////////////////////////////////// //
public enum TileSize = 8;


// ////////////////////////////////////////////////////////////////////////// //
Color getPixel (TrueColorImage img, int x, int y) {
  if (x >= 0 && y >= 0 && x < img.width && y < img.height) {
    return img.imageData.colors.ptr[y*img.width+x];
  } else {
    return Color(0, 0, 0, 0);
  }
}


void putPixel (TrueColorImage img, int x, int y, Color clr) {
  if (x >= 0 && y >= 0 && x < img.width && y < img.height && clr.a != 0) {
    img.imageData.colors.ptr[y*img.width+x] = clr;
  }
}


void fixPixelA (TrueColorImage img, int x, int y, ubyte a) {
  if (x >= 0 && y >= 0 && x < img.width && y < img.height) {
    img.imageData.colors.ptr[y*img.width+x].a = a;
  }
}


void fixPixelTileA (TrueColorImage img, int x, int y, ubyte a) {
  foreach (int dy; 0..TileSize) {
    foreach (int dx; 0..TileSize) {
      fixPixelA(img, x+dx, y+dy, a);
    }
  }
}


void putPixelTile (TrueColorImage img, int x, int y, Color clr) {
  foreach (int dy; 0..TileSize) {
    foreach (int dx; 0..TileSize) {
      putPixel(img, x+dx, y+dy, clr);
    }
  }
}


void putTile (TrueColorImage img, int x, int y, D2DImage wt) {
  if (wt is null) return;
  x -= wt.sx;
  y -= wt.sy;
  foreach (int dy; 0..wt.height) {
    foreach (int dx; 0..wt.width) {
      img.putPixel(x+dx, y, wt.getPixel(dx, dy));
    }
    ++y;
  }
}


// temp
static ubyte clampByte (ubyte b, int delta) {
  delta += b;
  return cast(ubyte)(delta < 0 ? 0 : delta > 255 ? 255 : delta);
}


// ////////////////////////////////////////////////////////////////////////// //
public final class LevelMap {
private:
  enum MapVersion = 2; // ����� ��������� ������ �����
  public enum MapSize = 100;

public:
  // tile type
  enum : ubyte {
    TILE_EMPTY = 0,
    TILE_WALL = 1,
    TILE_DOORC = 2, // closed door
    TILE_DOORO = 3, // opened door
    TILE_STEP = 4,
    TILE_WATER = 5,
    TILE_ACID1 = 6,
    TILE_ACID2 = 7,
    TILE_MBLOCK = 8, // just blocks monsters
    TILE_LIFTU = 9,
    TILE_LIFTD = 10,
    //
    TILE_ACTTRAP = 255,
  }

  enum : short {
    MB_COMMENT = -1,
    MB_END = 0,
    MB_WALLNAMES,
    MB_BACK,
    MB_WTYPE,
    MB_FRONT,
    MB_THING,
    MB_SWITCH,
    MB_MUSIC, // usually 8 bytes
    MB_SKY, // ushort: [1..3]
    MB_SWITCH2,
  }

  enum {
    SW_PL_PRESS = 1<<0,
    SW_MN_PRESS = 1<<1,
    SW_PL_NEAR  = 1<<2,
    SW_MN_NEAR  = 1<<3,
    SW_KEY_R    = 1<<4,
    SW_KEY_G    = 1<<5,
    SW_KEY_B    = 1<<6,
  }

  static struct MapThing {
    // thing flags
    enum : ushort {
      DirRight   = 0x0001,
      DeathMatch = 0x0010, // ���������� ������ � DeathMatch'�
    }

    short x, y;   // ����������
    ushort type;  // ���
    ushort flags; // �����

    @property const pure nothrow @safe @nogc {
      bool left () => ((flags&DirRight) == 0);
      bool right () => ((flags&DirRight) != 0);
      bool dmonly () => ((flags&DeathMatch) != 0);
    }
  }

  static struct MapSwitch {
    ubyte x, y;  // ����������/8
    ubyte type;  // ���
    ubyte tm;    // ������ ���� 0
    ubyte a, b;  // ������ - ����������/8 �����
    ushort c;    // �� ������������ (����� ��)
    ubyte flags; // ����� (SW_*)
  }

public:
  enum { Type, Front, Back, AllLiquids, LiquidMask, LightMask, MapTexturesMax } // tile types, Front+: megatexture types
  ubyte[][3] tiles; // Type, Front, Back
  string[] wallnames; // "_water_0": water, "_water_1": acid, "_water_2": lava
  ubyte[] walltypes; // bit0: solid/non-solid(0x01); bit1: 0x02 if "vtrap01"
  D2DImage[] textures;
  int width, height;
  Texture[MapTexturesMax] texgl;
  TrueColorImage[MapTexturesMax] teximgs;
  D2DImage skytex;
  Texture skytexgl;

  MapThing[] things;
  MapSwitch[] switches;

  this () {}

  this (string fname) { load(fname); }
  this(ST) (auto ref ST fl) { load(fl); }

  bool hasLiquidAt (int x, int y) {
    if (x < 0 || y < 0 || x >= MapSize || y >= MapSize) return false;
    auto tt = tiles.ptr[Front].ptr[y*MapSize+x];
    return (wallnames[tt] == "_water_0" || wallnames[tt] == "_water_1" || wallnames[tt] == "_water_2");
  }

  //FIXME: recreate textures cleanly
  void clearMegaTextures () {
    //foreach (Texture tex; texgl) if (tex !is null) tex.clear;
    //texgl[] = null;
  }

  // build OpenGL "megatextures" with the whole level on it
  void oglBuildMega (uint buildMask=0xffff_ffffu) {
    if (skytexgl is null) skytexgl = new Texture(skytex.img, Texture.Option.Linear, Texture.Option.Repeat);
    //auto img = new TrueColorImage(width*TileSize, height*TileSize);
    foreach (immutable type; Front..LightMask+1) {
      if (teximgs[type] is null) {
        buildMask |= 1<<type;
        teximgs[type] = new TrueColorImage(width*TileSize, height*TileSize);
      }
      if ((buildMask&(1<<type)) == 0) continue;
      auto img = teximgs[type];
      img.imageData.colors[] = Color(0, 0, 0, 0);
      if (type == Back) {
        /*
        foreach (int y; 0..(height*TileSize+skytex.height-1)/skytex.height) {
          foreach (int x; 0..(width*TileSize+skytex.width-1)/skytex.width) {
            img.putTile(x*skytex.width, y*skytex.height, skytex);
          }
        }
        */
      }
      foreach (int y; 0..height) {
        foreach (int x; 0..width) {
          if (type == LightMask) {
            // in the lightmask texture, we should have only occluders' pixels
            auto tt = tiles.ptr[Type].ptr[y*MapSize+x];
            if ((tt&0x80) == 0 && (tt == TILE_WALL || tt == TILE_DOORC)) {
              img.putTile(x*TileSize, y*TileSize, textures.ptr[tiles.ptr[Back].ptr[y*MapSize+x]]);
              img.putTile(x*TileSize, y*TileSize, textures.ptr[tiles.ptr[Front].ptr[y*MapSize+x]]);
            } /*else if (tt != TILE_LIFTU && tt != TILE_LIFTD) {
              img.putTile(x*TileSize, y*TileSize, textures.ptr[tiles.ptr[Front].ptr[y*MapSize+x]]);
            }*/
          } else if (type == AllLiquids) {
            // texture with liquid background, for distortion
            auto tt = tiles.ptr[Front].ptr[y*MapSize+x];
            if (wallnames[tt] == "_water_0" || wallnames[tt] == "_water_1" || wallnames[tt] == "_water_2") {
              tt = tiles.ptr[Back].ptr[y*MapSize+x];
              img.putTile(x*TileSize, y*TileSize, textures.ptr[tt]);
              foreach (int dy; 0..TileSize) {
                foreach (int dx; 0..TileSize) {
                  //img.putPixel(x*TileSize+dx, y*TileSize+dy, Color((tt == 3 ? 128 : 0), (tt == 2 ? 128 : 0), (tt == 1 ? 128 : 0), 128));
                  auto c = img.getPixel(x*TileSize+dx, y*TileSize+dy);
                  if (c.a == 0) img.putPixel(x*TileSize+dx, y*TileSize+dy, Color(0, 0, 0, 255));
                }
              }
            }
          } else if (type == LiquidMask) {
            // texture with liquid colors, will be blended on top of the level
            auto tt = tiles.ptr[Front].ptr[y*MapSize+x];
            auto wclr = Color(0, 0, 0, 0);
                 if (wallnames[tt] == "_water_0") wclr = Color(0, 0, 100, 128); // water
            else if (wallnames[tt] == "_water_1") wclr = Color(24, 140, 0, 128); // acid
            else if (wallnames[tt] == "_water_2") wclr = Color(160, 0, 0, 128); // lava
            if (wclr.a) {
              img.putPixelTile(x*TileSize, y*TileSize, wclr);
              // if this is top one, make some light border
              if (!hasLiquidAt(x, y-1)) {
                // border
                auto wcc = wclr.lighten(0.6);
                wcc.a = wclr.a;
                foreach (int dx; 0..TileSize) img.putPixel(x*TileSize+dx, y*TileSize+0, wcc);
                wcc = wclr.lighten(0.4);
                wcc.a = wclr.a;
                foreach (int dx; 0..TileSize) img.putPixel(x*TileSize+dx, y*TileSize+1, wcc);
                wcc = wclr.lighten(0.2);
                wcc.a = wclr.a;
                foreach (int dx; 0..TileSize) img.putPixel(x*TileSize+dx, y*TileSize+2, wcc);
              }
            }
          } else {
            auto tf = tiles.ptr[Front].ptr[y*MapSize+x];
            auto tb = tiles.ptr[Back].ptr[y*MapSize+x];
            auto tt = tiles.ptr[type].ptr[y*MapSize+x];
            if (wallnames[tf] == "_water_0" || wallnames[tf] == "_water_1" || wallnames[tf] == "_water_2" ||
                wallnames[tb] == "_water_0" || wallnames[tb] == "_water_1" || wallnames[tb] == "_water_2") {
            } else {
              img.putTile(x*TileSize, y*TileSize, textures.ptr[tt]);
            }
          }
        }
      }
      /*{
        import std.string : format;
        import arsd.png : writePng;
        writePng("zpng%02s.png".format(type), img);
      }*/
      if (texgl[type] is null) {
        texgl[type] = new Texture(img, (type == LightMask ? Texture.Option./*Linear*/Nearest : Texture.Option.Nearest), Texture.Option.Clamp);
      } else {
        texgl[type].setFromImage(img, 0, 0);
      }
    }
  }

  void clear () {
    tiles[] = null;
    wallnames = null;
    walltypes = null;
    textures = null;
    foreach (Texture tex; texgl) if (tex !is null) tex.clear;
    texgl[] = null;
    skytex = null;
    things = null;
    switches = null;
  }

  void dump (int idx) {
    static char to62 (ubyte b) { pragma(inline, true); return cast(char)(b < 10 ? '0'+b : (b-10 < 26 ? 'A'+(b-10) : 'a'+(b-10-26))); }
    foreach (immutable y; 0..MapSize) {
      foreach (immutable x; 0..MapSize) {
        conwrite(to62(tiles[idx][y*MapSize+x]));
      }
      conwriteln;
    }
  }

  // true: found
  bool getThingPos (ushort id, int* x=null, int* y=null, ushort* flags=null) {
    foreach (ref th; things[]) {
      if (th.type == id) {
        if (x !is null) *x = th.x;
        if (y !is null) *y = th.y;
        if (flags !is null) *flags = th.flags;
        return true;
      }
    }
    if (x !is null) *x = 0;
    if (y !is null) *y = 0;
    if (flags !is null) *flags = 0;
    return false;
  }

private:
  void calcMapSize () {
    /*
    bool isEmpty(string dir) (int x, int y) if (dir == "col" || dir == "row") {
      while (x < MapSize && y < MapSize) {
        if (tiles[0][y*MapSize+x] || tiles[1][y*MapSize+x] || tiles[2][y*MapSize+x]) return false;
        static if (dir == "row") ++x; else ++y;
      }
      return true;
    }
    width = height = MapSize;
    // fix width
    while (width > 0 && isEmpty!"col"(width-1, 0)) --width;
    // fix height
    while (height > 0 && isEmpty!"row"(0, height-1)) --height;
    */
    width = height = MapSize;
  }

  void load (string fname) {
    load(VFile(fname));
  }

  void load(ST) (auto ref ST st) if (isReadableStream!ST) {
    clear();
    scope(failure) clear;

    char[8] sign;
    st.rawReadExact(sign[]);
    if (sign != "Doom2D\x1a\x00") throw new Exception("invalid map signature");
    if (st.readNum!ushort() != MapVersion) throw new Exception("invalid map version");

    // load map blocks
    foreach (ref a; tiles[]) a = new ubyte[](MapSize*MapSize);
    char[$] skyname = "sprites/sky/rsky1.vga";
    for (;;) {
      auto btype = st.readNum!ushort();
      if (btype == MB_END) break; // no more blocks
      auto bsubtype = st.readNum!ushort();
      auto bsize = st.readNum!uint();
      if (bsize == 0) continue; // skip this block, it has no data (wtf?!)
      // various tile types
      switch (btype) {
        case MB_SKY:
          if (bsize != 2) throw new Exception("invalid sky data size");
          ushort num = st.readNum!ushort();
          if (num >= 1 && num <= 3) skyname[$-5] = cast(char)('0'+num);
          break;
        case MB_BACK:
        case MB_FRONT:
        case MB_WTYPE:
          if (bsubtype > 1) throw new Exception("unknown tile block subtype");
          int idx = (btype == MB_BACK ? Back : (btype == MB_FRONT ? Front : Type));
          //ubyte[MapSize*MapSize] data = 0;
          auto data = tiles[idx];
          if (bsubtype == 0) {
            if (bsize != data.length) throw new Exception("invalid tile data size");
            st.rawReadExact(data[]);
          } else {
            // unpack RLE data
            auto pkdata = new ubyte[](bsize);
            st.rawReadExact(pkdata[]);
            int spos = 0, opos = 0;
            while (spos < pkdata.length) {
              ubyte b = pkdata[spos++];
              if (b != 255) {
                data[opos++] = b;
              } else {
                int count = pkdata[spos++];
                count |= pkdata[spos++]<<8;
                b = pkdata[spos++];
                while (count-- > 0) data[opos++] = b;
              }
            }
            assert(opos == data.length);
          }
          // copy unpacked data
          //foreach (immutable y; 0..MapSize) tiles[idx][y*MapSize] = data[y*MapSize..(y+1)*MapSize];
          break;
        case MB_WALLNAMES:
          wallnames.length = 0;
          wallnames ~= null;
          //wallnames[] = null;
          //walltypes.length = 0;
          while (bsize >= 8+1) {
            char[8] texname = 0;
            st.rawReadExact(texname[]);
            auto type = st.readNum!ubyte();
            //char[] tn;
            string tns;
            foreach (char ch; texname) {
              if (ch == 0) break;
              if (ch >= 'A' && ch <= 'Z') ch += 32;
              ch = dos2koi8(ch);
              tns ~= ch;
              //tn = texname[0..idx+1];
            }
            import std.uni : toLower;
            wallnames ~= koi8lotranslit(tns).toLower;
            type = (type ? 1 : 0);
            if (wallnames[$-1] == "vtrap01") type |= 0x02;
            //wallnames ~= recodeToKOI8(recode(tn.idup, "utf-8", "cp866").toLower, "utf-8");
            //debug { conwriteln(wallnames.length-1, " : ", wallnames[$-1]); }
            //if (wallnames[$-1][$-1] == '_') wallnames[$-1] ~= "1";
            //wallnames[$-1] ~= ".vga";
            //conwriteln(wallnames[$-1]);
            walltypes ~= type;
            bsize -= 8+1;
          }
          if (bsize != 0) throw new Exception("invalid texture chunk size");
          debug { conwriteln(wallnames.length, " textures loaded"); }
          break;
        case MB_THING:
          while (bsize >= 8) {
            bsize -= 8;
            MapThing t = void;
            t.x = st.readNum!short();
            t.y = st.readNum!short();
            t.type = st.readNum!ushort();
            t.flags = st.readNum!ushort();
            if (t.type != 0) things ~= t;
          }
          if (bsize != 0) throw new Exception("invalid thing chunk size");
          break;
        case MB_SWITCH2:
          while (bsize >= 9) {
            bsize -= 9;
            MapSwitch sw = void;
            sw.x = st.readNum!ubyte();
            sw.y = st.readNum!ubyte();
            sw.type = st.readNum!ubyte();
            sw.tm = st.readNum!ubyte();
            sw.a = st.readNum!ubyte();
            sw.b = st.readNum!ubyte();
            sw.c = st.readNum!ushort();
            sw.flags = st.readNum!ubyte();
            switches ~= sw;
          }
          if (bsize != 0) throw new Exception("invalid thing chunk size");
          break;
        default:
          auto pkdata = new ubyte[](bsize);
          st.rawReadExact(pkdata[]);
          break;
      }
    }
    calcMapSize();
    // load textures
    textures.length = 0;
    foreach (immutable idx, string name; wallnames) {
      if (name.length == 0 || name[0] == '_') {
        textures ~= null;
        continue;
      } else {
        textures ~= new D2DImage("tilegfx/"~name~".vga");
      }
    }
    assert(textures.length == wallnames.length);
    // fix tiles
    foreach (immutable y; 0..height) {
      foreach (immutable x; 0..width) {
        if (tiles[Front][y*MapSize+x] >= textures.length) tiles[Front][y*MapSize+x] = 0;
        if (tiles[Back][y*MapSize+x] >= textures.length) tiles[Back][y*MapSize+x] = 0;
      }
    }
    skytex = new D2DImage(skyname.idup);
    //skytex = new D2DImage("sprites/rsky3.vga");
    skytex.removeOffset();
    debug { conwriteln(width, "x", height); }
  }
}
