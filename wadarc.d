/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module wadarc;

import iv.cmdcon;
import iv.vfs;
//import iv.vfs.arcs.q1pak;


// ////////////////////////////////////////////////////////////////////////// //
private __gshared VFSDriver dataPathDriver;
private __gshared string dataPath;


// ////////////////////////////////////////////////////////////////////////// //
void setDataPath (const(char)[] path) {
  if (dataPathDriver !is null) assert(0);
  if (path.length == 0) {
    dataPath = "./";
  } else if (path[$-1] != '/') {
    dataPath = path.idup~"/";
  } else {
    dataPath = path.idup;
  }
  dataPathDriver = vfsNewDiskDriver(dataPath);
  vfsRegister!"first"(dataPathDriver);
}


string getDataPath () { pragma(inline, true); return dataPath; }


void addPak (const(char)[] fname) {
  conwriteln("; adding '", fname, "'...");
  vfsAddPak(fname);
}


string loadTextFile (const(char)[] fname) {
  auto fl = VFile(fname);
  auto sz = fl.size;
  if (sz < 0 || sz > 1024*1024) throw new Exception("invalid text file size: '"~fname.idup~"'");
  if (sz == 0) return "";
  auto res = new char[](cast(uint)sz);
  if (fl.rawRead(res[]).length != res.length) throw new Exception("error reading text file '"~fname.idup~"'");
  import std.exception : assumeUnique;
  return res.assumeUnique;
}
