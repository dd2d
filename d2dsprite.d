/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module d2dsprite is aliced;

private:
import iv.cmdcon;
import iv.glbinds;
import glutils;
import dacs;
import d2dimage;
import tatlas;

//version = tatlas_dump;


// ////////////////////////////////////////////////////////////////////////// //
// sprite atlases
__gshared TexAtlas[] atlases;
__gshared ImgSprite[string] sprImages;


// ////////////////////////////////////////////////////////////////////////// //
public void realiseSpriteAtlases () {
  foreach (immutable idx, TexAtlas a; atlases) {
    import arsd.png;
    import std.string : format;
    a.updateTexture();
    version(tatlas_dump) writePng("_za%02s.png".format(idx), a.img);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class ImgSprite {
  D2DImage vga;
  bool mirrored;
  TexAtlas atlas;
  TexAtlas.Rect arect;
  TexAtlas.FRect frect;
  GLuint listBase; // gl lists (4)

  private this () {}

  ~this () { if (/*!mirrored &&*/ listBase) glDeleteLists(listBase, 4); listBase = 0; }

final:
  void putToAtlas () {
    if (atlas !is null) return;
    TexAtlas.Rect arc;
    TexAtlas aa;
    foreach (TexAtlas a; atlases) {
      auto rc = a.insert(vga);
      if (rc.valid) { arc = rc; aa = a; break; }
    }
    if (!arc.valid) {
      int w = (vga.width < 1024 ? 1024 : vga.width);
      int h = (vga.height < 1024 ? 1024 : vga.height);
      aa = new TexAtlas(w, h);
      arc = aa.insert(vga);
      assert(arc.valid);
      atlases ~= aa;
    }
    atlas = aa;
    arect = arc;
    frect = aa.texCoords(arc);
    createLists();
  }

  private void createLists () {
    listBase = glGenLists(4);
    foreach (immutable my; 0..2) {
      foreach (immutable mx; 0..2) {
        GLuint ln = listBase+(my*2)+mx;
        glNewList(ln, GL_COMPILE);
        int x0 = -(mirrored ? vga.width-1-vga.sx : vga.sx);
        int y0 = -vga.sy;
        int x1 = x0+vga.width;
        int y1 = y0+vga.height;
        if (mx) { int tmp = x0; x0 = x1; x1 = tmp; }
        if (my) { int tmp = y0; y0 = y1; y1 = tmp; }
        glBegin(GL_QUADS);
          glTexCoord2f(frect.x0, frect.y0); glVertex2i(x0, y0); // top-left
          glTexCoord2f(frect.x1, frect.y0); glVertex2i(x1, y0); // top-right
          glTexCoord2f(frect.x1, frect.y1); glVertex2i(x1, y1); // bottom-right
          glTexCoord2f(frect.x0, frect.y1); glVertex2i(x0, y1); // bottom-left
        glEnd();
        glEndList();
      }
    }
  }

  void drawAtXY (int x, int y, bool mirrorX=false, bool mirrorY=false) {
    if (atlas is null || !atlas.hasTexture) return;
    //if (listBase == 0) createLists();
    if (mirrored) mirrorX = !mirrorX;
    ubyte lnum = (mirrorX ? 0x01 : 0x00)|(mirrorY ? 0x02 : 0x00);
    bindTexture(atlas.tex.tid);
    glPushMatrix();
    scope(exit) glPopMatrix();
    glTranslatef(x, y, 0);
    glCallList(listBase+lnum);
  }
}


public final class ImgSpriteMirrored : ImgSprite {
  private this (ImgSprite aspr) {
    vga = aspr.vga;
    mirrored = true;
    atlas = aspr.atlas;
    arect = aspr.arect;
    frect = aspr.frect;
    assert(atlas !is null);
    //listBase = aspr.listBase;
    listBase = 0;
    createLists();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
ImgSprite loadSpriteIntr (string spfilename, bool checkMirror, bool updateAtlas) {
  if (spfilename.length == 0) return null;
  ImgSprite im;
  if (auto imm = spfilename in sprImages) {
    // cached sprite
    //conwriteln("cached sprite '", spfilename, "'");
    im = *imm;
  } else {
    // new sprite
    import std.algorithm : endsWith;
    string spnameReal = spfilename;
    if (checkMirror && spnameReal.endsWith("_mirrored.vga")) {
      // for mirrored, load normal sprite and create mirrored one
      spnameReal = spnameReal[0..$-13]~".vga";
      if (auto imm = spnameReal in sprImages) {
        im = *imm;
      } else {
        im = loadSpriteIntr(spnameReal, false, updateAtlas);
        if (im !is null && updateAtlas) realiseSpriteAtlases();
      }
      if (im is null) return null;
      im = new ImgSpriteMirrored(im);
      assert(im.mirrored);
      assert(im.listBase);
      //conwriteln("mirroired sprite '", spfilename, "' [", spnameReal, "]");
    } else {
      im = new ImgSprite();
      //conwriteln("loading sprite '", spfilename, "'");
      im.vga = new D2DImage(spfilename);
      im.putToAtlas();
      if (updateAtlas) realiseSpriteAtlases();
    }
    sprImages[spfilename] = im;
  }
  return im;
}


public ImgSprite loadSprite (string spfilename, bool updateAtlas=false) { return loadSpriteIntr(spfilename, true, updateAtlas); }
