/* DooM2D: Midnight on the Firing Line
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// ////////////////////////////////////////////////////////////////////////// //
// cursor (hi, Death Track!)
public enum curWidth = 17;
public enum curHeight = 23;
static immutable ubyte[curWidth*curHeight] curImg = [
  0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,0,3,2,2,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,2,2,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,4,2,2,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,4,4,2,2,0,0,0,0,0,0,0,0,0,
  1,1,3,3,4,4,4,2,2,0,0,0,0,0,0,0,0,
  1,1,3,3,4,4,4,4,2,2,0,0,0,0,0,0,0,
  1,1,3,3,4,4,4,5,6,2,2,0,0,0,0,0,0,
  1,1,3,3,4,4,5,6,7,5,2,2,0,0,0,0,0,
  1,1,3,3,4,5,6,7,5,4,5,2,2,0,0,0,0,
  1,1,3,3,5,6,7,5,4,5,6,7,2,2,0,0,0,
  1,1,3,3,6,7,5,4,5,6,7,7,7,2,2,0,0,
  1,1,3,3,7,5,4,5,6,7,7,7,7,7,2,2,0,
  1,1,3,3,5,4,5,6,8,8,8,8,8,8,8,8,2,
  1,1,3,3,4,5,6,3,8,8,8,8,8,8,8,8,8,
  1,1,3,3,5,6,3,3,1,1,1,1,1,1,1,0,0,
  1,1,3,3,6,3,3,1,1,1,1,1,1,1,1,0,0,
  1,1,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
];
static immutable Color[9] curPal = [
  Color(  0,  0,  0,  0),
  Color(  0,  0,  0,163),
  Color( 85,255,255,255),
  Color( 85, 85,255,255),
  Color(255, 85, 85,255),
  Color(170,  0,170,255),
  Color( 85, 85, 85,255),
  Color(  0,  0,  0,255),
  Color(  0,  0,170,255),
];


// ////////////////////////////////////////////////////////////////////////// //
__gshared int edtMX, edtMY;


void editorDrawCursor (int x, int y) {
  x -= 2;
  auto ci = curImg.ptr;
  foreach (int dy; 0..curHeight) {
    foreach (int dx; 0..curWidth) {
      if (*ci) editorPutPixel(x+dx, y, curPal.ptr[*ci]);
      ++ci;
    }
    ++y;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void editorPutPixel (int x, int y, Color clr) {
  pragma(inline, true);
  if (x >= 0 && y >= 0 && x < editorImg.width && y < editorImg.height) {
    editorImg.imageData.colors.ptr[y*editorImg.width+x] = clr;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void editorUpdateImage () {
  editorImg.imageData.colors[] = Color(0, 0, 0, 0);
  editorDrawCursor(edtMX, edtMY);
}


// ////////////////////////////////////////////////////////////////////////// //
void editorKeyEvent (TMsgKeyEvent evt) {
  import arsd.simpledisplay;
  if (evt.pressed && evt.key == Key.Escape) {
    //import core.atomic;
    //atomicSet(editMode, false);
    //postToggleOption("EditMode", showMessage:true);
    return;
  }
  if (evt.pressed) {
    if (evt.key == '1') concmd("r_scale 1");
    if (evt.key == '2') concmd("r_scale 2");
    if (evt.key == 'l') concmd("r_lighting toggle");
    if (evt.key == 'q') concmd("ed_exit");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void editorMouseEvent (TMsgMouseEvent evt) {
  edtMX = evt.x;
  edtMY = evt.y;
}
